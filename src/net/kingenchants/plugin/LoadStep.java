package net.kingenchants.plugin;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface LoadStep {
	
	String name();
	
	String description();
	
	StepInvocation invoke()
			default StepInvocation.ENABLE;
	
	StepPriority
		priority() default StepPriority.NORMAL;
}
