package net.kingenchants.plugin;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.bukkit.plugin.java.JavaPlugin;
import org.cryptolead.core.CryptoCore;
import org.cryptolead.core.plugin.CException;

import net.kingenchants.chat.M;

public abstract class CPlugin extends JavaPlugin {
	
	private String
		name; // The name of the plugin.
	
	private Class<? extends CPlugin>
		loadClass; // The loading class.
	
	/**
	 * Creates new instance of CPlugin.
	 * @param name the name of the plugin.
	 * @param loadClass the loading class.
	 */
	public CPlugin(String name, Class<? extends CPlugin> loadClass) {
		this.name = name;
		this.loadClass = loadClass;
	}
	
	/**
	 * Gets the name of the plugin.
	 * @return the name.
	 */
	public String getPluginName() {
		return name;
	}
	
	/**
	 * Gets the loading class.
	 * @return
	 */
	public Class<? extends CPlugin> getLoadClass() {
		return loadClass;
	}
	
	/**
	 * Enables the plugin.
	 */
	public abstract void enable();
	
	/**
	 * Disables the plugin.
	 */
	public abstract void disable();
	
	/**
	 * Enables the plugin.
	 */
	@Override
	public void onEnable() {
		CryptoCore.sendConsole(M.ENABLING_PLUGIN, name);
		
		enable();
		
		try {
			loadSteps(StepInvocation.ENABLE);
		} catch (CException e) {
			CryptoCore.handleException(e);
		}
	}
	
	/**
	 * Disables the plugin.
	 */
	@Override
	public void onDisable() {
		CryptoCore.sendConsole(M.DISABLING_PLUGIN, name);
		
		disable();
		
		try {
			loadSteps(StepInvocation.DISABLE);
		} catch (CException e) {
			CryptoCore.handleException(e);
		}
	}
	
	/**
	 * Loads plugin's steps.
	 * @param invocation the invocation of the steps.
	 * @throws CException an exception if occurred.
	 */
	private void loadSteps(StepInvocation invocation) throws CException {
		if (loadClass == null)
			throw new CException("LOAD:001");
		
		List<Method> steps = new ArrayList<>();
		
		for (Method method : loadClass.getMethods()) {
			LoadStep step = method.getAnnotation(LoadStep.class);
			if (step == null)
				continue;
			if (!invocation.equals(StepInvocation.BOTH) && !step.invoke().equals(invocation))
				continue;
			
			steps.add(method);
		}
		
		steps.sort(new Comparator<Method>() {

			@Override
			public int compare(Method method1, Method method2) {
				return method2.getAnnotation(LoadStep.class).priority().getLevel() - method1.getAnnotation(LoadStep.class).priority().getLevel();
			}
		});
		
		for (Method method : steps) {
			LoadStep step = method.getAnnotation(LoadStep.class);
			CryptoCore.sendConsole(M.LOADING_STEP, step.description());
			
			try {
				method.invoke(null);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				CryptoCore.handleException(new CException("LOAD:002", e, "At method " + method.getName()));
			}
		}
	}
}