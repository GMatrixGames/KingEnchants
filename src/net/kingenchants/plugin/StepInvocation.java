package net.kingenchants.plugin;

public enum StepInvocation {
	ENABLE(true, false), DISABLE(false, true), BOTH(true, true);
	
	private boolean
			enable,
			disable;
	
	StepInvocation(boolean enable, boolean disable) {
		this.enable = enable;
		this.disable = disable;
		
	}
	
	public boolean onEnable() {return enable;}
	public boolean onDisable() {return disable;}
}