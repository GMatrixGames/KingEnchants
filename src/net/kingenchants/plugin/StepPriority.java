package net.kingenchants.plugin;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public enum StepPriority {
	LOWEST(0),
	LOW(1),
	NORMAL(2),
	HIGH(3),
	HIGHEST(4);
	
	private int
		level;
	
	private StepPriority(int level) {
		this.level = level;
	}
	
	public int getLevel() {
		return level;
	}
	
	public boolean isHigherThan(StepPriority priority) {
		return level > priority.getLevel();
	}
	
	public static StepPriority[] valuesByLevel() {
		List<StepPriority> values = Arrays.asList(values());
		values.sort(new Comparator<StepPriority>() {

			@Override
			public int compare(StepPriority o1, StepPriority o2) {
				return o2.getLevel() - o1.getLevel();
			}
		});
		
		return values.toArray(new StepPriority[values.size()]);
	}
}