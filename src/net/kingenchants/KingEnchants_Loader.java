package net.kingenchants;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.cryptolead.core.CryptoCore;
import org.cryptolead.core.data.config.ConfigManager;
import org.cryptolead.core.plugin.CException;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;

import net.kingenchants.commands.Command_Enchanter;
import net.kingenchants.commands.Command_Enchants;
import net.kingenchants.commands.Command_KingEnchants;
import net.kingenchants.commands.Command_Merge;
import net.kingenchants.commands.Command_Tinkerer;
import net.kingenchants.data.Temp;
import net.kingenchants.enchants.EnchantManager;
import net.kingenchants.enchants.listeners.block.Listener_BlockBreak;
import net.kingenchants.enchants.listeners.entity.Listener_EntityDamageByEntity;
import net.kingenchants.enchants.listeners.entity.Listener_PlayerDeathEvent;
import net.kingenchants.enchants.listeners.entity.Listener_ProjectileHit;
import net.kingenchants.enchants.listeners.entity.Listener_ProjectileLaunch;
import net.kingenchants.gui.GUI_Enchanter;
import net.kingenchants.gui.GUI_Enchants;
import net.kingenchants.gui.GUI_Enchants_Category;
import net.kingenchants.gui.GUI_Merge;
import net.kingenchants.gui.GUI_Tinkerer;
import net.kingenchants.listeners.inventory.Listener_InventoryClick;
import net.kingenchants.listeners.inventory.Listener_InventoryClose;
import net.kingenchants.listeners.player.Listener_PlayerInteract;
import net.kingenchants.plugin.CPlugin;
import net.kingenchants.plugin.LoadStep;
import net.kingenchants.plugin.StepPriority;
import net.kingenchants.utils.SecondClock;
import net.milkbowl.vault.economy.Economy;

public class KingEnchants_Loader extends CPlugin {
	
	private static KingEnchants_Loader
		instance; // The instance of the plugin.
	
	/**
	 * Creates new instance of CentroomAPI_Loader.
	 * @param name the name of the plugin.
	 */
	public KingEnchants_Loader() {
		super("KingEnchants", KingEnchants_Loader.class);
		
		instance = this;
	}
	
	/**
	 * Enables the plugin.
	 */
	@Override
	public void enable() {
		if (getWorldGuard() == null) {
			CryptoCore.sendConsole("No WorldGuard found.");
            Bukkit.getPluginManager().disablePlugin(this);
            return;
		}
		if (!setupEconomy() ) {
			CryptoCore.sendConsole("No vault found.");
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }
	}
	
	/**
	 * Disables the plugin.
	 */
	@Override
	public void disable() {
        Bukkit.getScheduler().cancelTasks(this);
	}
	
	/**
	 * Loads config.
	 */
	@LoadStep(name = "Loads config", description = "Loads config.", priority = StepPriority.HIGHEST)
	public static void loadConfig() {
		if (!Temp.MODE)
			return;
		
		try {
			ConfigManager.loadSettings(KingEnchants.class);
		} catch (CException e) {
			CryptoCore.handleException(e);
		}
	}
	
	/**
	 * Registers listeners.
	 */
	@LoadStep(name = "Registers Listeners", description = "Registers listeners.", priority = StepPriority.HIGHEST)
	public static void registerListeners() {
		if (!Temp.MODE)
			return;
		
		// Register my runnable too, YoDontBeThatGirl
		Bukkit.getServer().getScheduler().runTaskTimer((Plugin) instance, (Runnable) new SecondClock(), 0L, 20L);
		
		
		Bukkit.getPluginManager().registerEvents(new Listener_InventoryClick(), instance);
		Bukkit.getPluginManager().registerEvents(new Listener_InventoryClose(), instance);
		Bukkit.getPluginManager().registerEvents(new net.kingenchants.enchants.listeners.entity.Listener_PlayerExpChange(), instance);
		Bukkit.getPluginManager().registerEvents(new Listener_PlayerInteract(), instance);
		Bukkit.getPluginManager().registerEvents(new Listener_BlockBreak(), instance);
		Bukkit.getPluginManager().registerEvents(new Listener_EntityDamageByEntity(), instance);
		Bukkit.getPluginManager().registerEvents(new Listener_ProjectileLaunch(), instance);
		Bukkit.getPluginManager().registerEvents(new Listener_ProjectileHit(), instance);
		Bukkit.getPluginManager().registerEvents(new Listener_PlayerDeathEvent(), instance);
	
	}
	
	/**
	 * Registers commands.
	 */
	@LoadStep(name = "Registers Commands", description = "Registers commands.", priority = StepPriority.HIGHEST)
	public static void registerCommands() {
		if (!Temp.MODE)
			return;
		
		new Command_KingEnchants().register();
		new Command_Merge().register();
		new Command_Tinkerer().register();
		new Command_Enchanter().register();
		new Command_Enchants().register();
	}
	
	/**
	 * Registers GUIs.
	 */
	@LoadStep(name = "Registers GUIs", description = "Registers GUIs.", priority = StepPriority.HIGHEST)
	public static void registerGUIs() {
		if (!Temp.MODE)
			return;
		
		new GUI_Enchanter();
		new GUI_Merge();
		new GUI_Tinkerer();
		new GUI_Enchants();
		new GUI_Enchants_Category();
	}
	
	@LoadStep(name = "Registers Enchantments", description = "Registers Enchantments.", priority = StepPriority.LOWEST)
	public static void registerEnchantments() {
		if (!Temp.MODE)
			return;
		
		EnchantManager.startTimer();
	}
	
	private boolean setupEconomy() {
        if (Bukkit.getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = Bukkit.getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        Temp.ECON = rsp.getProvider();
        return Temp.ECON != null;
    }
	
	public WorldGuardPlugin getWorldGuard() {
	    Plugin plugin = getServer().getPluginManager().getPlugin("WorldGuard");
	 
	    // WorldGuard may not be loaded
	    if (plugin == null || !(plugin instanceof WorldGuardPlugin)) {
	        return null; // Maybe you want throw an exception instead
	    }
	 
	    return (WorldGuardPlugin) plugin;
	}
	
	/**
	 * Gets the instance of the plugin.
	 * @return the instance.
	 */
	public static KingEnchants_Loader getInstance() {
		return instance;
	}
}