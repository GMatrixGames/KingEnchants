package net.kingenchants.chat;

import org.cryptolead.core.CryptoCore;
import org.cryptolead.core.chat.Msg;
import org.cryptolead.core.utils.ChatUtils.Color;

public class M {

	public static Msg
		// Commands:
		CMD_KINGENCHANTS_HELP = new Msg(Color.GOLD + Color.BOLD + "Available commands:" + Color.YELLOW
				+ "\n- kingenchants givebook <TYPE> (-l - Level | -s - Success Rate | -d - Destroy Rate | -p - Player) | To get a book | king.givebook"
				+ "\n\n- kingenchants give <RARITY> (-l - Level | -p - Player) | To get a random enchantment book | king.give"
				+ "\n\n- kingenchants fboost <MULTIPLIER> <MINUTES> (-p - Player) | Gives a booster| king.fboost"
				+ "\n\n- kingenchants gboost <MULTIPLIER> <MINUTES> | Activates a global booster| king.gboost"
				+ "\n\n- kingenchants item <ITEM> (-p - Player) | Gives a scroll or orb | king.item, king.item.* and king.item.<ITEM>"
				+ "\n\n- kingenchants givedust <RARITY> (-p - Player) | (-s - percentage) | (-a - amount)"),
		
		CMD_GIVEBOOK = new Msg("-TEXT-TEXT" + CryptoCore.COLOR_MAIN + " level {-NUMBER} book is given with success rate of " + Color.GREEN + "-NUMBER" + CryptoCore.COLOR_MAIN + " and destroy rate of " + Color.RED + "-NUMBER" + CryptoCore.COLOR_MAIN + " to {-PLAYER}."),
		CMD_GIVEBOOK_HELP = new Msg("Use /kingenchants givedust <RARITY> (-p - Player) | (-s - percentage) | (-a - amount)"),
		CMD_GIVEBOOK_INVALID_TYPE = new Msg("Invalid type."),
		CMD_GIVEBOOK_INVALID_LEVEL = new Msg("Invalid level."),
		CMD_GIVEBOOK_INVALID_SUCCESS = new Msg("Invalid success rate."),
		CMD_GIVEBOOK_INVALID_DESTROY = new Msg("Invalid destroy."),
		CMD_GIVEBOOK_INVALID_LEVEL_MAXIMUM = new Msg("Level is too high, maximum level of this type is -NUMBER."),
		CMD_GIVEBOOK_INVALID_PLAYER = new Msg("Invalid player."),
		
		
		CMD_GIVEDUST = new Msg("-TEXT-TEXT" + CryptoCore.COLOR_MAIN + " level {-NUMBER} book is given with success rate of " + Color.GREEN + "-NUMBER" + CryptoCore.COLOR_MAIN + " and destroy rate of " + Color.RED + "-NUMBER" + CryptoCore.COLOR_MAIN + " to {-PLAYER}."),
		CMD_GIVEDUST_HELP = new Msg("Use /kingenchants givedust (-a - Amount | -r - Rarity | -p - Percentage)"),
		CMD_GIVEDUST_INVALID_TYPE = new Msg("Invalid type."),
		CMD_GIVEDUST_INVALID_SUCCESS = new Msg("Invalid success rate."),
		CMD_GIVEDUST_INVALID_AMOUNT = new Msg("Level is too high, maximum level of this type is -NUMBER."),
		
		CMD_GIVE_HELP = new Msg("Use /kingenchants give <RARITY> (-l - Level | -p - Player)"),
		CMD_GIVE_INVALID_PLAYER = new Msg("Invalid player."),
		CMD_GIVE_INVALID_RARITY = new Msg("Invalid rarity."),
		CMD_GIVE_INVALID_LEVEL = new Msg("Invalid level."),
		CMD_GIVE = new Msg("Enchantment book is given with rarity of -TEXT-TEXT" + CryptoCore.COLOR_MAIN + " to {-PLAYER}."),
		CMD_GIVE_LEVEL = new Msg("Enchantment book is given with rarity of -TEXT-TEXT" + CryptoCore.COLOR_MAIN + " and level of {-NUMBER} to {-PLAYER}."),
		
		CMD_ITEM_HELP = new Msg("Use /kingenchants item <TYPE> (-p - Player)"),
		CMD_ITEM_INVALID_PLAYER = new Msg("Invalid player."),
		CMD_ITEM_INVALID_TYPE = new Msg("Invalid type."),
		CMD_ITEM = new Msg("{-TEXT} is given to {-PLAYER}."),
		
		CMD_GLOBALBOOST_HELP = new Msg("Use /kingenchants gboost <MULTIPLIER> <MINUTES>"),
		CMD_GLOBALBOOST_INVALID_MULTIPLIER = new Msg("Invalid multiplier."),
		CMD_GLOBALBOOST_INVALID_TIME = new Msg("Invalid time."),
		CMD_GLOBALBOOST = new Msg("Global XP boost of x{-DOUBLE} activated for {-NUMBER} minutes."),
		
		CMD_BOOST_HELP = new Msg("Use /kingenchants fboost <MULTIPLIER> <MINUTES> (-p - Player)"),
		CMD_BOOST_INVALID_PLAYER = new Msg("Invalid player."),
		CMD_BOOST_INVALID_MULTIPLIER = new Msg("Invalid multiplier."),
		CMD_BOOST_INVALID_TIME = new Msg("Invalid time."),
		CMD_BOOST = new Msg("XP boost of x{-DOUBLE} for {-NUMBER} minutes given to {-PLAYER}."),
		
		CMD_ENCHANTER_SUCCESS = new Msg("Successfully bought -TEXT-TEXT " + CryptoCore.COLOR_MAIN + "book."),
		CMD_ENCHANTER_NO_SPACE = new Msg("You don't have enough space in your inventory."),
		CMD_ENCHANTER_NO_MONEY = new Msg("You can't afford this book."),
		
		// Scroll:
		WORB_CANT_APPLY_ITEM = new Msg("You cannot apply weapon orb to this item."),
		WORB_ALREADY_ON = new Msg("This item already have a weapon orb."),
		WORB_APPLY = new Msg("Weapon orb applied!"),
		
		AORB_CANT_APPLY_ITEM = new Msg("You cannot apply armor orb to this item."),
		AORB_ALREADY_ON = new Msg("This item already have a armor orb."),
		AORB_APPLY = new Msg("Armor orb applied!"),
		
		SCROLL_WHITE_CANT_APPLY_ITEM = new Msg("You cannot apply white scroll to this item."),
		SCROLL_WHITE_ALREADY_ON = new Msg("This item already have a white scroll."),
		SCROLL_WHITE_APPLY = new Msg("White scroll applied!"),
		
		SCROLL_RANDOM_CANT_APPLY_ITEM = new Msg("You can only apply randomization scroll to enchant books."),
		SCROLL_RANDOM_APPLY = new Msg("Randomization scroll applied!"),
		
		SCROLL_BLACK_CANT_APPLY_ITEM = new Msg("You cannot apply black scroll to this item."),
		SCROLL_BLACK_NO_ENCHANTS = new Msg("This item has no enchants."),
		SCROLL_BLACK_APPLY = new Msg("Black scroll applied! You got back -TEXT-TEXT."),
		SCROLL_BLACK_FAILED = new Msg("Black scroll failed."),
		SCROLL_BLACK_FAILED_SAVED = new Msg("Black scroll failed but item was saved by white scroll."),
		
		SCROLL_TRANSMOG_CANT_APPLY_ITEM = new Msg("You cannot apply transmog scroll to this item."),
		SCROLL_TRANSMOG_NO_ENCHANTS = new Msg("This item has no enchants."),
		SCROLL_TRANSMOG_APPLY = new Msg("Transmog scroll applied!"),
		
		BOOSTER_ALREADY_ACTIVE = new Msg("There is already an active booster for your faction."),
		BOOSTER_ACTIVED = new Msg("{-PLAYER} has activated a x{-DOUBLE} booster for {-NUMBER} minutes!"),
		BOOSTER_NO_FACTION = new Msg("You have to be in a faction in order to use a booster."),
		
		// Books:
		CANT_TINKER = new Msg("You cannot tinker this item."),
		TINKERER_FULL = new Msg("The tinkerer is already full."),
		SUCCESS_DUST_ALREADY_MAX = new Msg("This book already has 100% success rate."),
		SUCCESS_DUST_APPLIED = new Msg("Success dust used."),
		BASE_DUST_USED_FAILED = new Msg("Dust failed."),
		BASE_DUST_USED = new Msg("You got " + Color.GREEN + "Success Dust" + CryptoCore.COLOR_MAIN + " with percent of " + Color.GREEN + "-NUMBER" + CryptoCore.COLOR_MAIN + "%."),
		CANT_PUT_TYPE = new Msg("You cannot apply this enchant on that item."),
		CANT_PUT_MORE_ENCHANTS = new Msg("You cannot apply more enchants on that item."),
		CANT_PUT_THIS_ENCHANT = new Msg("You cannot apply more of this enchant on that item."),
		ENCHANT_APPLIED = new Msg("Enchant applied!"),
		ENCHANT_NOT_APPLIED = new Msg("Enchant FAILED to apply!"),
		ENCHANT_NOT_APPLIED_SCROLL = new Msg("Item supposed to be destroyed but was protected by white scroll."),
		BASE_DUST_USE_NO_SPACE = new Msg("You do not have enough space in your inventory to use this random dust."),
		ITEM_DESTROYED = new Msg("Item destroyed!"),
		INVALID_MERGE_ITEMS = new Msg("Invalid items."),
		CANT_MERGE_ITEMS = new Msg("These books cannot be merged."),
		BASE_BOOK_USED = new Msg("Enchantment book used. You got -TEXT" + CryptoCore.COLOR_MAIN + "!"),
		BASE_BOOK_USE_NO_SPACE = new Msg("You do not have enough space in your inventory to use this enchantment book."),
		
		// General:
		COMMAND_NO_PREMISSIONS = new Msg("You don't have permission to perform this command."),
		ENABLING_PLUGIN = new Msg("{LOG: Enabling plugin -TEXT...}"),
		DISABLING_PLUGIN = new Msg("{LOG: Disabling plugin -TEXT...}"),
		LOADING_STEP = new Msg("LOG: Loading -  {-TEXT}");
}