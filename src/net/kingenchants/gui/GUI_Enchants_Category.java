package net.kingenchants.gui;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.ItemStack;
import org.cryptolead.core.CryptoCore;
import org.cryptolead.core.commons.RandomUtils;
import org.cryptolead.core.plugin.CException;
import org.cryptolead.core.utils.InventoryUtils;

import net.kingenchants.data.players.P;
import net.kingenchants.enchants.AllowedItems;
import net.kingenchants.enchants.BookEnchant;
import net.kingenchants.enchants.CustomEnchant;
import net.kingenchants.enchants.EnchantType;
import net.kingenchants.utils.enchants.EnchantUtils;
import net.kingenchants.utils.gui.GUI;
import net.kingenchants.utils.gui.GUIItem;
import net.kingenchants.utils.gui.GUIItemClick;
import net.kingenchants.utils.gui.GUIPreset;

public class GUI_Enchants_Category extends GUIPreset {

	public GUI_Enchants_Category() {
		super("enchants.category");
	}

	@Override
	public GUI getGUI(Player player, Object... args) {
		GUI gui = new GUI(getID(), GUIs.ENCHANTS_CATEGORY_TITLE, 6, false);

		if (args.length < 1 || !(args[0] instanceof AllowedItems))
			return gui;

		AllowedItems allowed = (AllowedItems) args[0];

		List<EnchantType> types = new ArrayList<>();
		for (EnchantType type : EnchantType.values())
			if (type.getAllowedItems().equals(allowed))
				types.add(type);

		int slot = 1;
		int raw = 0;
		for (EnchantType type : types) {
			GUIItem item = new GUIItem(new ItemStack(Material.ENCHANTED_BOOK), raw, slot);
			item.setName(GUIs.ENCHANTS_CATEGORY_ITEMS_NAME, type.getRarity().getColor(), type.getName());
			// for (String l : type.getDescription().split("\n"))
			// item.addLore(GUIs.ENCHANTS_CATEGORY_ITEMS_LORE, l);

			int maxChars = 30;
			int current = 0;
			String message = "";
			for (String l : type.getDescription().split(" ")) {
				current += l.length() + 1;
				message += l + " ";
				if (current > maxChars) {
					item.addLore(GUIs.ENCHANTS_CATEGORY_ITEMS_LORE, message);
					current = 0;
					message = "";
				}
			}
			if (current > 0) {
				item.addLore(GUIs.ENCHANTS_CATEGORY_ITEMS_LORE, message);
			}

			item.addLore(GUIs.ENCHANTS_CATEGORY_ITEMS_LORE_LEVEL, type.getMaximumLevel());

			try {
				if (P.get(player).hasPermission("king.getenchants")) {
					item.addLore(" ");
					item.addLore(GUIs.ENCHANTS_CATEGORY_ITEMS_LORE_CLICK);
					item.addClickListener(new GUIItemClick() {

						@Override
						public void click(GUIItem item, Player player, InventoryAction action) {
							BookEnchant enchant = new BookEnchant(
									new CustomEnchant(type, RandomUtils.randomInt(1, type.getMaximumLevel())),
									RandomUtils.randomInt(100), RandomUtils.randomInt(100));
							ItemStack is = EnchantUtils.toBook(enchant);
							if (player != null)
								if (InventoryUtils.hasSpace(player.getInventory(), is))
									player.getInventory().addItem(is);
								else
									player.getWorld().dropItem(player.getLocation(), is);
						}
					});
				}
			} catch (CException e) {
				CryptoCore.handleException(e);
			}

			gui.addItem(item);
			slot++;
			if (slot == 8) {
				slot = 1;
				raw++;
			}
		}

		gui.setBackSlot();

		return gui;
	}
}