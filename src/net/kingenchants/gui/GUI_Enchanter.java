package net.kingenchants.gui;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.ItemStack;
import org.cryptolead.core.utils.InventoryUtils;

import net.kingenchants.KingEnchants;
import net.kingenchants.chat.M;
import net.kingenchants.data.players.P;
import net.kingenchants.enchants.EnchantRarity;
import net.kingenchants.utils.MoneyUtils;
import net.kingenchants.utils.enchants.EnchantUtils;
import net.kingenchants.utils.gui.GUI;
import net.kingenchants.utils.gui.GUIItem;
import net.kingenchants.utils.gui.GUIItemClick;
import net.kingenchants.utils.gui.GUIPreset;

public class GUI_Enchanter extends GUIPreset {
	
	public GUI_Enchanter() {
		super("enchanter");
	}

	@Override
	public GUI getGUI(Player player, Object... args) {
		GUI gui = new GUI(getID(), GUIs.ENCHANTER_TITLE, 3, false);
		
		int slot = 2;
		for (EnchantRarity rarity : EnchantRarity.values()) {
			GUIItem item = new GUIItem(new ItemStack(Material.STAINED_GLASS_PANE, 1, rarity.getBlockColor()), 1, slot);
			item.setName(GUIs.ENCHANTER_ITEMS_NAME, rarity.getColor(), rarity.getName());
			item.addLore(GUIs.ENCHANTER_ITEMS_LORE, rarity.getColor(), rarity.getName());
			
			int price = -1;
			if (KingEnchants.MONEY_TYPE.equals(MoneyUtils.EXP))
				price = rarity.getExpPrice();
			else if (KingEnchants.MONEY_TYPE.equals(MoneyUtils.VAULT))
				price = rarity.getVaultPrice();
			
			item.addLore(GUIs.ENCHANTER_ITEMS_LORE_PRICE, price);
			
			int priceFinal = price;
			item.addClickListener(new GUIItemClick() {
				
				@Override
				public void click(GUIItem item, Player player, InventoryAction action) {
					MoneyUtils moneyUtils = new MoneyUtils(player, KingEnchants.MONEY_TYPE);
					
					if (moneyUtils.takeMoney(priceFinal)) {
						ItemStack baseBook = EnchantUtils.toBaseBook(rarity, -1);
						
						if (InventoryUtils.hasSpace(player.getInventory(), baseBook)) {
							player.getInventory().addItem(baseBook);
							P.sendMessage(player, M.CMD_ENCHANTER_SUCCESS, rarity.getColor(), rarity.getName());
						} else
							P.sendMessage(player, M.CMD_ENCHANTER_NO_SPACE);
					} else
						P.sendMessage(player, M.CMD_ENCHANTER_NO_MONEY);
				}
			});
			
			gui.addItem(item);
			slot++;
		}
		
		return gui;
	}
}