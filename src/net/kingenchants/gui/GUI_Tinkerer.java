package net.kingenchants.gui;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.ItemStack;
import org.cryptolead.core.utils.InventoryUtils;

import net.kingenchants.enchants.BookEnchant;
import net.kingenchants.utils.enchants.EnchantUtils;
import net.kingenchants.utils.gui.GUI;
import net.kingenchants.utils.gui.GUIItem;
import net.kingenchants.utils.gui.GUIItemClick;
import net.kingenchants.utils.gui.GUIPreset;

public class GUI_Tinkerer extends GUIPreset {
	
	public GUI_Tinkerer() {
		super("tinkerer");
	}

	@Override
	public GUI getGUI(Player player, Object... args) {
		GUI gui = new GUI(getID(), GUIs.TINKERER_TITLE, 6, false);
		
		GUIItemClick click = new GUIItemClick() {
			
			@Override
			public void click(GUIItem item, Player player, InventoryAction action) {
				List<ItemStack> backItems = new ArrayList<>();
				
				for (int i = 0; i < 6; i++)
					for (int j = 0; j < 4; j++)
						if (j + (i * 9) != 0) {
							ItemStack is = player.getOpenInventory().getTopInventory().getItem(j + (i * 9));
							
							if (is == null)
								continue;
							
							BookEnchant enchant = EnchantUtils.fromBook(is);
							if (enchant == null) {
								backItems.add(is);
								continue;
							}
							
							ItemStack dust = EnchantUtils.toBaseDust();
							dust.setAmount(is.getAmount());
							backItems.add(dust);
							player.getOpenInventory().getTopInventory().setItem(j + (i * 9), null);
						}
				
				for (ItemStack is : backItems) {
					if (InventoryUtils.hasSpace(player.getInventory(), is))
						player.getInventory().addItem(is);
					else
						player.getWorld().dropItem(player.getLocation(), is);
				}
				
				player.closeInventory();
			}
		};
		
		GUIItem tinker = new GUIItem(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 14), 0, 0);
		tinker.setName(GUIs.TINKERER_TINKER_NAME);
		tinker.addLore(GUIs.TINKERER_TINKER_LORE);
		tinker.addClickListener(click);
		
		gui.addItem(tinker);
		
		tinker = new GUIItem(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 14), 0, 8);
		tinker.setName(GUIs.TINKERER_TINKER_NAME);
		tinker.addLore(GUIs.TINKERER_TINKER_LORE);
		tinker.addClickListener(click);
		
		gui.addItem(tinker);
		
		for (int i = 0; i < 6; i++) {
			GUIItem seperator = new GUIItem(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 0), i, 4);
			seperator.setName(" ");
			
			gui.addItem(seperator);
		}
		
		return gui;
	}
}