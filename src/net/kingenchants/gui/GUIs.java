package net.kingenchants.gui;

import org.cryptolead.core.CryptoCore;
import org.cryptolead.core.chat.Msg;
import org.cryptolead.core.utils.ChatUtils.Color;

public class GUIs {

	public static Msg
	// Enchanter:
	ENCHANTER_TITLE = new Msg(Color.DARK_GRAY + Color.BOLD + "Enchant"),

			ENCHANTER_ITEMS_NAME = new Msg("-TEXT" + Color.BOLD + "-TEXT"),
			ENCHANTER_ITEMS_LORE = new Msg("Click to buy -TEXT-TEXT " + CryptoCore.COLOR_MAIN + "enchant book"),
			ENCHANTER_ITEMS_LORE_PRICE = new Msg("Cost: {-NUMBER}"),

			// Enchants:
			ENCHANTS_TITLE = new Msg(Color.DARK_GRAY + Color.BOLD + "Enchants"),

			ENCHANTS_ITEMS_NAME = new Msg(Color.GOLD + Color.BOLD + "-TEXT"),
			ENCHANTS_ITEMS_LORE = new Msg("Click to view enchants"),

			// Enchants Category:
			ENCHANTS_CATEGORY_TITLE = new Msg(Color.DARK_GRAY + Color.BOLD + "Enchants"),

			ENCHANTS_CATEGORY_ITEMS_NAME = new Msg("-TEXT" + Color.BOLD + "-TEXT"),
			ENCHANTS_CATEGORY_ITEMS_LORE = new Msg("-TEXT"),
			ENCHANTS_CATEGORY_ITEMS_LORE_LEVEL = new Msg("Maximum Level: {-NUMBER}"),
			ENCHANTS_CATEGORY_ITEMS_LORE_CLICK = new Msg("Click to get a book."),

			// Merge:
			MERGE_TITLE = new Msg(Color.DARK_GRAY + Color.BOLD + "Alchemist"),

			MERGE_MERGE_NAME = new Msg(Color.GOLD + Color.BOLD + "Merge Books"),
			MERGE_MERGE_LORE = new Msg("Click to merge books"),

			// Tinkerer:
			TINKERER_TITLE = new Msg(Color.DARK_GRAY + Color.BOLD + "Tinkerer"),

			TINKERER_TINKER_NAME = new Msg(Color.GOLD + Color.BOLD + "Tinker"),
			TINKERER_TINKER_LORE = new Msg("Click to tinker items");
}