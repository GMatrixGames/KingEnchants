package net.kingenchants.gui;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.ItemStack;

import net.kingenchants.chat.M;
import net.kingenchants.data.players.P;
import net.kingenchants.enchants.BookEnchant;
import net.kingenchants.utils.enchants.EnchantUtils;
import net.kingenchants.utils.gui.GUI;
import net.kingenchants.utils.gui.GUIItem;
import net.kingenchants.utils.gui.GUIItemClick;
import net.kingenchants.utils.gui.GUIPreset;

public class GUI_Merge extends GUIPreset {
	
	public GUI_Merge() {
		super("merge");
	}

	@Override
	public GUI getGUI(Player player, Object... args) {
		GUI gui = new GUI(getID(), GUIs.MERGE_TITLE, 3, false);
		
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 9; j++)
				if (!(i == 1 && j == 2) && !(i == 1 && j == 6) && !(i == 4 && j == 2)) {
					GUIItem item = new GUIItem(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15), i, j);
					item.setName(" ");
					gui.addItem(item);
				}
		
		GUIItem merge = new GUIItem(new ItemStack(Material.WORKBENCH), 2, 4);
		merge.setName(GUIs.MERGE_MERGE_NAME);
		merge.addLore(GUIs.MERGE_MERGE_LORE);
		merge.addClickListener(new GUIItemClick() {
			
			@Override
			public void click(GUIItem item, Player player, InventoryAction action) {
				ItemStack is0 = player.getOpenInventory().getItem(11);
				ItemStack is1 = player.getOpenInventory().getItem(15);
				
				BookEnchant book0 = EnchantUtils.fromBook(is0);
				BookEnchant book1 = EnchantUtils.fromBook(is1);
				if (book0 == null || book1 == null) {
					P.sendMessage(player, M.INVALID_MERGE_ITEMS);
					return;
				}
				
				BookEnchant enchant = EnchantUtils.mergeBooks(book0, book1);
				if (enchant == null) {
					P.sendMessage(player, M.CANT_MERGE_ITEMS);
					return;
				}
				
				ItemStack is = EnchantUtils.toBook(enchant);
				player.getInventory().addItem(is);
				
				if (is0.getAmount() > 1) {
					ItemStack isBack0 = is0;
					isBack0.setAmount(is0.getAmount() - 1);
					player.getInventory().addItem(isBack0);
					
					ItemStack isBack1 = is1;
					isBack1.setAmount(is1.getAmount() - 1);
					player.getInventory().addItem(isBack1);
				}
				
				player.getOpenInventory().setItem(11, null);
				player.getOpenInventory().setItem(15, null);
				
				player.closeInventory();
			}
		});
		
		gui.addItem(merge);
		
		return gui;
	}
}