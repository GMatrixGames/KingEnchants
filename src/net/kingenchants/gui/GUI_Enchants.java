package net.kingenchants.gui;

import org.bukkit.entity.Player;

import net.kingenchants.enchants.AllowedItems;
import net.kingenchants.utils.gui.GUI;
import net.kingenchants.utils.gui.GUIItem;
import net.kingenchants.utils.gui.GUIPreset;

public class GUI_Enchants extends GUIPreset {
	
	public GUI_Enchants() {
		super("enchants");
	}

	@Override
	public GUI getGUI(Player player, Object... args) {
		GUI gui = new GUI(getID(), GUIs.ENCHANTS_TITLE, 4, false);
		
		int slot = 1;
		int raw = 1;
		for (AllowedItems allowed : AllowedItems.values()) {
			GUIItem item = new GUIItem(allowed.getItem(), raw, slot);
			item.setName(GUIs.ENCHANTS_ITEMS_NAME, allowed.getName());
			item.addLore(GUIs.ENCHANTS_ITEMS_LORE);
			item.setClickID("enchants.category", allowed);
			
			gui.addItem(item);
			slot++;
			if (slot == 8) {
				slot = 1;
				raw++;
			}
		}
		
		return gui;
	}
}