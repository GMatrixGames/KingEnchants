package net.kingenchants;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.cryptolead.core.CryptoCore;
import org.cryptolead.core.chat.ChatManager;
import org.cryptolead.core.chat.Msg;
import org.cryptolead.core.commons.minecraft.nms.NMSUtils;
import org.cryptolead.core.data.config.ConfigValue;
import org.cryptolead.core.plugin.CException;

import net.kingenchants.data.players.P;
import net.kingenchants.utils.MoneyUtils;

public class KingEnchants {
	
	@ConfigValue(name = "money_type")
	public static String
		MONEY_TYPE = MoneyUtils.VAULT;
	
	public static String
		COMMAND_PERMISSIONS_PREFIX = "";
	
	@ConfigValue(name = "maximum_enchants")
	public static int
		MAX_ENCHANTS = 100;
	
	@ConfigValue(name = "success_dust_rate")
	public static int
		SUCCESS_DUST_RATE = 80;
	
	public static int
		LONGBOW_DAMAGE = 2;
	
	// ----- Methods -----
	
	/**
	 * Broadcasts a message.
	 * @param message the message.
	 */
	public static void broadcast(String message) {
		for (Player player : getPlayers())
			P.sendMessage(player, message);
	}
	
	/**
	 * Broadcasts a message.
	 * @param message the message.
	 * @param args the argument of the message.
	 */
	public static void broadcast(Msg message, Object... args) {
		for (Player player : getPlayers())
			P.sendMessage(player, ChatManager.process(message, player.getUniqueId(), (Object[]) args));
	}
	
	/**
	 * Calls an event.
	 * @param event the event.
	 */
	public static void callEvent(Event event) {
		Bukkit.getPluginManager().callEvent(event);
	}
	
	/**
	 * Gets the online players.
	 * @return the players.
	 */
	public static Set<Player> getPlayers() {
		return new HashSet<>(Bukkit.getOnlinePlayers());
	}
	
	/**
	 * Gets the command map of the server.
	 * @return the command map.
	 */
	public static CommandMap getCommandMap() {
		try {
			Method m = NMSUtils.CRAFT_SERVER.getMethod("getCommandMap");

			return (CommandMap) m.invoke(Bukkit.getServer());
		} catch(Exception e) {
			CryptoCore.handleException(new CException("CMD:001", e));
		}
		return null;
	}
	
	/**
	 * Reloads the server.
	 */
	public static void reloadServer() {
		Bukkit.reload();
	}
	
	public static void sendMessage(CommandSender sender, Msg message, Object... args) {
		if (sender instanceof Player)
			P.sendMessage((Player) sender, message, args);
		else
			CryptoCore.sendConsole(message, args);
	}
}