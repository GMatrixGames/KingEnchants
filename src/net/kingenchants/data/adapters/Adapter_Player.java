package net.kingenchants.data.adapters;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.cryptolead.core.CryptoCore;
import org.cryptolead.core.commons.StringUtils.UUIDUtils;
import org.cryptolead.core.data.adapters.Adapter;
import org.cryptolead.core.plugin.CException;
import org.json.simple.JSONObject;

import net.kingenchants.data.players.P;

public class Adapter_Player extends Adapter<Player> {
	
	/**
	 * Creates a new instance of the adapter.
	 */
	public Adapter_Player() {
		super("Player", Player.class);
	}
	
	/**
	 * Serializes the value.
	 * @param arg the value to serialize.
	 * @return the serialized value.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public JSONObject serialize(Player arg) {
		JSONObject json = new JSONObject();
		json.put("value", arg.getUniqueId().toString());
		
		return json;
	}
	
	/**
	 * Deserializing a value.
	 * @param arg the JSON object to deserialize.
	 * @return the deserialized object.
	 */
	@Override
	public Player deserialize(JSONObject arg) {
		if (arg.get("value") == null)
			return null;
		
		if (!UUIDUtils.isUUID(String.valueOf(arg.get("value"))))
			return Bukkit.getPlayer(String.valueOf(arg.get("value")));
		
		return Bukkit.getPlayer(UUID.fromString(String.valueOf(arg.get("value"))));
	}
	
	@Override
	public String getName(Player arg) {
		try {
			return P.get(arg).getNick();
		} catch (CException e) {
			CryptoCore.handleException(e);
			return "ERROR";
		}
	}
}