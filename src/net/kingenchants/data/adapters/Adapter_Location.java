package net.kingenchants.data.adapters;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.cryptolead.core.data.RawLocation;
import org.cryptolead.core.data.adapters.Adapter;
import org.cryptolead.core.data.adapters.AdapterManager;
import org.json.simple.JSONObject;

public class Adapter_Location extends Adapter<Location> {
	
	/**
	 * Creates a new instance of the adapter.
	 */
	public Adapter_Location() {
		super("Location", Location.class);
	}
	
	/**
	 * Serializes the value.
	 * @param arg the value to serialize.
	 * @return the serialized value.
	 */
	@Override
	public JSONObject serialize(Location arg) {
		JSONObject json = new JSONObject();
		
		Adapter<RawLocation> rawAdapter = AdapterManager.getAdapter(RawLocation.class);
		if (rawAdapter == null)
			return json;
		
		return rawAdapter.serialize(new RawLocation(arg.getWorld() == null ? null : arg.getWorld().getName(), arg.getX(), arg.getY(), arg.getZ(), arg.getYaw(), arg.getPitch()));
	}
	
	/**
	 * Deserializing a value.
	 * @param arg the JSON object to deserialize.
	 * @return the deserialized object.
	 */
	@Override
	public Location deserialize(JSONObject arg) {
		Adapter<RawLocation> rawAdapter = AdapterManager.getAdapter(RawLocation.class);
		if (rawAdapter == null)
			return null;
		
		RawLocation raw = rawAdapter.deserialize(arg);
		
		org.bukkit.Location location = new Location(raw.getWorld() == null ? null : Bukkit.getWorld(raw.getWorld()), raw.getX(), raw.getY(), raw.getZ());
		location.setYaw(raw.getYaw());
		location.setPitch(raw.getPitch());
		
		return location;
	}
	
	@Override
	public String getName(Location arg) {
		return "World: " + arg.getWorld() + " X: " + arg.getX() + " Y: " + arg.getY() + " Z: " + arg.getZ() + " Yaw: " + arg.getYaw() + " Pitch: " + arg.getPitch();
	}
}