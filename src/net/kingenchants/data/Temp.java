package net.kingenchants.data;

import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.bukkit.entity.Arrow;
import org.bukkit.inventory.ItemStack;

import com.massivecraft.factions.Faction;

import net.kingenchants.data.adapters.Adapter_Location;
import net.kingenchants.data.adapters.Adapter_Player;
import net.kingenchants.utils.gui.GUI;
import net.kingenchants.utils.gui.GUIPreset;
import net.milkbowl.vault.economy.Economy;

public class Temp {
	
	// ----- Misc. -----
	
	public static boolean
		MODE = true; // The mode the plugin.
	
	public static Map<String, ItemStack>
		HEADS_CACHE = new HashMap<>(); // Heads' ItemStack cache.
	
	public static Map<URL, ItemStack>
		HEADS_URL_CACHE = new HashMap<>(); // Heads' ItemStack cache.
	
	public static Set<GUIPreset>
		GUI_PRESETS = new HashSet<>(); // The loaded GUI presets.
	
	public static Map<UUID, GUI>
		OPEN_GUIS = new HashMap<>(); // Players' opened GUIs.
	
	public static Map<UUID, GUI>
		LAST_GUIS = new HashMap<>(); // Players' last GUIs.
	
	public static Map<Faction, Double>
		XP_MULTIPLIER = new HashMap<>(); // XP multipliers.
	
	public static double
		GLOBAL_XP_MULTIPLIER = -1; // Global XP multiplier.
	
	public static Economy
		ECON;
	
	public static Map<Arrow, Double>
		DAMAGE_ARROWS = new HashMap<>();
	
	static {
		new Adapter_Player();
		new Adapter_Location();
	}
	
}