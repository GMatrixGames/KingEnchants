package net.kingenchants.data.players;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.cryptolead.core.chat.ChatManager;
import org.cryptolead.core.chat.Msg;
import org.cryptolead.core.commons.Validate;
import org.cryptolead.core.data.players.PD;
import org.cryptolead.core.plugin.CException;
import org.cryptolead.core.utils.ChatUtils.Color;

import net.md_5.bungee.api.chat.TextComponent;

public class P {
	
	/**
	 * Kicks the player.
	 * @param player the player.
	 * @param message the message.
	 * @param args the arguments of the message.
	 */
	public static void kick(Player player, Msg message, Object... args) {
		player.kickPlayer(ChatManager.process(message, player.getUniqueId(), args));
	}
	
	/**
	 * Teleports a player.
	 * @param player the player.
	 * @param location the location.
	 */
	public static void teleport(Player player, Location location) {
		player.teleport(location);
	}
	
	/**
	 * Kicks the player.
	 * @param player the player.
	 * @param message the message.
	 */
	public static void kick(Player player, String message) {
		player.kickPlayer(message);
	}
	
	/**
	 * Sends a message to a player.
	 * @param player the player.
	 * @param message the message.
	 * @param args the arguments of the message.
	 */
	public static void sendMessage(Player player, Msg message, Object... args) {
		sendMessage(player, ChatManager.process(message, player.getUniqueId(), args));
	}
	
	/**
	 * Sends a message to a player.
	 * @param player the player.
	 * @param message the message.
	 */
	public static void sendMessage(Player player, String message) {
		if (message != null)
			player.sendMessage(Color.GOLD + Color.BOLD + "ENCHANTS " + Color.DARK_GRAY + "� " + Color.RESET + message);
	}
	
	/**
	 * Sends a message to a player.
	 * @param player the player.
	 * @param message the message.
	 */
	public static void sendMessage(Player player, TextComponent message) {
		if (message != null)
			player.spigot().sendMessage(message);
	}
	
	/**
	 * Gets player data.
	 * @param player the player.
	 * @return the player data.
	 * @throws CException an exception if occurred.
	 */
	public static PD get(Player player) throws CException {
		Validate.notNull(player, "player +-");
		
		return PD.get(player.getUniqueId());
	}
}