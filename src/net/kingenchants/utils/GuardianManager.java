package net.kingenchants.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.IronGolem;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;

import net.kingenchants.enchants.CustomEnchant;
import net.kingenchants.enchants.EnchantType;
import net.kingenchants.utils.enchants.EnchantUtils;

public class GuardianManager {

	public static ArrayList<Guardian> guardians = new ArrayList<Guardian>();
	private static List<Guardian> remove = new ArrayList<Guardian>();

	public static void removeGuardian(Guardian g) {
		remove.add(g);
	}

	public static Guardian spawn(IronGolem golem, Player spawner) {
		Guardian g = new Guardian(spawner, golem);
		guardians.add(g);
		return g;
	}

	public static void update(Guardian g) {
		g.update();
	}

	public static void changeOwner(IronGolem golem, Player newOwner) {
		for (Guardian g : guardians) {
			if (g.getGolem().getUniqueId().equals(golem.getUniqueId())) {

				g.setSpawner(newOwner);
				g.update();

			}
		}

	}

	public static void healSpawner(IronGolem golem) {
		for (Guardian g : guardians) {
			if (g.getGolem().getUniqueId().equals(golem.getUniqueId())) {
				if (!g.getSpawner().isOnline())
					continue;
				double health = g.getSpawner().getHealth();

				CustomEnchant ce = EnchantUtils.getArmorEnchant(g.getSpawner(), EnchantType.BLOOD_TRANSFUSION);
				if (ce != null) {
					Random rand = new Random();
					boolean two = ce.getLevel() == 3 || (rand.nextInt(1) < 1 && ce.getLevel() == 2);
					if (health <= 18) {
						g.getSpawner().setHealth(g.getSpawner().getHealth() + (two ? 2 : 1));
						g.getSpawner().sendMessage(ChatColor.GREEN + " +" + (two ? 2 : 1) + "HP - Blood Transfusion");
					} else if (health < 20) {
						g.getSpawner().setHealth(20);
						g.getSpawner().sendMessage(ChatColor.GREEN + " +" + (two ? 2 : 1) + "HP - Blood Transfusion");
					}
				}
			}
		}
	}

	public static void updateAll() {
		guardians.removeAll(remove);
		remove.clear();
		for (Guardian g : guardians) {
			g.update();
		}


	}

}

class Guardian {
	IronGolem golem;
	Player spawner;
	Player original;
	int secondsRemaining;
	String name;

	public void destroy() {
		if (golem.getHealth() < 10) {
			GuardianManager.removeGuardian(this);
			golem.setHealth(0);
			golem = null;
			spawner = null;
			original = null;
			secondsRemaining = -1;
			try {
				this.finalize();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	public Guardian(Player spawner, IronGolem golem) {
		this.original = spawner;
		this.name = ChatColor.YELLOW + "" + ChatColor.BOLD + spawner.getName() + "'s Guardian";
		this.golem = golem;
		this.spawner = spawner;
		this.secondsRemaining = 120;
	}

	public void update() {
		this.name = ChatColor.YELLOW + "" + ChatColor.BOLD + this.spawner.getName() + "'s Guardian";
		golem.setCustomName(this.name);
		golem.setCustomNameVisible(true);
		golem.setTarget(this.getTarget());
		this.secondsRemaining--;
		if (this.secondsRemaining <= 0) {
			this.golem.setHealth(0);
		}
		this.destroy();
	}

	private LivingEntity getTarget() {
		double low = 1000000.0D;
		Player target = null;

		Faction sf = FPlayers.getInstance().getByPlayer(spawner).getFaction();
		for (Entity e : golem.getNearbyEntities(100, 100, 100)) {
			if (!e.getType().equals(EntityType.PLAYER)) {
				continue;
			}
			double current = e.getLocation().distance(golem.getLocation());
			if (current < low) {
				Player ep = (Player) e;
				FPlayer fp = FPlayers.getInstance().getByPlayer(ep);
				Faction fac = fp.getFaction();
				if (!fac.getId().equals(sf.getId())) {
					target = ep;
					low = current;
				}
			}
		}
		return target;
	}

	public IronGolem getGolem() {
		return golem;
	}

	public void setGolem(IronGolem golem) {
		this.golem = golem;
	}

	public Player getSpawner() {
		return spawner;
	}

	public void setSpawner(Player spawner) {
		this.spawner = spawner;
	}

	public Player getOriginal() {
		return original;
	}

	public void setOriginal(Player original) {
		this.original = original;
	}

	public int getSecondsRemaining() {
		return secondsRemaining;
	}

	public void setSecondsRemaining(int secondsRemaining) {
		this.secondsRemaining = secondsRemaining;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}