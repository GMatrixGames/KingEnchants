package net.kingenchants.utils;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Zombie;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;

public class RuseManager {

	public static ArrayList<Ruse> zombies = new ArrayList<Ruse>();
	private static List<Ruse> remove = new ArrayList<Ruse>();

	public static void removeRuse(Ruse g) {
		remove.add(g);
	}

	public static Ruse spawn(Zombie zombie, Player spawner) {
		Ruse g = new Ruse(spawner, zombie);
		zombies.add(g);
		return g;
	}

	public static void update(Ruse g) {
		g.update();
	}

	public static void updateAll() {
		zombies.removeAll(remove);
		remove.clear();
		for (Ruse g : zombies) {
			g.update();
		}

	}

}

class Ruse {
	Zombie zombie;
	Player spawner;
	int secondsRemaining;

	public void destroy() {
		if (zombie.getHealth() < 10) {
			RuseManager.removeRuse(this);
			zombie.setHealth(0);
			zombie = null;
			spawner = null;
			secondsRemaining = -1;
			try {
				this.finalize();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	public Ruse(Player spawner, Zombie e) {
		this.secondsRemaining = 15;
		this.zombie = e;
		this.spawner = spawner;
	}

	public void update() {
		zombie.setTarget(getTarget());
		this.secondsRemaining--;
		if (this.secondsRemaining <= 0) {
			this.zombie.setHealth(0);
		}
		this.destroy();
	}

	private LivingEntity getTarget() {
		double low = 1000000.0D;
		Player target = null;

		Faction sf = FPlayers.getInstance().getByPlayer(spawner).getFaction();
		for (Entity e : zombie.getNearbyEntities(100, 100, 100)) {
			if (!e.getType().equals(EntityType.PLAYER)) {
				continue;
			}
			double current = e.getLocation().distance(zombie.getLocation());
			if (current < low) {
				Player ep = (Player) e;
				FPlayer fp = FPlayers.getInstance().getByPlayer(ep);
				Faction fac = fp.getFaction();
				if (!fac.getId().equals(sf.getId())) {
					target = ep;
					low = current;
				}
			}
		}
		return target;
	}

	public Zombie getGolem() {
		return zombie;
	}

	public void setGolem(Zombie zombie) {
		this.zombie = zombie;
	}

	public Player getSpawner() {
		return spawner;
	}

	public void setSpawner(Player spawner) {
		this.spawner = spawner;
	}

	public int getSecondsRemaining() {
		return secondsRemaining;
	}

	public void setSecondsRemaining(int secondsRemaining) {
		this.secondsRemaining = secondsRemaining;
	}

}