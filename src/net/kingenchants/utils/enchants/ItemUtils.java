package net.kingenchants.utils.enchants;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;


public class ItemUtils {

	public static ItemStack getHead(Player p, Player k, ItemStack weapon) {
		SkullMeta meta = (SkullMeta) Bukkit.getItemFactory().getItemMeta(Material.SKULL_ITEM);
		meta.setOwner(p.getName());
		ItemStack result = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
		meta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + p.getName() + "'s Head");

		result.addUnsafeEnchantment(Enchantment.ARROW_DAMAGE, 1);
		List<String> lore = new ArrayList<String>();

		lore.add(ChatColor.GRAY + "Killed by " + ChatColor.AQUA + k.getName());
		if (weapon.hasItemMeta() && weapon.getItemMeta().hasDisplayName())
			lore.add(ChatColor.GRAY + "Using " + ChatColor.AQUA + weapon.getItemMeta().getDisplayName());

		lore.add(ChatColor.GRAY + "On " + ChatColor.AQUA + formatDate());
		lore.add(ChatColor.GRAY + "At " + ChatColor.AQUA + locStr(p.getLocation()));
		lore.add(ChatColor.GRAY + "In " + ChatColor.AQUA + (p.getLocation().getWorld().getName().toLowerCase()));
		meta.setLore(lore);
		result.setItemMeta(meta);

		return result;
	}

	public static String locStr(Location l) {

		return "X: " + l.getBlockX() + " Y: " + l.getBlockY() + " Z: " + l.getBlockZ();

	}

	public static String formatDate() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		LocalDate localDate = LocalDate.now();
		return (dtf.format(localDate));
	}

}
