package net.kingenchants.utils.enchants;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.cryptolead.core.commons.MathUtils;
import org.cryptolead.core.commons.RandomUtils;
import org.cryptolead.core.utils.ChatUtils;
import org.cryptolead.core.utils.ChatUtils.Color;
import org.cryptolead.core.utils.RomanNumber;

import net.kingenchants.KingEnchants;
import net.kingenchants.enchants.AllowedItems;
import net.kingenchants.enchants.BookEnchant;
import net.kingenchants.enchants.CustomEnchant;
import net.kingenchants.enchants.DustRarity;
import net.kingenchants.enchants.EnchantRarity;
import net.kingenchants.enchants.EnchantType;
import net.kingenchants.enchants.ScrollType;
import net.kingenchants.utils.gui.GUIItem;
import net.md_5.bungee.api.ChatColor;

public class EnchantUtils {

	public static Entry<Double, Integer> fromBooster(ItemStack is) {
		if (is == null || !is.hasItemMeta() || !is.getItemMeta().hasDisplayName() || !is.getItemMeta().hasLore())
			return null;

		double percent = -1;
		int time = -1;

		if (is.getItemMeta().getDisplayName().equals(toBooster(0, 0).getItemMeta().getDisplayName())) {
			for (int i = 0; i < is.getItemMeta().getLore().size(); i++) {
				String line = ChatUtils.removeColors(is.getItemMeta().getLore().get(i));
				if (line == null)
					continue;

				if (line.equals("MULTIPLIER")) {
					String rawPercent = ChatUtils.removeColors(is.getItemMeta().getLore().get(i + 1)).replace("x", "");
					if (MathUtils.isDouble(rawPercent))
						percent = Double.parseDouble(rawPercent);
					else
						return null;
				}

				if (line.equals("DURATION")) {
					String rawTime = ChatUtils.removeColors(is.getItemMeta().getLore().get(i + 1)).replace("min", "");
					if (MathUtils.isInteger(rawTime))
						time = Integer.parseInt(rawTime);
					else
						return null;
				}
			}
		} else
			return null;

		return new SimpleEntry<>(percent, time);
	}

	public static ItemStack toBooster(double percent, int time) {
		GUIItem item = new GUIItem(new ItemStack(Material.EMERALD), -1);
		item.setName(Color.AQUA + Color.BOLD + "Faction XP Booster");
		item.addLore(Color.GRAY + "Multiplies XP gained by all Faction members\nwhile active.\n ");
		item.addLore(Color.GOLD + Color.BOLD + "MULTIPLIER");
		item.addLore(Color.GRAY + percent + "x");
		item.addLore(Color.GOLD + Color.BOLD + "DURATION");
		item.addLore(Color.GRAY + time + "min");
		item.addLore("\n" + Color.GRAY + Color.ITALIC + "Right-click to activate the XP Booster.");

		return item.getItem();
	}

	public static ItemStack removeWhiteScroll(ItemStack is) {
		ItemMeta im = is.getItemMeta();
		List<String> lore = im.getLore();
		if (lore == null)
			return is;
		for (int i = 0; i < lore.size(); i++) {
			String line = ChatUtils.removeColors(lore.get(i));
			while (line.startsWith(" "))
				line = line.replaceFirst(" ", "");

			if (line.equals("Protected"))
				lore.remove(i);
		}

		im.setLore(lore);
		is.setItemMeta(im);

		return is;
	}

	public static ItemStack removeEnchant(ItemStack is, EnchantType type) {
		ItemMeta im = is.getItemMeta();
		List<String> lore = im.getLore();
		if (lore == null)
			return is;
		for (int i = 0; i < lore.size(); i++) {
			String line = ChatUtils.removeColors(lore.get(i));
			while (line.startsWith(" "))
				line = line.replaceFirst(" ", "");

			if (line.startsWith(type.getName()))
				lore.remove(i);
		}

		im.setLore(lore);
		is.setItemMeta(im);

		return is;
	}

	public static ItemStack toScroll(ScrollType type) {
		GUIItem item = new GUIItem(type.getItem(), -1);
		item.setName(type.getColor() + type.getName());
		item.addLore(Color.GRAY + type.getDescription());

		return item.getItem();
	}

	public static ItemStack toBlackScroll(int percent) {
		ScrollType type = ScrollType.BLACK;
		GUIItem item = new GUIItem(type.getItem(), -1);
		item.setName(type.getColor() + type.getName());
		item.addLore(Color.GRAY + type.getDescription().replace("-NUMBER", String.valueOf(percent)));

		return item.getItem();
	}

	public static int fromBlackScroll(ItemStack is) {
		if (is == null || !is.hasItemMeta() || !is.getItemMeta().hasDisplayName() || !is.getItemMeta().hasLore())
			return -1;

		if (is.getItemMeta().getLore().size() < 3)
			return -1;

		String[] lore = ChatUtils.removeColors(is.getItemMeta().getLore().get(2)).split(" ");

		return Integer.parseInt(lore[3].replace("%", ""));
	}

	public static ScrollType fromScroll(ItemStack is) {
		if (is == null || !is.hasItemMeta() || !is.getItemMeta().hasDisplayName() || !is.getItemMeta().hasLore())
			return null;
		ScrollType type = null;
		String name = ChatUtils.removeColors(is.getItemMeta().getDisplayName());

		String[] names = name.split(" ");
		type = ScrollType.getScrollByFirstName(names[0]);
		return type;
	}

	public static CustomEnchant getArmorEnchant(Player player, EnchantType type) {
		CustomEnchant enchant = null;
		int level = -1;

		enchant = getEnchant(player.getInventory().getHelmet(), type);
		if (enchant != null)
			level = enchant.getLevel();

		CustomEnchant e = getEnchant(player.getInventory().getChestplate(), type);
		if (e != null && e.getLevel() > level) {
			level = e.getLevel();
			enchant = e;
		}

		e = getEnchant(player.getInventory().getLeggings(), type);
		if (e != null && e.getLevel() > level) {
			level = e.getLevel();
			enchant = e;
		}

		e = getEnchant(player.getInventory().getBoots(), type);
		if (e != null && e.getLevel() > level) {
			level = e.getLevel();
			enchant = e;
		}

		if (level > 0 && enchant != null)
			return enchant;

		return null;
	}

	public static CustomEnchant getEnchant(ItemStack is, EnchantType type) {
		if (is == null)
			return null;

		Entry<List<CustomEnchant>, Boolean> enchants = getEnchants(is);
		if (enchants == null)
			return null;

		for (CustomEnchant enchant : enchants.getKey())
			if (enchant.getType().equals(type))
				return enchant;

		return null;
	}

	public static int stackArmorEnchant(Player player, EnchantType type) {

		CustomEnchant enchant = null;
		boolean found = false;
		int level = 0;

		enchant = getEnchant(player.getInventory().getHelmet(), type);
		if (enchant != null) {
			level += enchant.getLevel();
			found = true;
		}
		CustomEnchant e = getEnchant(player.getInventory().getChestplate(), type);
		if (e != null && e.getLevel() > level) {
			level += e.getLevel();
			found = true;
		}

		e = getEnchant(player.getInventory().getLeggings(), type);
		if (e != null && e.getLevel() > level) {
			level += e.getLevel();
			found = true;
		}

		e = getEnchant(player.getInventory().getBoots(), type);
		if (e != null && e.getLevel() > level) {
			level += e.getLevel();
			found = true;
		}
		if (found)
			return level;
		return -1;
	}

	public static ItemStack toBaseDust() {
		GUIItem item = new GUIItem(new ItemStack(Material.FIREBALL), -1);
		item.setName(ChatColor.AQUA + "" + ChatColor.BOLD + "Secret Dust");

		item.addLore("&7This secret dust will give you a dust from &bCommon&7, &aUncommon&7, &7and &dRoyal."
				.replaceAll("&", "�"));
		item.addLore(ChatColor.GRAY + "The dust will be used to increase success rate on items.");
		item.addLore(" ");
		item.addLore(ChatColor.GRAY + "" + ChatColor.ITALIC + "Right-click to reveal the dust type and percent.");

		return item.getItem();
	}

	public static EnchantRarity fromBaseDust(ItemStack is) {
		if (is == null || !is.getType().equals(Material.FIREBALL) || !is.hasItemMeta()
				|| !is.getItemMeta().hasDisplayName() || !is.getItemMeta().hasLore())
			return null;

		EnchantRarity rarity = EnchantRarity.BASIC;
		if (!is.getItemMeta().getDisplayName().contains(ChatColor.AQUA + "" + ChatColor.BOLD + "Secret Dust"))
			return null;
		return rarity;
	}

	public static Entry<ItemStack, Boolean> getRandomSuccessDust() {
		if (RandomUtils.randomInt(100) < KingEnchants.SUCCESS_DUST_RATE) {
			DustRarity rarity = null;
			Random rand = new Random();
			int next = rand.nextInt(3);
			for (DustRarity ra : DustRarity.values()) {
				if (ra.getValue() == next)
					rarity = ra;
			}
			if (rarity == null) {
				GUIItem item = new GUIItem(new ItemStack(Material.SULPHUR), -1);
				item.setName(Color.WHITE + "Mystery Dust");
				item.addLore(Color.GRAY + "This enchant dust has failed!");
				return new SimpleEntry<>(item.getItem(), false);
			}

			int percent = RandomUtils.randomInt(rarity.getMinimumDust(), rarity.getMaximumDust());

			GUIItem item = new GUIItem(new ItemStack(Material.GLOWSTONE_DUST), -1);
			item.setName(rarity.getColor() + rarity.getName() + " Dust");
			item.addLore(Color.GREEN + "" + ChatColor.BOLD + "+" + percent + "% Success Rate");
			item.addLore(ChatColor.GRAY + "It will increase the success rate of an item to " + rarity.getColor()
					+ percent + "%");
			item.addLore(" ");
			item.addLore(ChatColor.GRAY + "" + ChatColor.ITALIC + "Apply onto item to increase the success rate.");
			// &7It will increase the success rate of an item to &(x) XXX%
			return new SimpleEntry<>(item.getItem(), true);
		} else {
			GUIItem item = new GUIItem(new ItemStack(Material.SULPHUR), -1);
			item.setName(Color.WHITE + "Mystery Dust");
			item.addLore(Color.GRAY + "This enchant dust has failed!");
			return new SimpleEntry<>(item.getItem(), false);
		}
	}

	public static ItemStack getRandomSuccessDustStack(int amount, DustRarity rarity, int success) {
		if (success == -1)
			success = RandomUtils.randomInt(rarity.getMinimumDust(), rarity.getMaximumDust());
		GUIItem item = new GUIItem(new ItemStack(Material.GLOWSTONE_DUST, amount), -1);
		item.setName(rarity.getColor() + rarity.getName() + " Dust");
		item.addLore(Color.GREEN + "" + ChatColor.BOLD + "+" + success + "% Success Rate");
		item.addLore(ChatColor.GRAY + "It will increase the success rate of an item to " + rarity.getColor() + success
				+ "%");
		item.addLore(" ");
		item.addLore(ChatColor.GRAY + "" + ChatColor.ITALIC + "Apply onto item to increase the success rate.");
		item.getItem();
		return item.getItem();
	}

	public static int fromSuccessDust(ItemStack is) {
		if (is == null || !is.getType().equals(Material.GLOWSTONE_DUST) || !is.hasItemMeta()
				|| !is.getItemMeta().hasDisplayName() || !is.getItemMeta().hasLore())
			return -1;

		String lore = ChatUtils.removeColors(is.getItemMeta().getLore().get(0)).replace("+", "")
				.replace("% Success Rate", "");
		String[] line = lore.split(" ");

		if (!MathUtils.isInteger(line[0]))
			return -1;

		return Integer.parseInt(line[0]);
	}

	public static ItemStack sortEnchants(ItemStack is, List<CustomEnchant> enchants, boolean hasScroll) {
		enchants.sort(new Comparator<CustomEnchant>() {

			@Override
			public int compare(CustomEnchant o1, CustomEnchant o2) {
				return o2.getType().getRarity().getValue() - o1.getType().getRarity().getValue();
			}
		});

		ItemStack item = is;
		ItemMeta im = item.getItemMeta();

		im = item.getItemMeta();
		List<String> lore = new ArrayList<>();
		lore.add(Color.GRAY + "Enchants: " + Color.YELLOW + enchants.size());

		if (hasScroll)
			lore.add(Color.WHITE + Color.BOLD + "Protected");

		if (hasOrb(is))
			lore.add(Color.WHITE + Color.BOLD + "+1 Enchant");

		lore.add(" ");

		for (CustomEnchant enchant : enchants)
			lore.add(enchant.getType().getRarity().getColor() + enchant.getType().getName() + " "
					+ RomanNumber.toRoman(enchant.getLevel()));

		im.setLore(lore);
		item.setItemMeta(im);

		return item;
	}

	public static ItemStack addEnchant(ItemStack is, CustomEnchant enchant) {
		if (is == null)
			return null;

		ItemMeta im = is.getItemMeta();
		List<String> lore = im.getLore();
		if (lore == null)
			lore = new ArrayList<>();
		lore.add(enchant.getType().getRarity().getColor() + enchant.getType().getName() + " "
				+ RomanNumber.toRoman(enchant.getLevel()));
		im.setLore(lore);
		is.setItemMeta(im);

		return is;
	}

	public static ItemStack addWhiteScroll(ItemStack is) {
		if (is == null)
			return null;

		ItemMeta im = is.getItemMeta();
		List<String> lore = im.getLore();
		if (lore == null)
			lore = new ArrayList<>();
		lore.add(Color.WHITE + Color.BOLD + "Protected");
		im.setLore(lore);
		is.setItemMeta(im);

		return is;
	}

	public static ItemStack addOrb(ItemStack is) {
		if (is == null)
			return null;

		ItemMeta im = is.getItemMeta();
		List<String> lore = im.getLore();
		if (lore == null)
			lore = new ArrayList<>();
		lore.add(Color.WHITE + Color.BOLD + "+1 Enchant");
		im.setLore(lore);
		is.setItemMeta(im);

		return is;
	}

	// Gets CustomEnchant and has WhiteScroll from EnchantedItem.
	public static Entry<List<CustomEnchant>, Boolean> getEnchants(ItemStack is) {
		if (is == null)
			return null;

		List<CustomEnchant> enchants = new ArrayList<>();
		boolean hasWhiteScroll = false;

		if (is.hasItemMeta() && is.getItemMeta().hasLore())
			for (String line : is.getItemMeta().getLore()) {
				String rawLine = ChatUtils.removeColors(line);

				if (rawLine.equalsIgnoreCase("Protected"))
					hasWhiteScroll = true;

				String[] args = rawLine.split(" ");
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < args.length - 1; i++) {
					sb.append(args[i]);

					if (i < args.length - 2)
						sb.append(" ");
				}

				EnchantType type = EnchantType.getTypeByName(sb.toString());
				if (type == null)
					continue;

				enchants.add(new CustomEnchant(type, RomanNumber.fromRoman(args[args.length - 1])));
			}

		return new SimpleEntry<>(enchants, hasWhiteScroll);
	}
	
	public static boolean hasOrb(ItemStack is) {
		if (is == null)
			return false;

		boolean hasOrb = false;

		if (is.hasItemMeta() && is.getItemMeta().hasLore())
			for (String line : is.getItemMeta().getLore()) {
				String rawLine = ChatUtils.removeColors(line);

				if (rawLine.equalsIgnoreCase("+1 Enchant")) {
					hasOrb = true;
					break;
				}
			}

		return hasOrb;
	}

	// Gets Random BookEnchant from Rarity and Level
	public static BookEnchant getRandom(EnchantRarity rarity, int level) {
		List<EnchantType> types = new ArrayList<>();
		for (EnchantType type : EnchantType.values())
			if (type.getRarity().equals(rarity))
				types.add(type);

		if (types.size() == 0)
			return null;

		EnchantType type = types.size() == 1 ? types.get(0) : types.get(RandomUtils.randomInt(types.size()));

		return new BookEnchant(
				new CustomEnchant(type,
						level == -1 ? RandomUtils.randomInt(1, type.getMaximumLevel())
								: (level > type.getMaximumLevel() ? type.getMaximumLevel() : level)),
				RandomUtils.randomInt(0, 100), RandomUtils.randomInt(0, 100));
	}

	// Gets Book from BookEnchant.
	public static ItemStack toBook(BookEnchant enchant) {
		int length = 75;

		GUIItem item = new GUIItem(new ItemStack(Material.ENCHANTED_BOOK), -1);
		item.setName(enchant.getEnchant().getType().getRarity().getColor() + enchant.getEnchant().getType().getName()
				+ " " + RomanNumber.toRoman(enchant.getEnchant().getLevel()));

		String line = Color.GREEN + enchant.getSuccessRate() + "% Success Rate";
		item.addLore(ChatUtils.getSpaced(length, line));

		line = Color.RED + enchant.getDestroyRate() + "% Destroy Rate";
		item.addLore(ChatUtils.getSpaced(length, line));

		line = Color.GOLD + Color.STRIKETHROUGH + ChatUtils.multiply("-", 20) + Color.RESET;
		item.addLore(ChatUtils.getSpaced(length, line));

		int maxChars = 30;
		int current = 0;
		String message = "";
		for (String l : (Color.GRAY + enchant.getEnchant().getType().getDescription()).split(" ")) {
			current += l.length() + 1;
			message += l + " ";
			if (current > maxChars) {
				item.addLore(ChatUtils.getSpaced(length, message));
				current = 0;
				message = "";

			}
		}
		if (current > 0) {
			item.addLore(ChatUtils.getSpaced(length, message));
		}
		line = Color.GOLD + Color.STRIKETHROUGH + ChatUtils.multiply("-", 20) + Color.RESET;
		item.addLore(ChatUtils.getSpaced(length, line));

		AllowedItems allowed = enchant.getEnchant().getType().getAllowedItems();

		line = Color.GOLD + (allowed == null ? "Unknown" : ChatUtils.removeColors(allowed.getName())) + " Enchant";
		item.addLore(ChatUtils.getSpaced(length, line));

		line = Color.GRAY + Color.ITALIC + "Drag and drop";
		item.addLore(ChatUtils.getSpaced(length, line));

		line = Color.GRAY + "" + ChatColor.ITALIC + "onto item to apply";
		item.addLore(ChatUtils.getSpaced(length, line));

		return item.getItem();
	}

	// Gets BookEnchant from Book.
	public static BookEnchant fromBook(ItemStack is) {
		if (is == null || !is.getType().equals(Material.ENCHANTED_BOOK) || !is.hasItemMeta()
				|| !is.getItemMeta().hasDisplayName() || !is.getItemMeta().hasLore())
			return null;

		int success = -1;
		int destroy = -1;
		String enchantType = null;
		int level = -1;

		for (String line : is.getItemMeta().getLore()) {
			if (line == null || line.equals(""))
				continue;
			line = ChatUtils.removeColors(line);
			while (line.startsWith(" "))
				line = line.replaceFirst(" ", "");

			if (line.contains("% Success Rate")) {
				String rawSuccess = line.replace("% Success Rate", "").replace(" ", "");
				if (!MathUtils.isInteger(rawSuccess))
					return null;
				success = Integer.parseInt(rawSuccess);

				continue;
			}

			if (line.contains("% Destroy Rate")) {
				String rawDestroy = line.replace("% Destroy Rate", "").replace(" ", "");
				if (!MathUtils.isInteger(rawDestroy))
					return null;

				destroy = Integer.parseInt(rawDestroy);

				continue;
			}
		}

		String name = ChatUtils.removeColors(is.getItemMeta().getDisplayName());
		String[] names = name.split(" ");
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < names.length - 1; i++) {
			sb.append(names[i]);

			if (i < names.length - 2)
				sb.append(" ");
		}

		enchantType = sb.toString();

		String rawLevel = names[names.length - 1];

		level = RomanNumber.fromRoman(rawLevel);

		EnchantType type = EnchantType.getTypeByName(enchantType);
		if (type == null)
			return null;

		if (success == -1)
			return null;
		if (destroy == -1)
			return null;
		return new BookEnchant(new CustomEnchant(type, level), success, destroy);
	}

	// Gets Book from Rarity and Level.
	public static ItemStack toBaseBook(EnchantRarity rarity, int level) {
		GUIItem item = new GUIItem(new ItemStack(Material.BOOK), -1);
		if (level == -1)
			item.setName(rarity.getColor() + Color.BOLD + rarity.getName() + " Enchantment Book");
		else
			item.setName(rarity.getColor() + rarity.getName() + " Book " + RomanNumber.toRoman(level));
		item.addLore(Color.GRAY + "Examine to recieve a random\n" + rarity.getColor() + rarity.getName().toLowerCase()
				+ Color.GRAY + " enchantment book.");
		return item.getItem();
	}

	// Gets Rarity and Level from Base Book.
	public static Entry<EnchantRarity, Integer> fromBaseBook(ItemStack is) {
		if (is == null || !is.getType().equals(Material.BOOK) || !is.hasItemMeta()
				|| !is.getItemMeta().hasDisplayName() || !is.getItemMeta().hasLore())
			return null;

		EnchantRarity rarity = null;
		int level = -1;

		String name = ChatUtils.removeColors(is.getItemMeta().getDisplayName()).replace(" Enchantment Book", "");
		String[] names = name.split(" ");

		rarity = EnchantRarity.getRarity(names[0]);
		if (rarity == null)
			return null;

		if (names.length > 1)
			level = RomanNumber.fromRoman(names[1]);

		return new SimpleEntry<>(rarity, level);
	}

	// Merges two books to one.
	public static BookEnchant mergeBooks(BookEnchant book0, BookEnchant book1) {
		if (book0.getEnchant() == null || book1.getEnchant() == null || !book0.getEnchant().equals(book1.getEnchant()))
			return null;

		if (book0.getEnchant().getLevel() + 1 > book0.getEnchant().getType().getMaximumLevel())
			return null;

		return new BookEnchant(new CustomEnchant(book0.getEnchant().getType(), book0.getEnchant().getLevel() + 1),
				(int) Math.round((book0.getSuccessRate() + book1.getSuccessRate()) / 2),
				(int) Math.round((book0.getDestroyRate() + book1.getDestroyRate()) / 2));
	}
}