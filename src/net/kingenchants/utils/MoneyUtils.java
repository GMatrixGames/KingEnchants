package net.kingenchants.utils;

import org.bukkit.entity.Player;

import net.kingenchants.data.Temp;

public class MoneyUtils {
	
	Player p;
	String type;
	
	public static final String
		VAULT = "vault",
		EXP = "exp";
	
	public MoneyUtils(Player p) {
		this.p = p;
		this.type = EXP;
	}
	
	public MoneyUtils(Player p, String type) {
		this.p = p;
		this.type = type;
	}
	
	@SuppressWarnings("deprecation")
	public void setMoney(int amount) {
		if (type.equals(VAULT))
			Temp.ECON.depositPlayer(this.p.getName(), amount - getMoney());
		else
			this.p.setLevel(amount);
	}
	
	public void giveMoney(int amount) {
		setMoney(getMoney() + amount);
	}
	
	public boolean takeMoney(int amount) {
		if (getMoney() < amount)
			return false;
		
		if (type.equals(VAULT))
			Temp.ECON.withdrawPlayer(this.p, amount);
		else
			this.p.setLevel(getMoney() - amount);
		
		return true;
	}
	
	@SuppressWarnings("deprecation")
	public int getMoney() {
		if (type.equals(VAULT))
			return (int) Temp.ECON.getBalance(this.p.getName());
		else
			return (int) this.p.getLevel();
	}

}
