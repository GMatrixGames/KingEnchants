package net.kingenchants.utils;

import java.util.HashMap;

import org.bukkit.entity.Player;

import net.kingenchants.enchants.listeners.entity.Listener_EntityDamageByEntity;

public class SecondClock implements Runnable {

	@Override
	public void run() {

		GuardianManager.updateAll();
		RuseManager.updateAll();
		
		runBleed();
		runPacify();
		runSilence();
	}

	private void runSilence() {
		HashMap<Player, Integer> silence = new HashMap<Player, Integer>();
		for (Player p : Listener_EntityDamageByEntity.silence.keySet()) {
			int val = Listener_EntityDamageByEntity.silence.get(p) - 1;
			if (val != 0) {
				silence.put(p, val);
			}
		}
		Listener_EntityDamageByEntity.silence = silence;

	}

	private void runPacify() {
		HashMap<Player, Integer> pacify = new HashMap<Player, Integer>();
		for (Player p : Listener_EntityDamageByEntity.pacify.keySet()) {
			int val = Listener_EntityDamageByEntity.pacify.get(p) - 1;
			if (val != 0) {
				pacify.put(p, val);
			}
		}
		Listener_EntityDamageByEntity.pacify = pacify;

	}

	private void runBleed() {
		HashMap<Player, Integer> newBleed = new HashMap<Player, Integer>();
		for (Player p : Listener_EntityDamageByEntity.bleed.keySet()) {
			int bleedstack = Listener_EntityDamageByEntity.bleed.get(p);
			if (bleedstack != 1) {
				p.setWalkSpeed((float) (0.2 - 0.03 * bleedstack));
				newBleed.put(p, bleedstack - 1);
			} else {
				p.setWalkSpeed((float) 0.2);
			}
		}
		Listener_EntityDamageByEntity.bleed = newBleed;

	}

}
