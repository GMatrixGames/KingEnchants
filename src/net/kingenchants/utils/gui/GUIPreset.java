package net.kingenchants.utils.gui;

import org.bukkit.entity.Player;

import net.kingenchants.data.Temp;

public abstract class GUIPreset {
	
	 private String
	 	id; // The ID of the GUI.
	 
	 public GUIPreset(String id) {
		 this.id = id;
		 
		 Temp.GUI_PRESETS.add(this);
	 }
	 
	 /**
	  * Gets the ID of the GUI.
	  * @return the ID.
	  */
	 public String getID() {
		 return id;
	 }
	 
	 /**
	  * Gets the GUI for a player.
	  * @param player the player.
	  * @param args the arguments of the GUI.
	  * @return the GUI.
	  */
	 public abstract GUI getGUI(Player player, Object... args);
	 
	 /**
	  * Opens the GUI for a player.
	  * @param player the player.
	  * @param args the arguments of the GUI.
	  */
	 public void openGUI(Player player, Object... args) {
		 GUI gui = getGUI(player, args);
		 if (gui != null)
			 gui.open(player);
	 }
}