package net.kingenchants.utils.gui;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import net.kingenchants.events.CEvent;

public class GUIClickEvent extends CEvent implements Cancellable {
	
	private Player
		player;
	
	private GUI
		gui;
	
	private ClickType
		clickType;
	
	private int
		slot;
	
	private ItemStack
		item;
	
	private InventoryClickEvent
		originalEvent;
	
	private boolean
		isCancelled;
	
	public GUIClickEvent(Player player, GUI gui, ClickType clickType, int slot, ItemStack item, InventoryClickEvent originalEvent) {
		this.player = player;
		this.gui = gui;
		this.clickType = clickType;
		this.slot = slot;
		this.item = item;
		this.originalEvent = originalEvent;
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public GUI getGUI() {
		return gui;
	}
	
	public ClickType getClickType() {
		return clickType;
	}
	
	public int getSlot() {
		return slot;
	}
	
	public ItemStack getItem() {
		return item;
	}
	
	public InventoryClickEvent getOriginalEvent() {
		return originalEvent;
	}
	
	@Override
	public boolean isCancelled() {
		return isCancelled;
	}

	@Override
	public void setCancelled(boolean isCancelled) {
		this.isCancelled = isCancelled;
	}
}