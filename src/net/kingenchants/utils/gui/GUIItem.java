package net.kingenchants.utils.gui;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.cryptolead.core.chat.ChatManager;
import org.cryptolead.core.chat.Msg;
import org.cryptolead.core.commons.ID;
import org.cryptolead.core.commons.Validate;

public class GUIItem {

	private String id, leftClickID, rightClickID;

	private int slot;

	private ItemStack item;

	private Entry<Msg, Object[]> name;

	private List<Entry<Msg, Object[]>> lore = new ArrayList<>();

	private List<GUIItemClick> clickListeners = new ArrayList<>();

	public GUIItem(String id, ItemStack item, int slot, Msg name, List<Msg> lore) {
		this.id = id == null ? new ID().toString() : id;
		this.item = item;
		this.slot = slot < 0 ? 0 : slot;
		this.name = new SimpleEntry<>(name, new Object[0]);

		if (lore != null) {
			this.lore.clear();
			for (Msg text : lore)
				this.lore.add(new SimpleEntry<>(text, new Object[0]));
		}
	}

	public GUIItem(ItemStack item, int slot) {
		this(null, item, slot, new Msg(item.getItemMeta().getDisplayName()), null);
	}

	public GUIItem(ItemStack item, int line, int number) {
		this(item, line * 9 + number);
	}

	public String getID() {
		return id;
	}

	public int getSlot() {
		return slot;
	}

	public ItemStack getRawItem() {
		return item;
	}

	public Msg getName() {
		if (name == null)
			return null;
		return name.getKey();
	}

	public List<Msg> getLore() {
		if (this.lore == null)
			return null;

		List<Msg> lore = new ArrayList<>();
		for (Entry<Msg, Object[]> e : this.lore)
			lore.add(e.getKey());

		return lore;
	}

	public List<GUIItemClick> getClickListeners() {
		return clickListeners;
	}

	public ItemStack getItem(Player player) {
		ItemStack is = item.clone();

		ItemMeta im = is.getItemMeta();
		if (name != null && name.getKey() != null)
			im.setDisplayName(
					ChatManager.process(name.getKey(), player == null ? null : player.getUniqueId(), name.getValue()));

		List<String> newLore = new ArrayList<>();
		for (Entry<Msg, Object[]> e : lore) {
			if (e.getKey() == null)
				continue;
			newLore.add(ChatManager.process(e.getKey(), player == null ? null : player.getUniqueId(), e.getValue()));
		}

		im.setLore(newLore);

		is.setItemMeta(im);

		return is;
	}

	public ItemStack getItem() {
		return getItem(null);
	}

	public String getLeftClickID() {
		return leftClickID;
	}

	public String getRightClickOD() {
		return rightClickID;
	}

	public GUIItem setSlot(int slot) {
		this.slot = slot;

		return this;
	}

	public GUIItem setSlot(int line, int number) {
		return setSlot(line * 9 + number);
	}

	public GUIItem setName(Msg name, Object... args) {
		this.name = new SimpleEntry<>(name, args);

		return this;
	}

	public GUIItem setName(String name, Object... args) {
		return setName(new Msg(name), args);
	}

	public GUIItem setLore(List<Msg> lore) {
		this.lore.clear();
		for (Msg text : lore)
			this.lore.add(new SimpleEntry<>(text, new Object[0]));

		return this;
	}

	public GUIItem setLoreArgs(List<Entry<Msg, Object[]>> lore) {
		this.lore = lore == null ? new ArrayList<>() : lore;
		return this;
	}

	public GUIItem setLore(int line, Msg lore, Object... args) {
		this.lore.set(line, new SimpleEntry<>(lore, args));

		return this;
	}

	public GUIItem setLore(int line, String lore, Object... args) {
		return setLore(line, new Msg(lore), args);
	}

	public GUIItem addLore(Msg lore, Object... args) {
		this.lore.add(new SimpleEntry<>(lore, args));

		return this;
	}

	public GUIItem addLore(String lore) {
		for (String l : lore.split("\n"))
			addLore(new Msg(l));


		return this;
	}

	public GUIItem setItem(ItemStack item) {
		Validate.notNull(item, "item +-");

		this.item = item;

		return this;
	}

	public GUIItem setType(Material type) {
		Validate.notNull(type, "type +-");

		this.item.setType(type);

		return this;
	}

	public GUIItem setAmount(int amount) {
		this.item.setAmount(amount);

		return this;
	}

	public GUIItem setClickListeners(List<GUIItemClick> clickListeners) {
		this.clickListeners = clickListeners == null ? new ArrayList<>() : clickListeners;

		return this;
	}

	public GUIItem addClickListener(GUIItemClick clickListener) {
		clickListeners.add(clickListener);

		return this;
	}

	public GUIItem setClickID(String clickID, Object... args) {
		setLeftClickID(clickID, args);
		setRightClickID(clickID, args);

		return this;
	}

	public GUIItem setLeftClickID(String leftClickID, Object... args) {
		this.leftClickID = leftClickID;

		clickListeners.add(new GUIItemClick() {

			@Override
			public void click(GUIItem item, Player player, InventoryAction action) {
				if (action.equals(InventoryAction.PICKUP_ALL))
					GUI.getPreset(leftClickID).openGUI(player, args);
			}
		});

		return this;
	}

	public GUIItem setRightClickID(String rightClickID, Object... args) {
		this.rightClickID = rightClickID;

		clickListeners.add(new GUIItemClick() {

			@Override
			public void click(GUIItem item, Player player, InventoryAction action) {
				if (action.equals(InventoryAction.PICKUP_HALF))
					GUI.getPreset(rightClickID).openGUI(player, args);
			}
		});

		return this;
	}

	public GUIItem clone() {
		GUIItem item = new GUIItem(id, this.item, slot, name == null ? null : name.getKey(), getLore())
				.setClickListeners(clickListeners);
		item.setName(name.getKey(), name.getValue());
		item.setLoreArgs(lore);

		return item;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof GUIItem))
			return false;

		return ((GUIItem) obj).getID().equals(id);
	}
}