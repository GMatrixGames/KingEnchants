package net.kingenchants.utils.gui;

import java.util.List;

import org.cryptolead.core.commons.Validate;

public class GUIUtils {
	
	/**
	 * Inserting items to a GUI with certain space.
	 * @param gui the GUI.
	 * @param items the items.
	 * @param space the amount of space between items.
	 * @return needed lines.
	 */
	public static int insert(GUI gui, List<GUIItem> items, int space) {
		Validate.notNull(gui, "gui +-");
		Validate.notNull(items, "items +-");
		
		if (space < 0)
			space = 0;
		
		int line = 1;
		int slot = 1;
		for (GUIItem item : items) {
			item.setSlot(line * 9 + slot);
			gui.addItem(item);
			
			slot += space + 1;
			if (slot >= 8) {
				slot = 1;
				line += space == 0 ? 1 : 2;
			}
			
			if (line * 9 + slot > 42)
				break;
		}
		
		int lines = Math.round((line * 9 + slot) / 9) + 2;
		return lines < 3 ? 3 : lines;
	}
}