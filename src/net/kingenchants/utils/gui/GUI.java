package net.kingenchants.utils.gui;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.cryptolead.core.chat.ChatManager;
import org.cryptolead.core.chat.Msg;
import org.cryptolead.core.commons.ID;
import org.cryptolead.core.commons.Validate;

import net.kingenchants.data.Temp;

public class GUI {
	
	private String
		id;
	
	private int
		lines,
		backSlot = -1;
	
	private Entry<Msg, Object[]>
		name;
	
	private List<GUIItem>
		items = new ArrayList<>();
	
	private boolean
		isClickable;
	
	public GUI(String id, Msg name, int lines, boolean isClickable) {
		this.id = id == null ? new ID().toString() : id;
		this.name = new SimpleEntry<>(name, new Object[0]);
		this.lines = lines < 1 ? 1 : lines;
		this.isClickable = isClickable;
	}
	
	public GUI(Inventory base) {
		Validate.notNull(base, "base +-");
		
		this.id = new ID().toString();
		lines = base.getSize() / 9;
		this.name = new SimpleEntry<>(new Msg(base.getTitle()), new Object[0]);
	}
	
	public GUI(String name, int lines) {
		this(name, lines, false);
	}
	
	public GUI(String name, int lines, boolean isClickable) {
		this(null, new Msg(name), lines, isClickable);
	}
	
	public GUI(Msg name, int lines) {
		this(name, lines, false);
	}
	
	public GUI(Msg name, int lines, boolean isClickable) {
		this(null, name, lines, isClickable);
	}
	
	public String getID() {
		return id;
	}
	
	public int getLines() {
		return lines;
	}
	
	public int getBackSlot() {
		return backSlot;
	}
	
	public Msg getName() {
		if (name == null)
			return null;
		return name.getKey();
	}
	
	public List<GUIItem> getItems() {
		return items;
	}
	
	public boolean isClickable() {
		return isClickable;
	}
	
	public GUIItem getItem(String id) {
		Validate.notEmpty(id, "id +-");
		
		for (GUIItem item : items)
			if (item.getID().equals(id))
				return item;
		
		return null;
	}
	
	public GUI setLines(int lines) {
		this.lines = lines;
		
		return this;
	}
	
	public GUI setBackSlot(int backSlot) {
		this.backSlot = backSlot;
		
		return this;
	}
	
	public GUI setBackSlot(int line, int number) {
		this.backSlot = line * 9 + number;
		
		return this;
	}
	
	public GUI setBackSlot() {
		this.backSlot = (lines -1) * 9;
		
		return this;
	}
	
	public GUI setName(Msg name, Object... args) {
		this.name = new SimpleEntry<>(name, args);
		
		return this;
	}
	
	public GUI setName(String name, Object... args) {
		return setName(new Msg(name), args);
	}
	
	public GUI setItems(List<GUIItem> items) {
		this.items = items == null ? new ArrayList<>() : items;
		
		return this;
	}
	
	public GUI addItem(GUIItem item) {
		items.add(item.clone());
		
		return this;
	}
	
	public GUI setClickable(boolean isClickable) {
		this.isClickable = isClickable;
		
		return this;
	}
	
	public void open(Player player) {
		Validate.notNull(player, "player +-");
		
		Inventory inv = Bukkit.createInventory(player, (lines < 0 ? 1 : lines) * 9, (name == null || name.getKey() == null) ? "" : ChatManager.process(name.getKey(), player.getUniqueId(), name.getValue()));
		
		// Adding items:
		for (GUIItem item : items)
			if (item.getSlot() < inv.getSize())
				inv.setItem(item.getSlot(), item.getItem(player));
		
		// Adding back item:
		if (backSlot >= 0 && backSlot < inv.getSize() - 1) {
			GUIItem backItem = new GUIItem(new ItemStack(Material.ARROW), backSlot);
			backItem.setName(GUIsAPI.ITEM_BACK_NAME);
			backItem.addLore(GUIsAPI.ITEM_BACK_CLICK);
			
			inv.setItem(backItem.getSlot(), backItem.getItem(player));
		}
		
		// Setting history:
		GUI last = Temp.OPEN_GUIS.get(player.getUniqueId());
		if (last != null)
			Temp.LAST_GUIS.put(player.getUniqueId(), last);
		
		player.openInventory(inv);
		
		Temp.OPEN_GUIS.put(player.getUniqueId(), this);
	}
	
	public void clicked(Player player, int slot, InventoryAction action) {
		if (slot == backSlot) {
			if (!action.equals(InventoryAction.PICKUP_ALL))
				return;
			
			GUI.back(player);
			
			return;
		}
		
		for (GUIItem item : items)
			if (item.getSlot() == slot)
				for (GUIItemClick click : item.getClickListeners())
					click.click(item, player, action);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof GUI))
			return false;
		
		return ((GUI) obj).getID().equals(id);
	}
	
	
	/**
	 * Moving a player to the previous GUI.
	 * @param player the player.
	 */
	public static void back(Player player) {
		Validate.notNull(player, "player +-");
		
		GUI gui = Temp.LAST_GUIS.get(player.getUniqueId());
		if (gui == null)
			return;
		
		Temp.LAST_GUIS.remove(player.getUniqueId());
		
		gui.open(player);
	}
	
	/**
	 * Gets a GUI preset.
	 * @param id the ID of the preset.
	 * @return the preset. Null if not found.
	 */
	public static GUIPreset getPreset(String id) {
		for (GUIPreset preset : Temp.GUI_PRESETS)
			if (preset.getID().equals(id))
				return preset;
		
		return null;
	}
	
	/**
	 * Opens a GUI preset.
	 * @param player the player.
	 * @param id the ID of the preset.
	 * @param args the arguments of the GUI.
	 * @return the preset. Null if not found.
	 */
	public static void openPreset(Player player, String id, Object... args) {
		GUIPreset preset = getPreset(id);
		if (preset != null)
			preset.openGUI(player, args);
	}
}