package net.kingenchants.utils.gui;

import org.cryptolead.core.chat.Msg;
import org.cryptolead.core.utils.ChatUtils.Color;

public class GUIsAPI {
	
	public static Msg
		ITEM_BACK_NAME = 					new Msg(Color.GRAY + Color.BOLD + "Back"),
		ITEM_BACK_CLICK = 					new Msg("Left-click to move back");
}