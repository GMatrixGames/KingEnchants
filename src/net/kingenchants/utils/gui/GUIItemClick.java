package net.kingenchants.utils.gui;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;

public interface GUIItemClick {
	
	void click(GUIItem item, Player player, InventoryAction action);
}