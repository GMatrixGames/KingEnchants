package net.kingenchants.utils;

import org.bukkit.scheduler.BukkitRunnable;

import net.kingenchants.KingEnchants_Loader;

public abstract class CRunnable extends BukkitRunnable {
	
	public int
		timer = -1;
	
	/**
	 * Activating the task.
	 * @param period the task's period.
	 * @param timer the task's timer.
	 * @return 
	 */
	public CRunnable activate(int period, int timer) {
		runTaskTimer(KingEnchants_Loader.getInstance(), 0, period);
		this.timer = timer;
		
		return this;
	}
	
	/**
	 * Activating the task.
	 * @param delay the task's delay.
	 * @param period the task's period.
	 * @param timer the task's timer.
	 */
	public CRunnable activate(int delay, int period, int timer) {
		runTaskTimer(KingEnchants_Loader.getInstance(), delay, period);
		this.timer = timer;
		
		return this;
	}
}