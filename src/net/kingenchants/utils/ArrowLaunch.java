package net.kingenchants.utils;

import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.inventory.ItemStack;

public class ArrowLaunch {
	Projectile projectile;
	Player player;
	ItemStack weapon;
	World world;

	public ArrowLaunch(Projectile projectile) {
		this.projectile = projectile;
		this.player = (Player) projectile.getShooter();
		this.weapon = player.getItemInHand().clone();
		this.world = player.getWorld();
	}

	public Projectile getProjectile() {
		return projectile;
	}

	public void setProjectile(Projectile projectile) {
		this.projectile = projectile;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public ItemStack getWeapon() {
		return weapon;
	}

	public void setWeapon(ItemStack weapon) {
		this.weapon = weapon;
	}

	public World getWorld() {
		return world;
	}

	public void setWorld(World world) {
		this.world = world;
	}

	
	
}
