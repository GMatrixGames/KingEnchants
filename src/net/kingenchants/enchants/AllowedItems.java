package net.kingenchants.enchants;

import java.util.Arrays;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public enum AllowedItems {
	HELMET("Helmet", new ItemStack(Material.DIAMOND_HELMET),
			Material.LEATHER_HELMET,
			Material.GOLD_HELMET,
			Material.CHAINMAIL_HELMET,
			Material.IRON_HELMET,
			Material.DIAMOND_HELMET),
	/*CHESTPLATE("Chestplate", new ItemStack(Material.DIAMOND_CHESTPLATE),
			Material.LEATHER_CHESTPLATE,
			Material.GOLD_CHESTPLATE,
			Material.CHAINMAIL_CHESTPLATE,
			Material.IRON_CHESTPLATE,
			Material.DIAMOND_CHESTPLATE),
	LEGGINGS("Leggings", new ItemStack(Material.DIAMOND_LEGGINGS),
			Material.LEATHER_LEGGINGS,
			Material.GOLD_LEGGINGS,
			Material.CHAINMAIL_LEGGINGS,
			Material.IRON_LEGGINGS,
			Material.DIAMOND_LEGGINGS),*/
	BOOTS("Boots", new ItemStack(Material.DIAMOND_BOOTS),
			Material.LEATHER_BOOTS,
			Material.GOLD_BOOTS,
			Material.CHAINMAIL_BOOTS,
			Material.IRON_BOOTS,
			Material.DIAMOND_BOOTS),
	ARMOR("Armor", new ItemStack(Material.GOLD_CHESTPLATE),
			Material.LEATHER_HELMET,
			Material.GOLD_HELMET,
			Material.CHAINMAIL_HELMET,
			Material.IRON_HELMET,
			Material.DIAMOND_HELMET,
			Material.LEATHER_CHESTPLATE,
			Material.GOLD_CHESTPLATE,
			Material.CHAINMAIL_CHESTPLATE,
			Material.IRON_CHESTPLATE,
			Material.DIAMOND_CHESTPLATE,
			Material.LEATHER_LEGGINGS,
			Material.GOLD_LEGGINGS,
			Material.CHAINMAIL_LEGGINGS,
			Material.IRON_LEGGINGS,
			Material.DIAMOND_LEGGINGS,
			Material.LEATHER_BOOTS,
			Material.GOLD_BOOTS,
			Material.CHAINMAIL_BOOTS,
			Material.IRON_BOOTS,
			Material.DIAMOND_BOOTS),
	SWORD("Sword", new ItemStack(Material.DIAMOND_SWORD),
			Material.WOOD_SWORD,
			Material.GOLD_SWORD,
			Material.STONE_SWORD,
			Material.IRON_SWORD,
			Material.DIAMOND_SWORD),
	AXE("Axe", new ItemStack(Material.DIAMOND_AXE),
			Material.WOOD_AXE,
			Material.GOLD_AXE,
			Material.STONE_AXE,
			Material.IRON_AXE,
			Material.DIAMOND_AXE),
	WEAPON("Sword & Axe", new ItemStack(Material.GOLD_SWORD),
			Material.WOOD_SWORD,
			Material.GOLD_SWORD,
			Material.STONE_SWORD,
			Material.IRON_SWORD,
			Material.DIAMOND_SWORD,
			Material.WOOD_AXE,
			Material.GOLD_AXE,
			Material.STONE_AXE,
			Material.IRON_AXE,
			Material.DIAMOND_AXE),
	PICKAXE("Pickaxe", new ItemStack(Material.DIAMOND_PICKAXE),
			Material.WOOD_PICKAXE,
			Material.GOLD_PICKAXE,
			Material.STONE_PICKAXE,
			Material.IRON_PICKAXE,
			Material.DIAMOND_PICKAXE),
	/*SPADE("Shoval", new ItemStack(Material.DIAMOND_SPADE),
			Material.WOOD_SPADE,
			Material.GOLD_SPADE,
			Material.STONE_SPADE,
			Material.IRON_SPADE,
			Material.DIAMOND_SPADE),*/
	TOOLS("Tools", new ItemStack(Material.GOLD_PICKAXE),
			Material.WOOD_AXE,
			Material.GOLD_AXE,
			Material.STONE_AXE,
			Material.IRON_AXE,
			Material.DIAMOND_AXE,
			Material.WOOD_PICKAXE,
			Material.GOLD_PICKAXE,
			Material.STONE_PICKAXE,
			Material.IRON_PICKAXE,
			Material.DIAMOND_PICKAXE,
			Material.WOOD_SPADE,
			Material.GOLD_SPADE,
			Material.STONE_SPADE,
			Material.IRON_SPADE,
			Material.DIAMOND_SPADE),
	BOW("Bow", new ItemStack(Material.BOW),
			Material.BOW);
	
	private String
		name;
	
	private ItemStack
		item;
	
	private List<Material>
		items;
	
	AllowedItems(String name, ItemStack item, Material... items) {
		this.items = Arrays.asList(items);
		this.item = item;
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public ItemStack getItem() {
		return item;
	}
	
	public List<Material> getItems() {
		return this.items;
	}
	
	public boolean isAllowed(ItemStack is) {
		for (Material material : items)
			if (material.equals(is.getType()))
				return true;
		
		return false;
	}
}