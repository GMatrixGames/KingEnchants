package net.kingenchants.enchants;

public class BookEnchant {

	private CustomEnchant enchant;

	private int success;

	private int destroy;

	public BookEnchant(CustomEnchant enchant, int success, int destroy) {
		this.enchant = enchant;
		this.success = success;
		this.destroy = destroy;
	}

	public CustomEnchant getEnchant() {
		return this.enchant;
	}

	public int getSuccessRate() {
		return this.success;
	}

	public int getDestroyRate() {
		return this.destroy;
	}

	public void setSuccessRate(int success) {
		this.success = success;
	}

	public void setDestroyRate(int destroy) {
		this.destroy = destroy;
	}
}