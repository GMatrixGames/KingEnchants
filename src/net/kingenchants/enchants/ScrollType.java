package net.kingenchants.enchants;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.cryptolead.core.utils.ChatUtils;
import org.cryptolead.core.utils.ChatUtils.Color;

import net.kingenchants.KingEnchants;

public enum ScrollType {
	WHITE(ChatColor.BOLD + "White Scroll", "&7Prevents an item from being destroyed\n&7due to a failed Enchantment Book.\n \n&e&oPlace scroll onto item to apply. ".replaceAll("&", "�"), new ItemStack(Material.PAPER), Color.WHITE),
	BLACK(ChatColor.BOLD + "Black Scroll", "Removes a random enchantment\nfrom an iten and converts\nit into a " + Color.YELLOW + "-NUMBER" + Color.GRAY + "% success book.\n \n" + Color.GRAY + Color.ITALIC + "Place scroll on item to extract.", new ItemStack(Material.INK_SACK), Color.GRAY),
	RANDOM(ChatColor.BOLD + "Randomization Scroll", "&7Apply to any custom enchant book \n&7to reroll success / destory rates.\n \n&7&oDrag onto item to reroll rates. ".replaceAll("&", "�"), new ItemStack(Material.PAPER), Color.YELLOW),
	TRANSMOG(ChatColor.BOLD + "Transmog Scroll", "Organizes enchants by " + Color.YELLOW + "rarity" + Color.GRAY + " on item and adds enchant count on lore.\n\n" + Color.GRAY + Color.ITALIC + "Place scroll on item to apply.", new ItemStack(Material.PAPER), Color.RED),
	WORB(ChatColor.GOLD + "" + ChatColor.BOLD + "Weapon Enchantment Orb" + ChatColor.DARK_GRAY +  " [" +ChatColor.YELLOW + "" + ChatColor.UNDERLINE + (KingEnchants.MAX_ENCHANTS + 1)+ ChatColor.DARK_GRAY +  "]", ChatColor.GOLD + "\n+1 Enchantment Slots\n" + (KingEnchants.MAX_ENCHANTS + 1) + ChatColor.GOLD +" Max Enchantment Slots\n \n" + ChatColor.YELLOW + "Increases the # amount of enchantment \n" + ChatColor.YELLOW + "slots on a piece of armor by 1.", new ItemStack(Material.EYE_OF_ENDER), Color.YELLOW),
	AORB(ChatColor.GOLD + "" + ChatColor.BOLD + "Armor Enchantment Orb" + ChatColor.DARK_GRAY +  " [" +ChatColor.YELLOW + "" + ChatColor.UNDERLINE + (KingEnchants.MAX_ENCHANTS + 1)+ ChatColor.DARK_GRAY +  "]", ChatColor.GOLD + "\n+1 Enchantment Slots\n" + (KingEnchants.MAX_ENCHANTS + 1) + ChatColor.GOLD +" Max Enchantment Slots\n \n" + ChatColor.YELLOW + "Increases the # amount of enchantment \n" + ChatColor.YELLOW + "slots on a piece of armor by 1.", new ItemStack(Material.EYE_OF_ENDER), Color.RED);
	
	private String
		name,
		description, 
		color;
	
	private ItemStack
		item;
	
	
    ScrollType(String name, String description, ItemStack item,String color) {
        this.name = name;
        this.description = description;
        this.item = item;
        this.color = color;
    }
    
    public String getName() {
        return name;
    }
    
    public String getDescription() {
        return description;
    }
    
    public ItemStack getItem() {
    	return item;
    }
    
    public String getColor() {
    	return color;
    }
    
    public static ScrollType getScroll(String id) {
    	for (ScrollType scroll : values())
    		if (scroll.toString().equalsIgnoreCase(id))
    			return scroll;
    	
    	return null;
    }
    
    public static ScrollType getScrollByName(String name) {
    	for (ScrollType scroll : values())
    		if (scroll.getName().equalsIgnoreCase(name))
    			return scroll;
    	
    	return null;
    }
    
    public static ScrollType getScrollByFirstName(String name) {
    	for (ScrollType scroll : values())
    		if (ChatUtils.removeColors(scroll.getName()).split(" ")[0].equalsIgnoreCase(name))
    			return scroll;
    	
    	return null;
    }
}