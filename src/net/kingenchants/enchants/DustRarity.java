package net.kingenchants.enchants;

import org.cryptolead.core.utils.ChatUtils.Color;

public enum DustRarity {
	COMMON(0, "Common", Color.AQUA, (short) 3, 1, 5),
	UNCOMMON(1, "Uncommon", Color.GREEN , (short) 5, 5, 10),
	ROYAL(2, "Royal", Color.LIGHT_PURPLE, (short) 4, 10, 15);
	
	private String
		name;
	
	private String
		color;
	
	private int
		value,
		minDust,
		maxDust;
	
	private short
		blockColor;

    DustRarity(int value, String name, String color, short blockColor, int minDust, int maxDust) {
    	this.value = value;
        this.name = name;
        this.color = color;
        this.blockColor = blockColor;
        this.minDust = minDust;
        this.maxDust = maxDust;
    }
    
    public int getValue() {
    	return value;
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getColor() {
    	return this.color + Color.BOLD;
    }
    
    public short getBlockColor() {
    	return this.blockColor;
    }
    
    
    public int getMinimumDust() {
    	return minDust;
    }
    
    public int getMaximumDust() {
    	return maxDust;
    }
    
    public static DustRarity getRarity(String name) {
    	for (DustRarity rarity : values())
    		if (rarity.toString().equalsIgnoreCase(name))
    			return rarity;
    	
    	return null;
    }
}