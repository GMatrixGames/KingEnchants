package net.kingenchants.enchants;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;

import net.kingenchants.KingEnchants;
import net.kingenchants.utils.CRunnable;
import net.kingenchants.utils.enchants.EnchantUtils;

public class EnchantManager {

	public static void startTimer() {
		new CRunnable() {

			@Override
			public void run() {

				for (Player player : KingEnchants.getPlayers()) {
					List<PotionEffectType> perm = new ArrayList<PotionEffectType>();
					Collection<PotionEffect> effects = player.getActivePotionEffects();
					for (PotionEffect e : effects) {
						if (e.getDuration() > 10000)
							perm.add(e.getType());
					}

					CustomEnchant ce = EnchantUtils.getEnchant(player.getItemInHand(), EnchantType.HASTE);
					if (ce != null) {
						player.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 1000000,
								ce.getLevel() - 1, false, false));
						if (perm.contains(PotionEffectType.FAST_DIGGING))
							perm.remove(PotionEffectType.FAST_DIGGING);
					}

					ce = EnchantUtils.getEnchant(player.getInventory().getHelmet(), EnchantType.AQUATIC);
					if (ce != null) {
						player.addPotionEffect(
								new PotionEffect(PotionEffectType.WATER_BREATHING, 1000000, 1, false, false));
						if (perm.contains(PotionEffectType.WATER_BREATHING))
							perm.remove(PotionEffectType.WATER_BREATHING);
					}

					ce = EnchantUtils.getEnchant(player.getInventory().getHelmet(), EnchantType.GLOWING);
					if (ce != null) {
						player.addPotionEffect(
								new PotionEffect(PotionEffectType.NIGHT_VISION, 1000000, 1, false, false));
						if (perm.contains(PotionEffectType.NIGHT_VISION))
							perm.remove(PotionEffectType.NIGHT_VISION);
					}

					ce = EnchantUtils.getEnchant(player.getInventory().getHelmet(), EnchantType.ENDER_SHIFT);
					if (ce != null && player.getHealth() < 8) {
						player.addPotionEffect(
								new PotionEffect(PotionEffectType.REGENERATION, 200, ce.getLevel(), false, false));
						player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 200, ce.getLevel() >= 3 ? 2 : 1,
								false, false));
					}

					ce = EnchantUtils.getEnchant(player.getInventory().getBoots(), EnchantType.ANTI_GRAVITY);
					if (ce != null) {
						player.addPotionEffect(
								new PotionEffect(PotionEffectType.JUMP, 1000000, ce.getLevel() + 2, false, false));
						if (perm.contains(PotionEffectType.JUMP))
							perm.remove(PotionEffectType.JUMP);
					}

					// Changed every instance to "CE" to minimize ram usage
					// YoDontBeThatGirl

					ce = EnchantUtils.getEnchant(player.getInventory().getBoots(), EnchantType.SPRINGS);
					if (ce != null) {
						player.addPotionEffect(
								new PotionEffect(PotionEffectType.JUMP, 1000000, ce.getLevel() - 1, false, false));
						if (perm.contains(PotionEffectType.JUMP))
							perm.remove(PotionEffectType.JUMP);
					}

					ce = EnchantUtils.getArmorEnchant(player, EnchantType.GALLANTRY);
					if (ce != null && player.getHealth() < ce.getLevel() * 2) {
						player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 100,
								ce.getLevel() - 1, false, false));
					}

					ce = EnchantUtils.getArmorEnchant(player, EnchantType.JUGGERNAUT);
					if (ce != null) {
						player.addPotionEffect(new PotionEffect(PotionEffectType.HEALTH_BOOST, 1000000,
								(ce.getLevel()) - 1, false, false));
						if (perm.contains(PotionEffectType.HEALTH_BOOST))
							perm.remove(PotionEffectType.HEALTH_BOOST);

					}

					ce = EnchantUtils.getArmorEnchant(player, EnchantType.TIPSY);
					if (ce != null) {
						if (!player.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
							player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 1000000,
									ce.getLevel() > 2 ? 2 : ce.getLevel() == 1 ? 0 : 1, false, false));
							player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 1000000,
									ce.getLevel() > 2 ? 2 : ce.getLevel() == 1 ? 0 : 1, false, false));
						}
						if (perm.contains(PotionEffectType.INCREASE_DAMAGE))
							perm.remove(PotionEffectType.INCREASE_DAMAGE);
						if (perm.contains(PotionEffectType.SLOW))
							perm.remove(PotionEffectType.SLOW);

					}

					ce = EnchantUtils.getEnchant(player.getInventory().getHelmet(), EnchantType.LUCIDITY);
					if (ce != null && player.hasPotionEffect(PotionEffectType.BLINDNESS)) {
						for (PotionEffect potion : player.getActivePotionEffects()) {
							if (potion.getType().equals(PotionEffectType.BLINDNESS)
									&& potion.getAmplifier() <= ce.getLevel() - 1) {
								player.removePotionEffect(PotionEffectType.BLINDNESS);
							}
						}

					}
					ce = EnchantUtils.getArmorEnchant(player, EnchantType.SPEED);
					if (ce != null) {
						player.addPotionEffect(
								new PotionEffect(PotionEffectType.SPEED, 1000000, (ce.getLevel()) - 1, false, false));
						if (perm.contains(PotionEffectType.SPEED))
							perm.remove(PotionEffectType.SPEED);

					}

					ce = EnchantUtils.getArmorEnchant(player, EnchantType.COMMANDER);
					if (ce != null) {
						Random rand = new Random();
						int next = rand.nextInt(1000);
						if (next < ce.getLevel()) {

							FPlayer fPlayer = FPlayers.getInstance().getByPlayer(player);
							Faction faction = fPlayer.getFaction();
							if (faction != null) {
								String id = faction.getId();
								List<Entity> entities = player.getNearbyEntities(80, 80, 80);
								List<Player> factionMembers = new ArrayList<Player>();
								for (Entity e : entities) {
									if (!e.getType().equals(EntityType.PLAYER))
										continue;
									Player ep = (Player) e;
									FPlayer fp = FPlayers.getInstance().getByPlayer(ep);
									Faction fac = fp.getFaction();
									if (fac != null && fac.getId().equals(id)) {
										factionMembers.add(ep);
									}
								}
								for (Player fpl : factionMembers) {
									fpl.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING,
											ce.getLevel() > 3 ? 1 : 0, 100));
									fpl.addPotionEffect(new PotionEffect(PotionEffectType.SPEED,
											ce.getLevel() > 2 ? ce.getLevel() == 5 ? 2 : 1 : 0, 100));
								}
							}
						}
					}

					ce = EnchantUtils.getArmorEnchant(player, EnchantType.OBSIDIAN_SHIELD);
					if (ce != null) {
						player.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 1000000,
								(ce.getLevel()) - 1, false, false));
						if (perm.contains(PotionEffectType.FIRE_RESISTANCE))
							perm.remove(PotionEffectType.FIRE_RESISTANCE);
					}

					ce = EnchantUtils.getArmorEnchant(player, EnchantType.ENDER_WALKER);
					if (ce != null) {
						runEnderWalker(ce, player);
					}

					for (PotionEffectType potion : perm) {
						player.removePotionEffect(potion);
					}

				}
			}

			private void runEnderWalker(CustomEnchant ce, Player player) {
				for (PotionEffect effects : player.getActivePotionEffects()) {
					for (NegativeEffects bad : NegativeEffects.values()) {
						if (effects.getType().getName().equalsIgnoreCase(bad.name())) {
							player.removePotionEffect(effects.getType());
							if (ce.getLevel() > 3) {
								player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 1, 100));
							}
						}
					}
				}

			}
		}.activate(20, -1);
	}

	private enum NegativeEffects {
		HUNGER, POISON, WITHER;
	}

}