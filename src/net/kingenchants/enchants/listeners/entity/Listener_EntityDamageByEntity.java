package net.kingenchants.enchants.listeners.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.IronGolem;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;
import org.cryptolead.core.commons.RandomUtils;

import com.gmail.nossr50.datatypes.player.McMMOPlayer;
import com.gmail.nossr50.datatypes.skills.SkillType;
import com.gmail.nossr50.skills.swords.SwordsManager;
import com.gmail.nossr50.util.player.UserManager;
import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;

import net.kingenchants.KingEnchants;
import net.kingenchants.data.Temp;
import net.kingenchants.enchants.CustomEnchant;
import net.kingenchants.enchants.EnchantType;
import net.kingenchants.listeners.CListener;
import net.kingenchants.utils.ArrowLaunch;
import net.kingenchants.utils.GuardianManager;
import net.kingenchants.utils.RuseManager;
import net.kingenchants.utils.enchants.EnchantUtils;

public class Listener_EntityDamageByEntity extends CListener {

	List<ArrowLaunch> arrows = new ArrayList<ArrowLaunch>();

	public static HashMap<Player, Integer> bleed = new HashMap<Player, Integer>();
	public static HashMap<Player, Integer> pacify = new HashMap<Player, Integer>();
	HashMap<Player, Integer> rage = new HashMap<Player, Integer>();
	public static HashMap<Player, Integer> silence = new HashMap<Player, Integer>();

	@EventHandler(priority = EventPriority.MONITOR)
	public void registerArrows(ProjectileLaunchEvent event) {
		if (event.getEntity().getShooter() instanceof Player) {

			arrows.add(new ArrowLaunch(event.getEntity()));
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void land(ProjectileHitEvent event) {
		if (arrows.isEmpty())
			return;
		ArrowLaunch launch = null;
		for (ArrowLaunch al : arrows) {
			if (al.getProjectile().getUniqueId().equals(event.getEntity().getUniqueId())) {
				launch = al;
			}
		}
		if (launch == null)
			return;

		CustomEnchant enchant = EnchantUtils.getEnchant(launch.getWeapon(), EnchantType.EXPLOSIVE);
		if (enchant != null) {
			List<Entity> ents = launch.getProjectile().getNearbyEntities(enchant.getLevel(), enchant.getLevel(),
					enchant.getLevel());
			launch.getWorld().playEffect(launch.getProjectile().getLocation(), Effect.EXPLOSION_HUGE, 20);
			for (Entity ent : ents) {
				if (ent instanceof LivingEntity)
					((LivingEntity) ent).addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 20 * 3, 1));
			}
		}
		enchant = EnchantUtils.getEnchant(launch.getWeapon(), EnchantType.HELLFIRE);
		if (enchant != null) {
			List<Entity> ents = launch.getProjectile().getNearbyEntities(enchant.getLevel(), enchant.getLevel(),
					enchant.getLevel());
			launch.getWorld().playEffect(launch.getProjectile().getLocation(), Effect.FLAME, 20);
			for (Entity ent : ents) {
				if (ent instanceof LivingEntity)
					((LivingEntity) ent).damage(3);
			}
		}

		enchant = EnchantUtils.getEnchant(launch.getWeapon(), EnchantType.INFERNAL);
		if (enchant != null) {
			List<Entity> ents = launch.getProjectile().getNearbyEntities(enchant.getLevel() * 2, enchant.getLevel() * 2,
					enchant.getLevel() * 2);
			launch.getWorld().playEffect(launch.getProjectile().getLocation(), Effect.MOBSPAWNER_FLAMES, 200);
			for (Entity ent : ents) {
				if (ent instanceof LivingEntity)
					((LivingEntity) ent).setFireTicks(100);
			}
		}

	}

	// FOR STACKING ENCHANTS
	@EventHandler(priority = EventPriority.LOWEST)
	public void lowOn(EntityDamageByEntityEvent event) {

		Entity entity = event.getEntity();

		Entity damager = event.getDamager();

		if (entity.getType().equals(EntityType.PLAYER)) {
			Player player = (Player) entity;

			if (rage.containsKey((Player) entity)) {
				rage.remove((Player) entity);
			}

			int enchantLevel = EnchantUtils.stackArmorEnchant(player, EnchantType.HEAVY);
			if (damager.getType().equals(EntityType.ARROW) && enchantLevel != -1) {
				double enchant = event.getDamage();
				double damage = 1.0D - (double) enchantLevel * 0.02D;
				event.setDamage(enchant * damage);
			}
			
			if (damager.getType().equals(EntityType.PLAYER)) {
				Player damagerPlayer = (Player) damager;
				if (damagerPlayer.getItemInHand().getType().equals(Material.DIAMOND_SWORD)
						|| damagerPlayer.getItemInHand().getType().equals(Material.GOLD_SWORD)
						|| damagerPlayer.getItemInHand().getType().equals(Material.IRON_SWORD)
						|| damagerPlayer.getItemInHand().getType().equals(Material.STONE_SWORD)
						|| damagerPlayer.getItemInHand().getType().equals(Material.WOOD_SWORD)) {

					int enchant = EnchantUtils.stackArmorEnchant(player, EnchantType.ARMORED);
					if (enchant != -1) {
						double damage = event.getDamage();

						double multi = 1 - (enchant * 0.0185);
						event.setDamage(damage * multi);
					}

				} else if (damagerPlayer.getItemInHand().getType().equals(Material.DIAMOND_AXE)
						|| damagerPlayer.getItemInHand().getType().equals(Material.GOLD_AXE)
						|| damagerPlayer.getItemInHand().getType().equals(Material.IRON_AXE)
						|| damagerPlayer.getItemInHand().getType().equals(Material.STONE_AXE)
						|| damagerPlayer.getItemInHand().getType().equals(Material.WOOD_AXE)) {

					int enchant = EnchantUtils.stackArmorEnchant(player, EnchantType.TANK);
					if (enchant != -1) {
						double damage = event.getDamage();

						double multi = 1 - (enchant * 0.0185);
						event.setDamage(damage * multi);
					}

				}

				if (player.getItemInHand().getType().equals(Material.DIAMOND_AXE)
						|| player.getItemInHand().getType().equals(Material.GOLD_AXE)
						|| player.getItemInHand().getType().equals(Material.IRON_AXE)
						|| player.getItemInHand().getType().equals(Material.STONE_AXE)
						|| player.getItemInHand().getType().equals(Material.WOOD_AXE)) {
					CustomEnchant ce = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.PRIMITIVE);
					if (ce != null) {
						int level = ce.getLevel();
						double damage = event.getDamage();
						double multi = 1 + (level * 0.05);
						event.setDamage(damage * multi);
					}
				}

			} else if (event.getDamager().getType().equals(EntityType.ARROW)) {

				if (arrows.isEmpty())
					return;

				ArrowLaunch launch = null;
				for (ArrowLaunch al : arrows) {
					if (al.getProjectile().getUniqueId().equals(event.getEntity().getUniqueId())) {
						launch = al;
					}
				}

				if (launch == null)
					return;

				int enchant = EnchantUtils.stackArmorEnchant(launch.getPlayer(), EnchantType.MARKSMAN);
				if (enchant != -1) {
					event.setDamage(event.getDamage() * (1 + (enchant * 0.04)));
				}
			}
		}

	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void postON(EntityDamageByEntityEvent event) {
		if (event.isCancelled())
			return;
		Entity entity = event.getEntity();

		Entity damager = event.getDamager();

		if (damager instanceof Player) {
			Player damagerPlayer = (Player) damager;

			CustomEnchant enchant = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.EPICNESS);
			if (enchant != null) {

				damager.getWorld().playEffect(damager.getLocation(), Effect.BLAZE_SHOOT, 20);
				damager.getWorld().playSound(damager.getLocation(), (epicness(enchant.getLevel())), 10, 10);

			}

			if (entity.getType().equals(EntityType.IRON_GOLEM)) {
				GuardianManager.healSpawner((IronGolem) entity);
			} else if (entity.getType().equals(EntityType.PLAYER)) {

				Player player = (Player) entity;

				enchant = EnchantUtils.getArmorEnchant(player, EnchantType.FARCAST);
				if (enchant != null && player.getItemInHand().getType().equals(Material.BOW)) {

					Location al = player.getLocation();
					Location pl = damagerPlayer.getLocation();

					Vector vect = pl.subtract(al).toVector().normalize();
					vect.setX(vect.getX());
					vect.setY(vect.getY() + 2);
					vect.setZ(vect.getZ()).normalize().multiply(20 - player.getHealth());

					damagerPlayer.setVelocity(vect);

				}

				enchant = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.OBLITERATE);
				if (enchant != null) {
					int next = random(damagerPlayer, 100);
					if (next < enchant.getLevel() * 5) {
						player.setVelocity(damager.getLocation().getDirection().multiply(enchant.getLevel())
								.add(new Vector(0, 2, 0)));
						player.getWorld().playEffect(player.getLocation(), Effect.EXPLOSION_HUGE, 10);
					}
				}

				enchant = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.RAPIER);
				if (enchant != null) {
					if (entity.getType().equals(EntityType.PLAYER)
							&& ((Player) entity).getItemInHand().getType().equals(Material.BOW)) {

						((Player) entity).damage(event.getFinalDamage() * (enchant.getLevel() - 1) / 2);
					}

				}
				enchant = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.ANIMOSITY);
				if (enchant != null) {
					if (!pacify.containsKey(damagerPlayer)) {
						if (rage.containsKey(damagerPlayer)) {
							if (rage.get(damagerPlayer) + 1 <= enchant.getLevel())
								rage.put(damagerPlayer, rage.get(damagerPlayer) + 1);
						} else
							rage.put(damagerPlayer, 1);

						((Damageable) entity).damage(event.getFinalDamage() * (rage.get(damagerPlayer) / 10));
					}
				}

				enchant = EnchantUtils.getArmorEnchant(player, EnchantType.SHOCKWAVE);
				if (enchant != null) {

					int next = random(player, 100);
					if (next < enchant.getLevel()) {
						for (Entity e : player.getNearbyEntities(20, 20, 20)) {
							Location al = player.getLocation();
							Location pl = e.getLocation();

							Vector vect = pl.subtract(al).toVector().normalize();
							vect.setX(vect.getX() * 2);
							vect.setY(vect.getY() * 3 + 0.5);
							vect.setZ(vect.getZ() * 2).normalize();

							e.setVelocity(vect);
							// TODO add support so this works with wg / combat
							// tag
						}
					}
				}

				enchant = EnchantUtils.getArmorEnchant(player, EnchantType.RAGDOLL);
				if (enchant != null) {
					player.setVelocity(damager.getLocation().getDirection().multiply(enchant.getLevel() * 2)
							.add(new Vector(0, 2, 0)));
				}

			}

			CustomEnchant ce = EnchantUtils.getEnchant(((Player) damager).getItemInHand(), EnchantType.CHAIN);
			if (ce != null && !entity.getType().equals(EntityType.PLAYER)) {
				entity.setVelocity(damager.getLocation().getDirection().multiply(-1).normalize());
			}
		}

		if (event.getDamager() instanceof Projectile) {
			if (arrows.isEmpty())
				return;

			Projectile proj = (Projectile) event.getDamager();
			ArrowLaunch launch = null;
			for (ArrowLaunch al : arrows) {
				if (al.getProjectile().getUniqueId().equals(proj.getUniqueId())) {
					launch = al;
				}
			}

			if (launch == null)
				return;
			CustomEnchant enchant = EnchantUtils.getEnchant(launch.getWeapon(), EnchantType.SNARE);

			if (enchant != null && entity instanceof LivingEntity) {
				if (getMetaphysical(entity))
					((LivingEntity) entity)
							.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 5 * 20, enchant.getLevel()));
			}

			enchant = EnchantUtils.getEnchant(launch.getWeapon(), EnchantType.HEADSHOT);
			if (enchant != null && entity instanceof Player) {
				double y = launch.getProjectile().getLocation().getY();
				double y1 = ((LivingEntity) entity).getEyeLocation().getY();
				if (y > y1 - 0.2) {
					((Damageable) entity).damage(event.getFinalDamage() * (getHeadshot(enchant.getLevel()) - 1));
					entity.sendMessage("�6�l  ** HEADSHOT [" + getHeadshot(enchant.getLevel()) + "x Damage]");
					launch.getPlayer()
							.sendMessage("�6�l  ** HEADSHOT [" + getHeadshot(enchant.getLevel()) + "x Damage]");
				}
			}
			enchant = EnchantUtils.getEnchant(launch.getWeapon(), EnchantType.PIERCING);
			if (enchant != null && entity instanceof Player) {
				((Damageable) entity).damage(enchant.getLevel());
			}

			enchant = EnchantUtils.getEnchant(launch.getWeapon(), EnchantType.PACIFY);
			if (enchant != null && entity instanceof Player) {
				pacify.put((Player) entity, enchant.getLevel() * 2);
				entity.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + " ** PACIFIED ** " + "("
						+ enchant.getLevel() * 2 + " seconds)");
			}
			enchant = EnchantUtils.getEnchant(launch.getWeapon(), EnchantType.UNFOCUS);
			if (enchant != null && entity instanceof Player) {
				((Player) entity)
						.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 5, 20 * enchant.getLevel()));

				entity.sendMessage("�6�l  ** UNFOCUS **  ");

			}
			enchant = EnchantUtils.getEnchant(launch.getWeapon(), EnchantType.ARROW_LIFESTEAL);
			if (enchant != null) {
				Player launcher = launch.getPlayer();
				if (launcher.isOnline()) {

					double health = launcher.getMaxHealth() - launcher.getHealth();
					if (health > enchant.getLevel()) {
						launcher.setHealth(launcher.getHealth() + enchant.getLevel());
					} else
						launcher.setHealth(launcher.getMaxHealth());

				}
			}
			enchant = EnchantUtils.getEnchant(launch.getWeapon(), EnchantType.HIJACK);
			if (enchant != null && entity.getType().equals(EntityType.IRON_GOLEM)) {

				int next = random(launch.getPlayer(), 10);
				if (next < enchant.getLevel()) {

					IronGolem golem = (IronGolem) entity;

					GuardianManager.changeOwner(golem, launch.getPlayer());

					launch.getPlayer().sendMessage(ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + " ** HIJACKED ** ");

				}

			}

		}

	}

	private double getHeadshot(int level) {
		switch (level) {
		case 1:
			return 2;
		case 2:
			return 2.4;
		case 3:
			return 2.8;
		case 4:
			return 3.2;
		case 5:
			return 3.5;
		default:
			return 1;
		}

	}

	private Sound epicness(int level) {

		switch (level) {
		case 1:
			return Sound.GHAST_MOAN;
		case 2:
			return Sound.BLAZE_BREATH;
		default:
			return Sound.AMBIENCE_RAIN;
		}
	}

	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = false)
	public void on(EntityDamageByEntityEvent event) {
		Entity entity = event.getEntity();
		Entity damager = event.getDamager();

		silence: if (entity instanceof Player) {
			Player player = (Player) entity;

			if (player.getItemInHand() != null && player.getItemInHand().getType().equals(Material.BOW)
					&& damager instanceof Arrow) {
				Arrow arrow = (Arrow) damager;
				if (Temp.DAMAGE_ARROWS.containsKey(arrow)) {
					event.setDamage(event.getDamage() + Temp.DAMAGE_ARROWS.get(arrow));
					Temp.DAMAGE_ARROWS.remove(arrow);
				}
			}

			if (silence.containsKey(player))
				break silence;

			damagerEntityAndEntityPlayer(event, damager, player);
			if (damager instanceof Player) {
				damagerPlayerAndEntityPlayer(event, (Player) damager, player);
			} else if (event.getDamager() instanceof Projectile) {
				Projectile p = (Projectile) event.getDamager();
				CustomEnchant enchant = EnchantUtils.getArmorEnchant(player, EnchantType.ARROW_BREAK);
				if (enchant != null) {

					Random rand = new Random();
					if (rand.nextInt(100) < enchant.getLevel() * 25) {
						event.setCancelled(true);
						p.teleport(new Location(player.getWorld(), 0, -10, 0));
						player.getWorld().playEffect(player.getLocation(), Effect.CLICK2, 20);
					}

				}

				CustomEnchant ench = EnchantUtils.getEnchant(player.getItemInHand(), EnchantType.ARROW_DEFLECT);
				if (ench != null) {
					Random rand = new Random();
					if (rand.nextInt(55) != 1) {
						event.setCancelled(true);
						player.getWorld().playEffect(player.getLocation(), Effect.CLICK2, 20);
						p.teleport(new Location(player.getWorld(), 0, -10, 0));
					}
				}
			}
		}

		if (damager instanceof Player) {
			Player damagerPlayer = (Player) damager;

			CustomEnchant ench = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.REFORGED);
			if (ench != null) {
				Random random = new Random();
				int next = random.nextInt(100);
				if (next < 20) {
					for (int i = 0; i < ench.getLevel(); i++) {
						improveDurability(damagerPlayer.getItemInHand());
					}

				}
			}
		}
	}

	private void improveDurability(ItemStack item) {
		if (item.getDurability() > 10) {
			item.setDurability((short) (item.getDurability() - 10));
		}

	}

	@SuppressWarnings("null")
	private void depleteArmor(int itemNum, int Amount, Player player) {
		ItemStack item = null;
		switch (itemNum) {
		case 0:
			player.getInventory().getHelmet();
			break;
		case 1:
			player.getInventory().getChestplate();
			break;
		case 2:
			player.getInventory().getLeggings();
			break;
		case 3:
			player.getInventory().getBoots();
			break;

		default:
			return;
		}
		try {
			item.setDurability((short) (item.getDurability() - Amount));
		} catch (Exception e) { // TODO make not vague

		}
	}

	public int random(Player relevant, int max) {
		Random rand = new Random();
		int next = rand.nextInt(max);
		CustomEnchant enchant = EnchantUtils.getArmorEnchant(relevant, EnchantType.LUCKY);
		if (enchant != null) {
			if (rand.nextInt(50) < enchant.getLevel())
				return 0;
		}
		return next;

	}

	private boolean getMetaphysical(Entity player) {
		// if they have return false
		if (!player.getType().equals(EntityType.PLAYER))
			return true;

		CustomEnchant enchant = EnchantUtils.getArmorEnchant((Player) player, EnchantType.METAPHYSICAL);

		if (enchant != null) {

			Random rand = new Random();
			if (rand.nextInt(10) < enchant.getLevel()) {

				player.sendMessage(ChatColor.AQUA + "" + ChatColor.BOLD + "Metaphysical: Prevented Slowness");
				return false;

			}

		}

		return true;
	}

	public void damagerPlayerAndEntityPlayer(EntityDamageByEntityEvent event, Player damagerPlayer, Player player) {

		CustomEnchant enchant = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.ACIDITY);
		if (enchant != null) {
			Random random = new Random();
			int next = random.nextInt(100);
			if (next < enchant.getLevel() * 5) {
				player.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 40 * enchant.getLevel(), 2));
			}
		}

		enchant = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.BLEED);
		if (enchant != null) {
			runBloodlust(damagerPlayer);

			if (bleed.containsKey(player)) {
				bleed.put(player, bleed.get(player) + 1);
			} else {
				bleed.put(player, 1);
			}
		}

		enchant = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.DEEP_WOUNDS);
		if (enchant != null) {

			int level = enchant.getLevel();
			Random random = new Random();
			int next = random.nextInt(100);
			if (next < level * 2) {
				McMMOPlayer mcMMOPlayer = UserManager.getPlayer(damagerPlayer);
				SwordsManager swordsManager = mcMMOPlayer.getSwordsManager();

				if (swordsManager.canActivateAbility()) {
					mcMMOPlayer.checkAbilityActivation(SkillType.SWORDS);
				}

				if (swordsManager.canUseBleed()) {
					swordsManager.bleedCheck(player);
				}

			}
		}

		enchant = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.DEMONFORGED);
		if (enchant != null) {
			Random random = new Random();
			depleteArmor(random.nextInt(4), random.nextInt(enchant.getLevel()), player);
		}

		enchant = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.AMBUSCADE);
		if (enchant != null) {
			Random random = new Random();
			int next = random.nextInt(100);
			if (next < enchant.getLevel() * 5) {
				if (getMetaphysical(player))
					player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 40 * enchant.getLevel(), 4));
			}
		}

		enchant = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.SKILL_SWIPE);
		if (enchant != null) {

			if (player.getTotalExperience() > enchant.getLevel() * 20) {

				player.setTotalExperience(player.getTotalExperience() - enchant.getLevel() * 20);
				damagerPlayer.setTotalExperience(damagerPlayer.getTotalExperience() + enchant.getLevel() * 20);
			}
		}

		enchant = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.SILENCE);
		if (enchant != null) {
			Random random = new Random();
			int next = random.nextInt(100);
			if (next < enchant.getLevel() * 3) {
				silence.put(player, enchant.getLevel() * 2);
				player.sendMessage(ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "  ** SILENCED ( " + enchant.getLevel()
						+ " seconds ) **  ");

			}

		}

		enchant = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.ICE_ASPECT);
		if (enchant != null) {
			Random random = new Random();
			int next = random.nextInt(100);
			if (next < enchant.getLevel() * 5) {
				if (getMetaphysical(player))
					player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 40 * enchant.getLevel(), 10));
			}
		}

		enchant = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.VAMPIRE);
		if (enchant != null) {
			double next = random(damagerPlayer, 10);
			if (next < enchant.getLevel()) {
				if (damagerPlayer.getHealth() < damagerPlayer.getMaxHealth()
						- (event.getFinalDamage() / 10) * enchant.getLevel())
					damagerPlayer
							.setHealth(damagerPlayer.getHealth() + (event.getFinalDamage() / 10) * enchant.getLevel());
				else
					damagerPlayer.setHealth(damagerPlayer.getMaxHealth());
			}
		}

		enchant = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.TRAP);
		if (enchant != null) {
			Random random = new Random();
			int next = random.nextInt(100);
			if (next < enchant.getLevel() * 5) {
				if (getMetaphysical(player))
					player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 20 * enchant.getLevel(), 200));
			}
		}

		enchant = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.BLESSED);
		if (enchant != null) {
			Random random = new Random();
			int next = random.nextInt(100);
			if (next < enchant.getLevel() * 4) {
				runBless(damagerPlayer);
			}
		}

		enchant = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.PUMMEL);
		if (enchant != null) {
			Random random = new Random();
			int next = random.nextInt(100);
			if (next < enchant.getLevel() * 5) {
				if (getMetaphysical(player))
					player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 40 * enchant.getLevel(), 2));
			}
		}

		enchant = EnchantUtils.getArmorEnchant(damagerPlayer, EnchantType.DESTRUCTION);
		if (enchant != null) {
			Random random = new Random();
			int next = random.nextInt(100);
			if (next < enchant.getLevel() * 3) {

				FPlayer fPlayer = FPlayers.getInstance().getByPlayer(damagerPlayer);
				Faction faction = fPlayer.getFaction();

				for (Entity e : damagerPlayer.getNearbyEntities(20, 20, 20)) {
					if (!e.getType().equals(EntityType.PLAYER))
						continue;

					FPlayer fP = FPlayers.getInstance().getByPlayer((Player) e);
					Faction fa = fP.getFaction();
					exit: if (fa != null) {
						if (faction == null)
							break exit;
						if (fa.getId().equals(faction.getId()))
							continue;
					}
					Player p = (Player) e;

					switch (random.nextInt(4)) {
					case 3:
						p.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, enchant.getLevel() * 10,
								(int) enchant.getLevel() / 2));
						break;

					case 2:
						p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, enchant.getLevel() * 10,
								(int) enchant.getLevel() / 2));
						break;
					case 1:
						p.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, enchant.getLevel() * 10,
								(int) enchant.getLevel() / 2));
						break;
					default:
						p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, enchant.getLevel() * 15,
								(int) ((int) enchant.getLevel() / 1.5)));
						break;
					}
				}
			}
		}

		CustomEnchant execute = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.EXECUTE);
		if (execute != null && player.getHealth() < 8)
			event.setDamage(event.getDamage() + execute.getLevel() * 2);

		CustomEnchant enrage = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.ENRAGE);
		if (enrage != null && damagerPlayer.getHealth() < 8)
			event.setDamage(event.getDamage() + enrage.getLevel() * 2);

		CustomEnchant confusion = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.CONFUSION);
		if (confusion != null && RandomUtils.randomInt(100) <= confusion.getLevel() * 2)
			player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 260, 1, true, false));

		CustomEnchant iceFreeze = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.ICE_FREEZE);
		if (iceFreeze != null && RandomUtils.randomInt(100) <= iceFreeze.getLevel() * 2)
			if (getMetaphysical(player))
				player.addPotionEffect(
						new PotionEffect(PotionEffectType.SLOW, 20 * iceFreeze.getLevel() * 3, 50, true, false));

		CustomEnchant cleave = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.CLEAVE);
		if (cleave != null && RandomUtils.randomInt(100) <= cleave.getLevel() * 2)
			for (Player p : KingEnchants.getPlayers())
				if (!p.equals(player) && p.getWorld().equals(player.getWorld()))
					if (p.getLocation().distance(player.getLocation()) <= cleave.getLevel())
						p.damage(1);

		CustomEnchant insomnia = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.INSOMNIA);
		if (insomnia != null && RandomUtils.randomInt(100) <= insomnia.getLevel() * 2) {
			if (getMetaphysical(player))
				player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 200, 1, true, false));
			player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 200, 1, true, false));
			player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 260, 1, true, false));
		}

		CustomEnchant berserk = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.BERSERK);
		if (berserk != null && RandomUtils.randomInt(100) <= berserk.getLevel() * 2) {
			damagerPlayer.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 200, 1, true, false));
			damagerPlayer.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 200, 1, true, false));
		}

		CustomEnchant curse = EnchantUtils.getArmorEnchant(damagerPlayer, EnchantType.CURSE);
		if (curse != null && RandomUtils.randomInt(100) <= curse.getLevel() * 2)
			player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 200, 1, true, false));

		CustomEnchant featherweight = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.FEATHERWEIGHT);
		if (featherweight != null && RandomUtils.randomInt(100) <= featherweight.getLevel() * 2)
			damagerPlayer.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 200, 3, true, false));

		CustomEnchant ravenous = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.RAVENOUS);
		if (ravenous != null && RandomUtils.randomInt(100) <= ravenous.getLevel() * 3)
			damagerPlayer.setFoodLevel(damagerPlayer.getFoodLevel() + ravenous.getLevel() * 3);

		CustomEnchant blind = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.BLIND);
		if (blind != null && RandomUtils.randomInt(100) <= blind.getLevel() * 2)
			player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 200, 1, true, false));

		CustomEnchant paralyze = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.PARALYZE);
		if (paralyze != null && RandomUtils.randomInt(100) <= paralyze.getLevel() * 2) {
			player.getLocation().getWorld().strikeLightningEffect(player.getLocation());
			if (getMetaphysical(player))
				player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 200, 1, true, false));
			player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 200, 1, true, false));
		}
		CustomEnchant lifesteal = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.LIFESTEAL);
		if (lifesteal != null) {
			int rand = random(damagerPlayer, 40);
			if (rand < 10) {
				if (damagerPlayer.getHealth() + lifesteal.getLevel() > damagerPlayer.getMaxHealth()) {
					damagerPlayer.setHealth(damagerPlayer.getMaxHealth());
				} else {
					damagerPlayer.setHealth(damagerPlayer.getHealth() + lifesteal.getLevel());
				}
			}
		}

		CustomEnchant poison = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.POISON);
		if (poison != null && RandomUtils.randomInt(100) <= poison.getLevel() * 2)
			player.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 200, 1, true, false));

		CustomEnchant wither = EnchantUtils.getEnchant(damagerPlayer.getItemInHand(), EnchantType.WITHER);
		if (wither != null && RandomUtils.randomInt(100) <= wither.getLevel() * 2)
			player.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 200, 1, true, false));

	}

	private void runBloodlust(Player damagerPlayer) {
		for (Entity e : damagerPlayer.getNearbyEntities(10, 10, 10)) {
			if (!e.getType().equals(EntityType.PLAYER))
				continue;
			Player p = (Player) e;
			CustomEnchant enchant = EnchantUtils.getArmorEnchant(p, EnchantType.BLOOD_LUST);
			if (enchant != null) {

				double chance = random(p, 100);

				if (chance < enchant.getLevel() * 6) {
					if (p.getHealth() < p.getMaxHealth() - enchant.getLevel()) {
						p.setHealth(p.getHealth() + enchant.getLevel());
					} else {
						p.setHealth(p.getMaxHealth());
					}
					p.sendMessage(ChatColor.DARK_RED + " ** BloodLust ** ");
				}
			}
		}
	}

	public enum NegativeEffects {
		CONFUSION, HARM, HUNGER, POISON, SLOW_DIGGING, SLOW, WEAKNESS, WITHER;
	}

	public void runBless(Player player) {
		for (PotionEffect effects : player.getActivePotionEffects()) {
			for (NegativeEffects bad : NegativeEffects.values()) {
				if (effects.getType().getName().equalsIgnoreCase(bad.name())) {
					player.removePotionEffect(effects.getType());
				}
			}
		}
	}

	public void damagerEntityAndEntityPlayer(EntityDamageByEntityEvent event, Entity damager, Player player) {
		CustomEnchant

		enchant = EnchantUtils.getArmorEnchant(player, EnchantType.CACTUS);
		if (enchant != null && damager instanceof Damageable) {
			((Damageable) damager).damage(enchant.getLevel() * 2);
		}
		enchant = EnchantUtils.getArmorEnchant(player, EnchantType.GUARDIANS);
		if (enchant != null) {
			double chance = random(player, 100);
			if (chance < enchant.getLevel() && chance != 0) {

				IronGolem golem = (IronGolem) player.getWorld().spawnEntity(player.getLocation(),
						EntityType.IRON_GOLEM);
				golem.setMaxHealth(300);
				golem.setHealth(30 * enchant.getLevel());
				GuardianManager.update(GuardianManager.spawn(golem, player));
			}

		}

		enchant = EnchantUtils.getArmorEnchant(player, EnchantType.DODGE);
		if (enchant != null) {
			boolean sneaking = player.isSneaking();
			Random rand = new Random();
			int next = rand.nextInt(100) * ((sneaking ? 2 : 1) * enchant.getLevel() / 10);
			if (next >= 40) {
				event.setCancelled(true);
				player.sendMessage(ChatColor.BLUE + "" + ChatColor.BOLD + "  ** DODGED **  ");
			}

		}
		enchant = EnchantUtils.getArmorEnchant(player, EnchantType.FROZEN);
		if (enchant != null) {
			int next = random(player, 100);
			if (next < enchant.getLevel() * 5) {
				if (damager instanceof Player)
					if (getMetaphysical(damager))
						((Player) damager)
								.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 40 * enchant.getLevel(), 2));
			}
		}

		enchant = EnchantUtils.getArmorEnchant(player, EnchantType.UNDEAD_RUSE);
		if (enchant != null) {
			int next = random(player, 100);
			if (next < 3) {
				for (int i = 0; i < enchant.getLevel(); i++) {
					Entity e = player.getWorld().spawnEntity(player.getLocation(), EntityType.ZOMBIE);
					e.setCustomName(ChatColor.AQUA + player.getName() + "'s Undead Ruse");
					Zombie z = (Zombie) e;
					z.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 2, 40000));
					z.setBaby(false);
					z.setVillager(false);
					z.setCanPickupItems(false);
					z.setCustomNameVisible(true);
					RuseManager.spawn(z, player);
				}
			}

		}

		int ench = EnchantUtils.stackArmorEnchant(player, EnchantType.HARDENED);
		if (ench != -1) {
			Random random = new Random();
			int next = random(player, 100);
			if (next < ench) {

				switch (random.nextInt(3)) {
				case 0:
					improveDurability(player.getInventory().getHelmet());
					break;
				case 1:
					improveDurability(player.getInventory().getChestplate());
					break;
				case 2:
					improveDurability(player.getInventory().getLeggings());
					break;
				case 3:
					improveDurability(player.getInventory().getBoots());
					break;
				default:
					improveDurability(player.getInventory().getHelmet());
					break;
				}

			}
		}

		enchant = EnchantUtils.getArmorEnchant(player, EnchantType.CONJURING);
		if (enchant != null) {
			int next = random(player, 100);
			if (next < enchant.getLevel() * 5) {
				if (damager instanceof Player)
					((Player) damager)
							.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 40 * enchant.getLevel(), 2));
			}
		}

		enchant = EnchantUtils.getArmorEnchant(player, EnchantType.STORMCALLER);
		if (enchant != null) {
			int next = random(player, 100);
			if (next < enchant.getLevel() * 5) {
				((Damageable) damager).damage(enchant.getLevel());
				damager.getLocation().getWorld().strikeLightningEffect(damager.getLocation());
			}
		}

		enchant = EnchantUtils.getArmorEnchant(player, EnchantType.ROCKET_ESCAPE);
		if (enchant != null) {
			if (player.getHealth() - event.getFinalDamage() <= 1) {
				player.setVelocity(new Vector(0, 4 * enchant.getLevel(), 0));
				event.setCancelled(true);
				player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 2, 200));
				player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 255, 300));
				player.getWorld().playEffect(player.getLocation(), Effect.LARGE_SMOKE, 100);
			}
		}

		enchant = EnchantUtils.getArmorEnchant(player, EnchantType.SMOKE_BOMB);
		if (enchant != null) {
			if (player.getHealth() - event.getFinalDamage() <= 0) {
				FPlayer fPlayer = FPlayers.getInstance().getByPlayer(player);
				Faction faction = fPlayer.getFaction();

				for (Entity e : player.getNearbyEntities(20, 20, 20)) {
					if (!e.getType().equals(EntityType.PLAYER))
						continue;

					FPlayer fP = FPlayers.getInstance().getByPlayer((Player) e);
					Faction fa = fP.getFaction();
					exit: if (fa != null) {
						if (faction == null)
							break exit;
						if (fa.getId().equals(faction.getId()))
							continue;
					}
					Player p = (Player) e;
					p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, enchant.getLevel() * 20,
							enchant.getLevel() > 2 ? enchant.getLevel() > 4 ? 3 : 2 : 1));
					p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, enchant.getLevel() * 20,
							enchant.getLevel() > 2 ? enchant.getLevel() > 4 ? 3 : 2 : 1));
					p.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, enchant.getLevel() * 20,
							enchant.getLevel() > 2 ? enchant.getLevel() > 4 ? 3 : 2 : 1));
					p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, enchant.getLevel() * 60,
							enchant.getLevel() > 2 ? enchant.getLevel() > 4 ? 3 : 2 : 1));
				}
			}
		}

		enchant = EnchantUtils.getArmorEnchant(player, EnchantType.SPIRIT_LINK);
		if (enchant != null) {
			int next = random(player, 300);
			if (next < enchant.getLevel() * 5) {

				FPlayer fPlayer = FPlayers.getInstance().getByPlayer(player);
				Faction faction = fPlayer.getFaction();
				if (faction != null) {

					String id = faction.getId();

					List<Entity> entities = player.getNearbyEntities(80, 80, 80);
					List<Player> factionMembers = new ArrayList<Player>();
					for (Entity e : entities) {
						if (!e.getType().equals(EntityType.PLAYER))
							continue;
						Player ep = (Player) e;
						FPlayer fp = FPlayers.getInstance().getByPlayer(ep);
						Faction fac = fp.getFaction();
						if (fac != null && fac.getId().equals(id)) {
							factionMembers.add(ep);
						}
					}
					for (Player fpl : factionMembers)
						if (fpl.getHealth() < 20 - enchant.getLevel())
							fpl.setHealth(fpl.getHealth() + enchant.getLevel());
				}

			}

		}
		enchant = EnchantUtils.getArmorEnchant(player, EnchantType.DECEIVER);
		if (enchant != null) {
			Random random = new Random();
			int next = random.nextInt(100);
			if (next < enchant.getLevel() * 5) {

				player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 30, 2));

			}
		}

		CustomEnchant molten = EnchantUtils.getArmorEnchant(player, EnchantType.MOLTEN);
		if (molten != null && RandomUtils.randomInt(100) <= molten.getLevel() * 2)
			damager.setFireTicks(molten.getLevel() * 20 * 2);

		CustomEnchant selfDestruct = EnchantUtils.getArmorEnchant(player, EnchantType.SELF_DESTRUCT);
		if (selfDestruct != null && RandomUtils.randomInt(100) <= selfDestruct.getLevel() * 4)
			player.getWorld().spawnEntity(player.getLocation(), EntityType.PRIMED_TNT);
	}

	@EventHandler
	public void onDamage(EntityDamageEvent e) {
		if (e.getEntityType().equals(EntityType.PLAYER)) {

			Player player = (Player) e.getEntity();

			int enlighted = EnchantUtils.stackArmorEnchant(player, EnchantType.IMBUED);
			if (enlighted != -1 && RandomUtils.randomInt(100) <= enlighted * 8
					&& player.getHealth() <= player.getMaxHealth() - 2) {
				player.setHealth((player).getHealth() + 2);
			}

			if (silence.containsKey(player))
				return;

			int angelic = EnchantUtils.stackArmorEnchant(player, EnchantType.ANGELIC);
			if (angelic != -1 && RandomUtils.randomInt(100) <= angelic * 5
					&& player.getHealth() <= player.getMaxHealth() - 2) {
				player.setHealth((player).getHealth() + 2);
			}

			CustomEnchant inversion = EnchantUtils.getEnchant(player.getItemInHand(), EnchantType.INVERSION);
			if (inversion != null && RandomUtils.randomInt(15) <= inversion.getLevel() / 2
					&& player.getHealth() <= player.getMaxHealth() - inversion.getLevel()) {
				player.setHealth((player).getHealth() + inversion.getLevel() / 2);
				player.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + " ** INVERSION  (+" + inversion.getLevel() / 2
						+ " HP)  ** ");
			}

			CustomEnchant block = EnchantUtils.getEnchant(player.getItemInHand(), EnchantType.BLOCK);
			if (block != null && player.isBlocking()) {
				e.setDamage(e.getDamage() * (1 - (block.getLevel() * 0.2)));
				player.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + " ** BLOCKED  ** ");
			}
		}
	}

}