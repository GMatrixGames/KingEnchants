package net.kingenchants.enchants.listeners.entity;

import java.util.Random;

import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.PlayerDeathEvent;

import net.kingenchants.enchants.CustomEnchant;
import net.kingenchants.enchants.EnchantType;
import net.kingenchants.listeners.CListener;
import net.kingenchants.utils.enchants.EnchantUtils;
import net.kingenchants.utils.enchants.ItemUtils;

public class Listener_PlayerDeathEvent extends CListener {

	@EventHandler(priority = EventPriority.LOW)
	public void on(PlayerDeathEvent e) {

		Player dead = e.getEntity();
		if (e.getEntity().getKiller().getType().equals(EntityType.PLAYER)) {
			Player killer = e.getEntity().getKiller();

			CustomEnchant ce = EnchantUtils.getEnchant(killer.getItemInHand(), EnchantType.DECAPITATION);
			if (ce != null) {

				int level = ce.getLevel();
				level *= 30;
				Random rand = new Random();
				int rnd = rand.nextInt(100);
				if (rnd < level) {

					killer.getWorld().dropItemNaturally(killer.getEyeLocation(),
							ItemUtils.getHead(dead, killer, killer.getItemInHand()));
					killer.getWorld().playSound(killer.getLocation(), Sound.GLASS, 200, 200);
				}

			}
			ce = EnchantUtils.getEnchant(killer.getItemInHand(), EnchantType.HEADLESS);
			if (ce != null) {

				int level = ce.getLevel();
				level *= 30;
				Random rand = new Random();
				int rnd = rand.nextInt(100);
				if (rnd < level) {

					killer.getWorld().dropItemNaturally(killer.getEyeLocation(),
							ItemUtils.getHead(dead, killer, killer.getItemInHand()));
					killer.getWorld().playSound(killer.getLocation(), Sound.GLASS, 200, 200);
				}

			}

		}

	}

}
