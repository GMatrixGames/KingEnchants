package net.kingenchants.enchants.listeners.entity;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;

import com.gmail.nossr50.events.experience.McMMOPlayerXpGainEvent;

import net.kingenchants.enchants.CustomEnchant;
import net.kingenchants.enchants.EnchantType;
import net.kingenchants.listeners.CListener;
import net.kingenchants.utils.enchants.EnchantUtils;

public class Listener_PlayerExpChange extends CListener {

	// Going to use this class for assorted listeners also!

	@EventHandler
	public void onEat(PlayerItemConsumeEvent e) {
		Player p = e.getPlayer();
		CustomEnchant ce = EnchantUtils.getArmorEnchant(p, EnchantType.SUSTENANCE);
		if (ce != null) {
			p.setFoodLevel(20);
			p.setSaturation(p.getSaturation() * 2);
		}

	}

	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		if (e.getFrom().getBlock() != e.getTo().getBlock()) {

			Random rand = new Random();
			int next = rand.nextInt(50);
			CustomEnchant implants = EnchantUtils.getArmorEnchant(e.getPlayer(), EnchantType.IMPLANTS);
			if (implants != null) {

				int fin = rand.nextInt(50);
				if (fin < implants.getLevel()) {
					if (implants.getLevel() == 3) {
						if (next > 25 && e.getPlayer().getHealth() < e.getPlayer().getMaxHealth() - 1) {
							e.getPlayer().setHealth(e.getPlayer().getHealth() + 1);
						}
						e.getPlayer().setFoodLevel(20);
					} else if (implants.getLevel() == 2) {
						if (next > 40 && e.getPlayer().getHealth() < e.getPlayer().getMaxHealth() - 1) {
							e.getPlayer().setHealth(e.getPlayer().getHealth() + 1);
						}
						e.getPlayer().setFoodLevel(20);
					} else {
						e.getPlayer().setFoodLevel(20);
					}
				}
			}

		}

	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void entityDeath(EntityDeathEvent e) {
		if (e.getEntity().getKiller() != null) {
			if (e.getEntity().getKiller().getType().equals(EntityType.PLAYER)) {
				Player killer = e.getEntity().getKiller();
				if (e.getEntity().getType().equals(EntityType.PLAYER))
					return;
				CustomEnchant ce = EnchantUtils.getEnchant(killer.getItemInHand(), EnchantType.INQUISITIVE);
				if (ce != null) {
					killer.setTotalExperience(killer.getTotalExperience() + e.getDroppedExp() * ce.getLevel());
				}
			}
		}
	}

	@EventHandler
	public void onMCMMOXP(McMMOPlayerXpGainEvent e) {
		// Nimble - Increases mcMMO xp gained in Acrobatics whiled equiped.
		// (Boots)
		// Skilling - Increases mcMMO xp gained in all gathering skills while
		// equipped. (Tool)
		// Cultivation - Increases mcMMO xp gained in all combat skills while
		// equipped. (Weapon)
		ItemStack boots = e.getPlayer().getInventory().getBoots(), hand = e.getPlayer().getItemInHand();

		CustomEnchant ce = EnchantUtils.getEnchant(boots, EnchantType.NIMBLE);
		if (ce != null) {
			e.setRawXpGained(e.getRawXpGained() * ce.getLevel());
		}

		ce = EnchantUtils.getEnchant(hand, EnchantType.SKILLING);
		if (ce != null) {
			e.setRawXpGained(e.getRawXpGained() * ce.getLevel());
		}

		ce = EnchantUtils.getEnchant(hand, EnchantType.CULTIVATION);
		if (ce != null) {
			e.setRawXpGained(e.getRawXpGained() * ce.getLevel());
		}

	}

	@EventHandler
	public void onInt(PlayerInteractEvent e) {
		if (e.getItem() == null)
			return;
		if (!e.getAction().equals(Action.LEFT_CLICK_BLOCK))
			return;
		if (e.getClickedBlock() == null)
			return;
		CustomEnchant ce = EnchantUtils.getEnchant(e.getItem(), EnchantType.OBSIDIAN_DESTROYER);
		if (ce != null) {
			Random rand = new Random();
			int next = rand.nextInt(100);
			if (next < ce.getLevel() + 1) {
				BlockBreakEvent be = new BlockBreakEvent(e.getClickedBlock(), e.getPlayer());
				Bukkit.getPluginManager().callEvent(be);
				if (!be.isCancelled()) {
					e.getClickedBlock().breakNaturally();
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void on(PlayerExpChangeEvent event) {
		Player player = event.getPlayer();
		// FPlayer fPlayer = FPlayers.getInstance().getByPlayer(player);
		// Faction faction = fPlayer.getFaction();

		CustomEnchant grinder = EnchantUtils.getEnchant(player.getItemInHand(), EnchantType.GRINDER);
		if (grinder != null)
			event.setAmount((int) Math.round(event.getAmount() * grinder.getLevel()));
	}

}
