package net.kingenchants.enchants.listeners.entity;

import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.projectiles.ProjectileSource;
import org.cryptolead.core.commons.RandomUtils;

import net.kingenchants.enchants.CustomEnchant;
import net.kingenchants.enchants.EnchantType;
import net.kingenchants.listeners.CListener;
import net.kingenchants.utils.enchants.EnchantUtils;

public class Listener_ProjectileHit extends CListener{
	
	@EventHandler
	public void on(ProjectileHitEvent e) {
		Projectile projectile = e.getEntity();
		ProjectileSource shooter = projectile.getShooter();
		
		if (shooter instanceof Player) {
			Player player = (Player) shooter;
			
			CustomEnchant lightning = EnchantUtils.getEnchant(player.getItemInHand(), EnchantType.LIGHTNING);
			if (lightning != null && RandomUtils.randomInt(100) <= lightning.getLevel() * 3)
					projectile.getLocation().getWorld().strikeLightning(projectile.getLocation()).setFireTicks(0);
			
		}
	}

}
