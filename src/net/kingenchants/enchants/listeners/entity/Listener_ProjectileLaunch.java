package net.kingenchants.enchants.listeners.entity;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.projectiles.ProjectileSource;

import net.kingenchants.KingEnchants;
import net.kingenchants.data.Temp;
import net.kingenchants.enchants.CustomEnchant;
import net.kingenchants.enchants.EnchantType;
import net.kingenchants.listeners.CListener;
import net.kingenchants.utils.enchants.EnchantUtils;

public class Listener_ProjectileLaunch extends CListener {
	
	@EventHandler
	public void on(ProjectileLaunchEvent event) {
		Projectile projectile = event.getEntity();
		ProjectileSource shooter = projectile.getShooter();
		
		if (shooter instanceof Player) {
			Player player = (Player) shooter;
			
			CustomEnchant longBow = EnchantUtils.getEnchant(player.getItemInHand(), EnchantType.LONG_BOW);
			if (projectile instanceof Arrow && player.getItemInHand() != null && longBow != null)
				Temp.DAMAGE_ARROWS.put((Arrow) projectile, (double) (KingEnchants.LONGBOW_DAMAGE * longBow.getLevel()));
		}
	}
}