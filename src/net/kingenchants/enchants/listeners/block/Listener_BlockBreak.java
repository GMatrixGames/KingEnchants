package net.kingenchants.enchants.listeners.block;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.sk89q.worldguard.blacklist.event.BlockBreakBlacklistEvent;
import com.sk89q.worldguard.protection.events.DisallowedPVPEvent;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.Flag;
import com.sk89q.worldguard.protection.flags.Flags;
import com.sk89q.worldguard.protection.flags.StateFlag.State;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import net.kingenchants.KingEnchants_Loader;
import net.kingenchants.enchants.CustomEnchant;
import net.kingenchants.enchants.EnchantType;
import net.kingenchants.listeners.CListener;
import net.kingenchants.utils.enchants.EnchantUtils;

public class Listener_BlockBreak extends CListener {

	private BlockFace current = null;
	private Block block = null;

	@EventHandler(priority = EventPriority.LOWEST)
	public void PostOn(BlockBreakEvent event) {
		Player player = event.getPlayer();
		if (event.isCancelled())
			return;
		CustomEnchant enchant = EnchantUtils.getEnchant(player.getItemInHand(), EnchantType.TELEPATHY);
		if (enchant != null) {

			for (ItemStack item : event.getBlock().getDrops())
				player.getInventory().addItem(item);

			event.setCancelled(true);

			event.getBlock().setType(Material.AIR);
		}

	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void on(BlockBreakEvent event) {
		Player player = event.getPlayer();

		// YoDontBeThatGirl

		CustomEnchant enchant = EnchantUtils.getEnchant(player.getItemInHand(), EnchantType.EXPERIECE);
		if (enchant != null) {
			player.setExp(player.getExp() + (enchant.getLevel() * 3));
		}

		enchant = EnchantUtils.getEnchant(player.getItemInHand(), EnchantType.DETONATE);
		if (enchant != null) {
			detonate(enchant, player);
		}

		enchant = EnchantUtils.getEnchant(player.getItemInHand(), EnchantType.AUTO_SMELT);
		if (enchant != null) {
			Material m = event.getBlock().getType();
			if (m.equals(Material.GOLD_ORE)) {
				event.getBlock().getWorld().playEffect(event.getBlock().getLocation(), Effect.SMALL_SMOKE, 5);
				event.setCancelled(true);

				int xp = event.getExpToDrop();
				ItemStack ore = new ItemStack(Material.GOLD_INGOT, enchant.getLevel());
				event.getBlock().setType(Material.AIR);
				event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), ore);
				player.setExp(player.getExp() + xp * enchant.getLevel());
			} else if (m.equals(Material.IRON_ORE)) {
				event.getBlock().getWorld().playEffect(event.getBlock().getLocation(), Effect.SMALL_SMOKE, 5);
				event.setCancelled(true);
				int xp = event.getExpToDrop();
				event.getBlock().setType(Material.AIR);
				ItemStack ore = new ItemStack(Material.IRON_INGOT, enchant.getLevel());
				event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), ore);
				player.setExp(player.getExp() + xp * enchant.getLevel());
			}
		}

		enchant = EnchantUtils.getArmorEnchant(player, EnchantType.OXYGENATE);
		if (enchant != null) {
			player.setRemainingAir(200);
		}

	}

	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
	public void playerInteractEvent(PlayerInteractEvent e) {
		if (e.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
			block = e.getClickedBlock();
			current = e.getBlockFace();
		}
	}

	private void detonate(CustomEnchant ce, Player p) {
		int level = ce.getLevel();
		int distance = level / 3;
		Random rand = new Random();
		switch (level) {
		case 1:
			distance = rand.nextDouble() < 0.33 ? 1 : 0;
			break;
		case 2:
			distance = rand.nextDouble() < 0.66 ? 1 : 0;
			break;
		case 3:
			distance = 1;
			break;
		case 4:
			distance = rand.nextDouble() < 0.33 ? 2 : 1;
			break;
		case 5:
			distance = rand.nextDouble() < 0.66 ? 2 : 1;
			break;
		case 6:
			distance = 2;
			break;
		case 7:
			distance = rand.nextDouble() < 0.33 ? 3 : 2;
			break;
		case 8:
			distance = rand.nextDouble() < 0.66 ? 3 : 2;
			break;
		case 9:
			distance = 3;
			break;
		default:
			break;
		}
		List<BlockFace> faces = new ArrayList<BlockFace>();
		faces.add(BlockFace.UP);
		faces.add(BlockFace.DOWN);
		faces.add(BlockFace.NORTH);
		faces.add(BlockFace.EAST);
		faces.add(BlockFace.SOUTH);
		faces.add(BlockFace.WEST);
		if (current.equals(BlockFace.DOWN) || current.equals(BlockFace.UP)) {
			faces.remove(BlockFace.DOWN);
			faces.remove(BlockFace.UP);
		} else if (current.equals(BlockFace.EAST) || current.equals(BlockFace.WEST)) {
			faces.remove(BlockFace.EAST);
			faces.remove(BlockFace.WEST);
		} else if (current.equals(BlockFace.NORTH) || current.equals(BlockFace.SOUTH)) {
			faces.remove(BlockFace.NORTH);
			faces.remove(BlockFace.SOUTH);
		}
		Block centre = block;
		for (int i = 0; i < distance; i++) {
			bbreak(p, centre);
			for (BlockFace f : faces) {
				bbreak(p, centre.getRelative(f));
			}
			centre = centre.getRelative(current);
		}

	}
	
	private void bbreak(Player p, Block b) {
		for (ProtectedRegion region : KingEnchants_Loader.getInstance().getWorldGuard().getRegionManager(b.getWorld()).getRegions().values()) {
			if (!region.contains(b.getX(), b.getY(), b.getZ())) continue;
			if (region.getOwners().contains(p.getUniqueId())) continue;
			if (region.getFlags().containsKey(DefaultFlag.BLOCK_BREAK)) {
				if (region.getFlag(DefaultFlag.BLOCK_BREAK) == State.DENY)
					return;
			}
		}
		if (b.getType().equals(Material.STONE))
			b.breakNaturally();
	}
}