package net.kingenchants.enchants;

import org.bukkit.Color;
import org.cryptolead.core.utils.ChatUtils;

public enum EnchantRarity {
	BASIC(0, "Basic", ChatUtils.Color.AQUA, (short) 3, 20, 2500, 1, 5, Color.AQUA),
	COMMON(1, "Common", ChatUtils.Color.GREEN, (short) 5, 35, 5000, 1, 10, Color.GREEN),
	RARE(2, "Rare", ChatUtils.Color.YELLOW, (short) 4, 50, 10000, 1, 15, Color.YELLOW),
	EPIC(3, "Epic", ChatUtils.Color.LIGHT_PURPLE, (short) 2, 65, 25000, 1, 20, Color.FUCHSIA),
	LEGENDARY(4, "Legendary", ChatUtils.Color.GOLD, (short) 1, 80, 50000, 1, 25, Color.ORANGE);
	
	private String
		name;
	
	private String
		color;
	
	private int
		value,
		expPrice,
		vaultPrice,
		minDust,
		maxDust;
	
	private short
		blockColor;
	
	private Color
		c;

    EnchantRarity(int value, String name, String color, short blockColor, int expPrice, int vaultPrice, int minDust, int maxDust, Color c) {
    	this.value = value;
        this.name = name;
        this.color = color;
        this.expPrice = expPrice;
        this.vaultPrice = vaultPrice;
        this.blockColor = blockColor;
        this.minDust = minDust;
        this.maxDust = maxDust;
        this.c = c;
    }
    
    public Color getC() {
    	return c;
    }
    
    public void setC(Color c) {
    	this.c = c;
    }
    
    public int getValue() {
    	return value;
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getColor() {
    	return this.color;
    }
    
    public short getBlockColor() {
    	return this.blockColor;
    }
    
    public int getExpPrice() {
    	return this.expPrice;
    }
    
    public int getVaultPrice() {
    	return this.vaultPrice;
    }
    
    public int getMinimumDust() {
    	return minDust;
    }
    
    public int getMaximumDust() {
    	return maxDust;
    }
    
    public static EnchantRarity getRarity(String name) {
    	for (EnchantRarity rarity : values())
    		if (rarity.toString().equalsIgnoreCase(name))
    			return rarity;
    	
    	return null;
    }
}