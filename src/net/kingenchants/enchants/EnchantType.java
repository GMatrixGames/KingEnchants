package net.kingenchants.enchants;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public enum EnchantType {

	
	
	
	AQUATIC("Aquatic", EnchantRarity.BASIC, "Gives permanent water breathing. ", 1, AllowedItems.HELMET), // -
	CONFUSION("Confusion", EnchantRarity.BASIC, "A chance to deal nausea to your victim. ", 3, AllowedItems.AXE), // -
	GLOWING("Glowing", EnchantRarity.BASIC, "Gives permanent night vision. ", 1, AllowedItems.HELMET), // -
	HASTE("Haste", EnchantRarity.BASIC, "Allows you to swing your tools faster. ", 3, AllowedItems.PICKAXE), // -
	LIGHTNING("Lightning", EnchantRarity.BASIC, "A chance to strike lightning where you strike. ", 3,
			AllowedItems.BOW), //
	INSOMNIA("Insomnia", EnchantRarity.BASIC, "Gives slowness, slow swinging, and confusion.", 7, AllowedItems.SWORD), //
	THUNDERING_BLOW("Thundering Blow", EnchantRarity.BASIC, "Can cause smite effect on your enemy ", 3,
			AllowedItems.SWORD), BERSERK("Berserk", EnchantRarity.COMMON, "A chance of strength and mining fatigue. ",
					5, AllowedItems.AXE), //
	COMMANDER("Commander", EnchantRarity.COMMON, "Nearby allies are given haste. ", 5, AllowedItems.ARMOR), CURSE(
			"Curse", EnchantRarity.COMMON, "Chance to cause mining fatigue. ", 5, AllowedItems.ARMOR), //
	ENDER_SHIFT("Ender Shift", EnchantRarity.COMMON, "Gives a speed/health boost at low hp. ", 3, AllowedItems.HELMET), // -
	EXPERIECE("Experience", EnchantRarity.COMMON, "Gives more exp when mining blocks. ", 5, AllowedItems.PICKAXE), //
	FEATHERWEIGHT("Featherweight", EnchantRarity.COMMON, "A chance to give a burst of haste.", 3, AllowedItems.SWORD), //
	LIFEBLOOM("Lifebloom", EnchantRarity.COMMON, "Completely heals allies and truces on your death. ", 5,
			AllowedItems.ARMOR), // Add Factions
	MOLTEN("Molten", EnchantRarity.COMMON, "Chance to light your attacker. ", 4, AllowedItems.ARMOR), // -
	RAVENOUS("Ravenous", EnchantRarity.COMMON, "A chance to regain hunger.", 4, AllowedItems.AXE), //
	SELF_DESTRUCT("Self Destruct", EnchantRarity.COMMON, "When close to death, buffed TNT spawns around you. ", 3,
			AllowedItems.ARMOR), //
	ANTI_GRAVITY("Anti Gravity", EnchantRarity.RARE, "Super jump but does not negate fall damage. ", 3,
			AllowedItems.BOOTS), //
	BLIND("Blind", EnchantRarity.RARE, "A chance of causing blindness when attacking. ", 3, AllowedItems.SWORD), //
	EXECUTE("Execute", EnchantRarity.RARE, "Damage buff when your target is at low hp.", 7, AllowedItems.SWORD), // -
	IMPLANTS("Implants", EnchantRarity.RARE, "Passively heals +1 health and restores +1 hunger every few seconds. ",
			3, AllowedItems.HELMET), ICE_FREEZE("Ice Freeze", EnchantRarity.RARE,
					"A chance to freeze enemy in place. ", 1, AllowedItems.SWORD), //
	PARALYZE("Paralyze", EnchantRarity.RARE,
			"Gives lightning effect and a chance for slowness and slow swinging. Also inflicts direct damage on proc.",
			4, AllowedItems.WEAPON), //
	POISON("Poison", EnchantRarity.RARE, "A chance of giving the poison effect.", 3, AllowedItems.WEAPON), //
	WITHER("Wither", EnchantRarity.RARE, "A chance to give the wither effect. ", 5, AllowedItems.WEAPON), //
	ANGELIC("Angelic", EnchantRarity.EPIC, "Heals health over time whenever damaged.", 5, AllowedItems.ARMOR), //
	ARROW_LIFESTEAL("Arrow Lifesteal", EnchantRarity.EPIC, "A chance to steal health from opponent while fighting. ",
			5, AllowedItems.BOW),
	BLESSED("Blessed", EnchantRarity.EPIC, "Removed enchant debuffs", 4, AllowedItems.AXE),
	BLOCK("Block", EnchantRarity.EPIC, "A chance to increase damage and redirect an attack. ", 3,
			AllowedItems.SWORD), CLEAVE("Cleave", EnchantRarity.EPIC,
					"Damages players within a radius that increases with the level of enchant. ", 7,
					AllowedItems.AXE), // Add Factions
	ENDER_WALKER("Ender Walker", EnchantRarity.EPIC,
			"Wither and Poison do not injure and have a chance to heal at high levels. ", 5, AllowedItems.BOOTS),
	DEMONFORGED("Demonforged", EnchantRarity.EPIC, "Increases amount armor is destroyed", 4, AllowedItems.SWORD),
	ENRAGE("Enrage", EnchantRarity.EPIC, "The lower your HP is, the more damage you deal. ", 5,
			AllowedItems.WEAPON), GUARDIANS("Guardians", EnchantRarity.EPIC,
					"Chance to spawn iron golems to assist you and watch over you.", 5,
					AllowedItems.ARMOR), LONG_BOW("Long Bow", EnchantRarity.EPIC,
							"Greatly increases damage dealt to enemy players that have a bow in their hands.", 4,
							AllowedItems.BOW), // -
	OBSIDIAN_SHIELD("Obsidian Shield", EnchantRarity.EPIC, "Grants permenant fire resistance.", 1, AllowedItems.ARMOR),
	PIERCING("Piercing", EnchantRarity.EPIC, "Inflicts more damage. ", 5, AllowedItems.BOW), DISINTEGRATE(
			"Disintegrate", EnchantRarity.LEGENDARY,
			"Chance to deal double durability damage to all enemy armor with every attack. ", 5, AllowedItems.SWORD),
	 TIPSY("Drunk", EnchantRarity.LEGENDARY, "Gives strength and slowness based on level.", 4, AllowedItems.HELMET),
	 PRIMITIVE("Barbarian", EnchantRarity.LEGENDARY, "Does more damage to people holding an axe", 4, AllowedItems.AXE),
	INQUISITIVE("Inquisitive", EnchantRarity.LEGENDARY, "Increases EXP drops from mobs. ", 5, AllowedItems.SWORD),
	SPEED("Speed", EnchantRarity.LEGENDARY, "Gives permenant speed boost.", 3, AllowedItems.BOOTS),
	DOUBLE_STRIKE("Double Strike", EnchantRarity.LEGENDARY, "A chance to strike twice. ", 5,
			AllowedItems.SWORD), GRINDER("Kill Aura", EnchantRarity.LEGENDARY, "Gives more XP when killing mobs.", 3,
					AllowedItems.SWORD), // -
	IMBUED("Enlighted", EnchantRarity.LEGENDARY, "Gives health when hit.", 3, AllowedItems.ARMOR),
	INVERSION("Inversion", EnchantRarity.LEGENDARY,
			"Damage dealt to you has a chance to be blocked and heal you for 1-4 HP instead. ", 4,
			AllowedItems.SWORD),

	/******************************************************************************************
	Here marks the start of where YoDontBeThatGirl began coding! If you would
	like anything changed and are not sure how to message me on discord:
	YoDontBeThatGirl#7977
	
	name, rarity, description, maxLevel, allowedItems 
	 
	Simple Enchants 
	******************************************************************************************/

	AUTO_SMELT("Auto Smelt", simple(), "Ores are automatically smelted when mined.", 3, AllowedItems.TOOLS), //works
	DECAPITATION("Decapitation", simple(), "Victims have a chance of dropping their head on death,", 3, AllowedItems.AXE), //works
	EPICNESS("Epicness", simple(), "Gives particles and sound effects.", 3, AllowedItems.WEAPON), // works
	HEADLESS("Headless", simple(), "Victims have a chance of dropping their head on death.", 3, AllowedItems.SWORD), //works
	OBLITERATE("Obliterate", simple(), "Hits with extreme knockback.", 5, AllowedItems.WEAPON), //works
	OXYGENATE("Oxygenate", simple(), "Refills oxygen levels when breaking blocks underwater.", 1, AllowedItems.ARMOR), //works
	
	
	/******************************************************************************************
	 Unique Enchants
	 ******************************************************************************************/
	
	DEEP_WOUNDS("Deep Wounds", unique(), "Increases the chance of the bleed effect.", 3, AllowedItems.SWORD), //Appears to work
	EXPLOSIVE("Explosive", unique(), "Explosive arrows.", 5, AllowedItems.BOW), //works
	OBSIDIAN_DESTROYER("Obsidian Destroyer", unique(), "Chance to instantly break obsidian blocks.", 5, AllowedItems.PICKAXE), //works
	RAGDOLL("Ragdoll", unique(), "Whenever you take damage, you are pushed further back.", 4, AllowedItems.ARMOR), //works
	REFORGED("Reforged", unique(), "Protects tool durability, items will take longer to break.", 10, AllowedItems.SWORD), //works
	
	/******************************************************************************************
	 Elite Enchants
	 ******************************************************************************************/
	
	
	CACTUS("Cactus", elite(), "Acts like thorns, but doesn't affect durability.", 2, AllowedItems.ARMOR), //works
	DODGE("Dodge", elite(), "Chance to dodge physical enemy attacks, increased chance if sneaking.", 5, AllowedItems.ARMOR), //works
	FROZEN("Frozen", elite(), "Can cause slowness when attacked.", 3, AllowedItems.ARMOR), // works
	HIJACK("Hijack", elite(), "Chance to convert summoned enemy Guardians into your own when they are shot with an arrow.", 4, AllowedItems.BOW),
	ICE_ASPECT("Ice Aspect", unique(), "Chance of causing the slowness effect on your opponent.", 3, AllowedItems.SWORD),
	INFERNAL("Infernal", unique(), "Explosive fire effect.", 3, AllowedItems.BOW),
	METAPHYSICAL("Metaphysical", elite(), "A chance to resist the slowness given by enemy enchantments.", 4, AllowedItems.ARMOR),
	NIMBLE("Nimble", elite(), "Increases mcMMO xp gained in Acrobatics whiled equipped.", 10, AllowedItems.BOOTS),
	SUSTENANCE("Sustenance", elite(), "1-2x normal hunger replenishment.", 3, AllowedItems.ARMOR), //fail
	PUMMEL("Pummel", elite(), "Chance to slow down nearby enemy players for a short period of time.", 3, AllowedItems.AXE),
	ROCKET_ESCAPE("Rocket Escape", elite(), "Blast off into the air at low hp.", 3, AllowedItems.BOOTS), // fail goes off too early
	SHOCKWAVE("Shockwave",elite(), "Chance to push back your attacker.", 5, AllowedItems.ARMOR),
	SKILLING("Skilling", elite(), "Increases mcMMO xp gained in all gathering skills while equipped.", 10, AllowedItems.TOOLS),
	SKILL_SWIPE("Skill Swipe", elite(), "A chance to steal some of your enemy's exp every time you damage them.", 5, AllowedItems.WEAPON), //fail
	SNARE("Snare", elite(), "Chance to slow and fatigue enemies with projectiles. ", 4, AllowedItems.BOW), // fail
	SPIRIT_LINK("Spirit Link", elite(), "Heals nearby faction/ally members upon damage.", 5, AllowedItems.ARMOR),
	SPRINGS("Springs", elite(), "Gives additional jump boost.", 3, AllowedItems.BOOTS),
	STORMCALLER("Stormcaller", elite(), "Chance to smite your opponent.", 4, AllowedItems.ARMOR),
	TELEPATHY("Telepathy", elite(), "Automatically places blocks broken by tools in your inventory.", 1, AllowedItems.TOOLS),
	CULTIVATION("Cultivation", elite(), "Increases mcMMO xp gained in all combat skills while equipped.", 1, AllowedItems.WEAPON),
	AMBUSCADE("Ambuscade", elite(), "Chance to give buffed slowness effect to your opponent.", 1, AllowedItems.SWORD),
	DECEIVER("Deceiver", elite(), "When hit, you have a chance to go invisible for a few seconds.", 8, AllowedItems.ARMOR), //couldn't get it to work
	UNDEAD_RUSE("Undead Ruse", elite(), "When hit, you have a chance to spawn zombie hordes to distract and disorient your opponent.", 10, AllowedItems.BOOTS),
	GALLANTRY("Gallantry", elite(), "Gives damage resistance when low on hp.", 3, AllowedItems.ARMOR),
	ACIDITY("Acidity", elite(), "A chance of dealing poison.", 3, AllowedItems.SWORD),
	CONJURING("Conjuring", elite(), "Has a chance to deal weakness.", 3, AllowedItems.ARMOR),
	TRAP("Trap", elite(), "A chance to give a highly damaging slowness effect to your opponent.", 3, AllowedItems.SWORD),
	VAMPIRE("Vampire", elite(), "Receive half of the damage given as health for yourself when you attack an opponent.", 3, AllowedItems.SWORD),
	SMOKE_BOMB("Smoke Bomb", elite(), "When you are near death you will spawn a smoke bomb to distract your enemies.", 8, AllowedItems.ARMOR),
	
	
	/*******************************************************************************************
	 
	 Ultimate Enchants
	 
 	 Reflect - Increases hp of your npc if you get combat logged by 15% per level. (Helmet)
	 
	 ******************************************************************************************/
	ARMORED("Armored", ultimate(), "Decreases damage from enemy swords by 1.85% per level. This enchantment is stackable.", 4, AllowedItems.ARMOR),
	ARROW_BREAK("Arrow Break", ultimate(), "Chance for arrows to bounce off and do no damage to you whenever you're wielding an axe with the enchant.", 4, AllowedItems.ARMOR),
	ARROW_DEFLECT("Arrow Deflect", ultimate(), "Prevents you from being damaged by enemy arrows more often than once. Every level x 200 milliseconds.", 4, AllowedItems.AXE),
	BLEED("Bleed", ultimate(), "Applies bleed stacks to enemies that decrease their movement speed. Use in combination with Ravage.", 6, AllowedItems.AXE),
	DETONATE("Detonate", ultimate(), "Summons up to a 3x3x3 explosion around any blocks you break.", 9, AllowedItems.TOOLS),
	FARCAST("Farcast", ultimate(), "Chance to knockback melee attackers by a couple blocks when they hit you. The lower your health, the higher the chance to knock.", 4, AllowedItems.ARMOR),
	HARDENED("Hardened", ultimate(), "Armor takes less durability damage.", 3, AllowedItems.ARMOR),
	HEAVY("Heavy", ultimate(), "Decreases damage from bows by 2% per level, and is stackable.", 5, AllowedItems.ARMOR),
	HELLFIRE("Hellfire", ultimate(), "All arrows shot by you turn into explosive fireballs.", 5, AllowedItems.BOW),
	LUCKY("Lucky", ultimate(), "You will find yourself more lucky in all server situations.", 10, AllowedItems.ARMOR),
	MARKSMAN("Marksman", ultimate(), "Increases damage dealt with bows this enchantment is stackable.", 4, AllowedItems.ARMOR),
	PACIFY("Pacify", ultimate(), "A chance to pacify your target, preventing them from building rage stacks for 1-3 seconds depending on level.", 3, AllowedItems.BOW),
	SILENCE("Silence", ultimate(), "Chance to stop activation of your enemies custom enchants.", 4, AllowedItems.SWORD),
	//REFLECT("Reflect", ultimate(), "Increases hp of your npc if you get combat logged by 15% per level.", 10, AllowedItems.HELMET),
	CHAIN("Chain", ultimate(), "Prevents mobs spawned from mob spawners from suffering from knockback from your attacks.", 1, AllowedItems.WEAPON),
	TANK("Tank", ultimate(), "Decreases damage by enemy axes by 1.85% per level. This enchantment is stackable.", 4, AllowedItems.ARMOR),
	UNFOCUS("Unfocus", ultimate(), "Chance to unfocus target player, reducing their outgoing bow damage by 50% for up to 10 seconds.", 5, AllowedItems.BOW),
	
	/******************************************************************************************
	 Legendary Enchants
	 ******************************************************************************************/
	
	JUGGERNAUT("Overload", legendary(), "Gains 2x the Overload level of extra hearts.", 3, AllowedItems.ARMOR),  	
	LUCIDITY("Clarity", legendary(), "Immune to blindness up to level of clarity enchantment.", 3, AllowedItems.HELMET),
	HEADSHOT("Sniper", legendary(), "Headshots with projectile items deal up to 3.5x damage.", 5, AllowedItems.BOW),
	ANIMOSITY("Rage", legendary(), "For every combo hit you land, your damage is multiplied by 1.1x.", 6, AllowedItems.WEAPON), 
	BLOOD_TRANSFUSION("Blood Transfusion", legendary(), "A chance to heal you for 1-2hp whenever your Guardians take damage.", 3, AllowedItems.ARMOR),
	RAPIER("Rapier", legendary(), "Multiplies damage against players who are wielding a bow at the time they are hit.", 5, AllowedItems.SWORD),
	LIFESTEAL("Lifesteal", legendary(), "Heals you when you hit people with a sword", 5, AllowedItems.SWORD),
	
	BLOOD_LUST("Blood Lust", legendary(), "Heals you whenever an enemy player within 7x7 blocks is damaged by the Bleed enchantment.", 6, AllowedItems.ARMOR),
	DESTRUCTION("Destruction", legendary(), "Damage all opponents nearby and debuff when you hit them.", 5, AllowedItems.ARMOR),
	
	;
		
	private EnchantRarity rarity;

	private String name;

	private String description;

	private int maxLevel;

	private AllowedItems allowedItems;

	private static EnchantRarity simple(){
		return EnchantRarity.BASIC;
	}
	private static EnchantRarity unique(){
		return EnchantRarity.COMMON;
	}
	private static EnchantRarity elite() {
		return EnchantRarity.RARE; 
	}
	private static EnchantRarity ultimate() {
		return EnchantRarity.EPIC; 
	}
	private static EnchantRarity legendary() {
		return EnchantRarity.LEGENDARY; 
	}
	EnchantType(String name, EnchantRarity rarity, String description, int maxLevel, AllowedItems allowedItems) {
		this.rarity = rarity;
		this.name = name;
		this.description = description;
		this.maxLevel = maxLevel;
		this.allowedItems = allowedItems;
	}

	public EnchantRarity getRarity() {
		return this.rarity;
	}

	public String getName() {
		return this.name;
	}

	public String getDescription() {
		return this.description;
	}

	public int getMaximumLevel() {
		return this.maxLevel;
	}

	public AllowedItems getAllowedItems() {
		return allowedItems;
	}

	public static EnchantType getType(String id) {
		for (EnchantType type : values())
			if (type.toString().equalsIgnoreCase(id))
				return type;
		return null;
	}

	public static EnchantType getTypeByName(String name) {
		for (EnchantType type : values())
			if (type.getName().equalsIgnoreCase(name))
				return type;
		return null;
	}

	public static void main(String[] args) {
		String fileName = "C:\\Users\\oranm\\Documents\\text.txt";

		String line = null;

		try {
			FileReader fileReader = new FileReader(fileName);

			BufferedReader bufferedReader = new BufferedReader(fileReader);

			List<String> lines = new ArrayList<>();
			while ((line = bufferedReader.readLine()) != null)
				lines.add(line);

			Map<String, Integer> map = new HashMap<>();
			Map<String, String> lore = new HashMap<>();
			for (int i = 0; i < lines.size(); i++) {
				EnchantType type = null;
				for (EnchantType t : values())
					if (lines.get(i).startsWith(t.getName().replace(" ", ""))) {
						type = t;
						break;
					}

				if (type == null)
					continue;

				map.put(type.getName(), Integer.parseInt(lines.get(i + 4).replace("  max-level: ", "")));

				StringBuilder sb = new StringBuilder();
				int j = i + 7;
				while (lines.get(j).startsWith("  - \"")) {
					sb.append(lines.get(j).replace("  - \"", "").replace("\"", "").replaceAll("&e", "") + " ");
					j++;
				}

				lore.put(type.getName(), sb.toString());
			}

			for (Entry<String, Integer> e : map.entrySet()) {
				System.out.println(e.getKey() + " " + e.getValue());
				System.out.println(lore.get(e.getKey()));
				System.out.println("-----");
			}
			System.out.println(map.size());
			bufferedReader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}