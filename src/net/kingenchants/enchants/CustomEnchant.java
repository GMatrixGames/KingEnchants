package net.kingenchants.enchants;

public class CustomEnchant {
	
	private EnchantType
		type;
	
	private int
		level;
	
	public CustomEnchant(EnchantType type, int level) {
		this.type = type;
		this.level = level;
	}
	
	public int getLevel() {
		return this.level;
	}
	
	public EnchantType getType() {
		return this.type;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof CustomEnchant))
			return false;
		
		return ((CustomEnchant) obj).getType().equals(type) && ((CustomEnchant) obj).getLevel() == level;
	}
}