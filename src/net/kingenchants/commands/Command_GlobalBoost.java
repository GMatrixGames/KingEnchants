package net.kingenchants.commands;

import org.bukkit.command.CommandSender;
import org.cryptolead.core.commons.MathUtils;

import net.kingenchants.KingEnchants;
import net.kingenchants.chat.M;
import net.kingenchants.data.Temp;
import net.kingenchants.utils.CRunnable;

public class Command_GlobalBoost {
	
	public static void run(CommandSender sender, String[] args) {
		
		if (!sender.hasPermission("king.gboost") && !sender.hasPermission("king.*")) {
			KingEnchants.sendMessage(sender, M.COMMAND_NO_PREMISSIONS);
			
			return;
		}
		
		if (args.length < 2) {
			KingEnchants.sendMessage(sender, M.CMD_GLOBALBOOST_HELP);
			
			return;
		}
		
		if (!MathUtils.isDouble(args[0])) {
			KingEnchants.sendMessage(sender, M.CMD_GLOBALBOOST_INVALID_MULTIPLIER);
			
			return;
		}
		
		double multiplier = Integer.parseInt(args[0]);
		
		if (!MathUtils.isInteger(args[1])) {
			KingEnchants.sendMessage(sender, M.CMD_GLOBALBOOST_INVALID_TIME);
			
			return;
		}
		
		int time = Integer.parseInt(args[1]);
		if (time < 1) {
			KingEnchants.sendMessage(sender, M.CMD_GLOBALBOOST_INVALID_TIME);
			
			return;
		}
		
		Temp.GLOBAL_XP_MULTIPLIER = multiplier;
		
		KingEnchants.sendMessage(sender, M.CMD_BOOST, multiplier, time);
		
		new CRunnable() {
			
			@Override
			public void run() {
				if (timer == 0) {
					cancel();
					
					Temp.GLOBAL_XP_MULTIPLIER = -1;
				}
				
				timer--;
			}
		}.activate(20 * 60, time);
	}
}