package net.kingenchants.commands;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.cryptolead.core.CryptoCore;
import org.cryptolead.core.plugin.CException;

import net.kingenchants.KingEnchants;
import net.kingenchants.chat.M;
import net.kingenchants.data.players.P;

public abstract class CCommand extends Command {
	
	private String
		name, // The name of the command.
		permission; // The permission of the command.
	
	private Set<String>
		commands = new HashSet<>(); // The commands of the command.
	
	/**
	 * Creates new instance of Command.
	 * @param name the name of the command.
	 * @param permission the permission of the command. Null for none.
	 * @param commands the commands of the command.
	 */
	public CCommand(String name, String permission, String... commands) {
		super(name);
		
		this.name = name;
		this.permission = permission;
		this.commands.addAll(Arrays.asList(commands));
	}
	
	/**
	 * Gets the name of the command.
	 * @return the name.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Gets the permission of the command.
	 * @return permission.
	 */
	public String getPermission() {
		return KingEnchants.COMMAND_PERMISSIONS_PREFIX + permission;
	}
	
	/**
	 * Gets the commands of the command.
	 * @return the commands.
	 */
	public Set<String> getCommands() {
		return commands;
	}
	
	@Override
	public boolean execute(CommandSender sender, String commandLabel, String[] args) {
		if (sender instanceof Player && !((Player) sender).isOp()) {
			Player player = (Player) sender;
			try {
				if (getPermission() != null && !P.get(player).hasPermission(getPermission())) {
					P.sendMessage(player, M.COMMAND_NO_PREMISSIONS);
					
					return false;
				}
			} catch (CException e) {
				CryptoCore.handleException(e);
				return false;
			}
		}
		
		run(sender, args);
		
		return false;
	}
	
	/**
	 * Performs the command.
	 * @param sender the sender of the command.
	 * @param args the arguments of the command.
	 */
	public abstract void run(CommandSender sender, String[] args);
	
	/**
	 * Registers the command.
	 */
	public void register() {
		for (String cmd : commands)
			KingEnchants.getCommandMap().register(cmd, "", this);
	}
}