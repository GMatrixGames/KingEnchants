package net.kingenchants.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.cryptolead.core.CryptoCore;
import org.cryptolead.core.plugin.CException;

import net.kingenchants.KingEnchants;
import net.kingenchants.chat.M;
import net.kingenchants.data.players.P;

public class Command_KingEnchants extends CCommand {
	
	public Command_KingEnchants() {
		super("King Enchants", null, "kingenchants", "ke");
	}
	
	@Override
	public void run(CommandSender sender, String[] args) {
		if  (sender instanceof Player) {
			Player player = (Player) sender;
			
			try {
				if (!P.get(player).hasPermission("king.givebook") && !P.get(player).hasPermission("king.give")
						&& !P.get(player).hasPermission("king.fboost") && !P.get(player).hasPermission("king.gboost") && !P.get(player).hasPermission("king.item")) {
					KingEnchants.sendMessage(sender, M.COMMAND_NO_PREMISSIONS);
					
					return;
				}
			} catch (CException e) {
				CryptoCore.handleException(e);
				return;
			}
		}
		
		if (args.length < 1) {
			KingEnchants.sendMessage(sender, M.CMD_KINGENCHANTS_HELP);
			
			return;
		}
		
		List<String> newArgsList = new ArrayList<>();
		for (int i = 1; i < args.length; i++)
			newArgsList.add(args[i]);
		String[] newArgs = newArgsList.toArray(new String[newArgsList.size()]);
		
		if (args[0].equalsIgnoreCase("givebook"))
			Command_GiveBook.run(sender, newArgs);
		else if (args[0].equalsIgnoreCase("give"))
			Command_Give.run(sender, newArgs);
		else if (args[0].equalsIgnoreCase("givedust"))
			Command_GiveDust.run(sender, newArgs);
		else if (args[0].equalsIgnoreCase("fboost"))
			Command_Boost.run(sender, newArgs);
		else if (args[0].equalsIgnoreCase("gboost"))
			Command_GlobalBoost.run(sender, newArgs);
		else if (args[0].equalsIgnoreCase("item"))
			Command_Item.run(sender, newArgs);
		else
			KingEnchants.sendMessage(sender, M.CMD_KINGENCHANTS_HELP);
	}
}