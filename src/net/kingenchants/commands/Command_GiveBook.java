package net.kingenchants.commands;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.cryptolead.core.commons.RandomUtils;
import org.cryptolead.core.utils.CommandUtils;
import org.cryptolead.core.utils.InventoryUtils;

import net.kingenchants.KingEnchants;
import net.kingenchants.chat.M;
import net.kingenchants.enchants.BookEnchant;
import net.kingenchants.enchants.CustomEnchant;
import net.kingenchants.enchants.EnchantType;
import net.kingenchants.utils.enchants.EnchantUtils;

public class Command_GiveBook {
	
	public static void run(CommandSender sender, String[] args) {
		
		if (!sender.hasPermission("king.givebook") && !sender.hasPermission("king.*")) {
			KingEnchants.sendMessage(sender, M.COMMAND_NO_PREMISSIONS);
			return;
		}
		
		if (args.length < 1) {
			KingEnchants.sendMessage(sender, M.CMD_GIVEBOOK_HELP);
			
			return;
		}
		
		Map<String, String> paramsNames = new HashMap<>();
		paramsNames.put("p", "PLAYER");
		paramsNames.put("l", "NUMBER");
		paramsNames.put("s", "NUMBER");
		paramsNames.put("d", "NUMBER");
		
		Map<String, Object> params = CommandUtils.getParameters(paramsNames, args);
		
		EnchantType type = EnchantType.getType(args[0]);
		if (type == null) {
			KingEnchants.sendMessage(sender, M.CMD_GIVEBOOK_INVALID_TYPE);
			
			return;
		}
		
		Player player = null;
		if (sender instanceof Player)
			player = (Player) sender;
		
		if (params.containsKey("p"))
			if (params.get("p") == null) {
				KingEnchants.sendMessage(sender, M.CMD_GIVEBOOK_INVALID_PLAYER);
				
				return;
			} else
				player = (Player) params.get("p");
		
		int level = 1;
		if (params.containsKey("l"))
			if (params.get("l") == null) {
				KingEnchants.sendMessage(sender, M.CMD_GIVEBOOK_INVALID_LEVEL);
				
				return;
			} else
				level = Integer.parseInt(String.valueOf(params.get("l")));
		
		if (level > type.getMaximumLevel() || level < 1) {
			KingEnchants.sendMessage(sender, M.CMD_GIVEBOOK_INVALID_LEVEL_MAXIMUM, type.getMaximumLevel());
			
			return;
		}
		
		if (level < 1) {
			KingEnchants.sendMessage(sender, M.CMD_GIVEBOOK_INVALID_LEVEL);
			
			return;
		}
		
		int success = -1;
		if (params.containsKey("s"))
			if (params.get("s") == null) {
				KingEnchants.sendMessage(sender, M.CMD_GIVEBOOK_INVALID_SUCCESS);
				
				return;
			} else
				success = Integer.parseInt(String.valueOf(params.get("s")));
		
		if (success != -1 && (success > 100 || success < 0)) {
			KingEnchants.sendMessage(sender, M.CMD_GIVEBOOK_INVALID_SUCCESS);
			
			return;
		}
		
		int destroy = -1;
		if (params.containsKey("d"))
			if (params.get("d") == null) {
				KingEnchants.sendMessage(sender, M.CMD_GIVEBOOK_INVALID_DESTROY);
				
				return;
			} else
				destroy = Integer.parseInt(String.valueOf(params.get("d")));
		
		if (destroy != -1 && (destroy > 100 || destroy < 0)) {
			KingEnchants.sendMessage(sender, M.CMD_GIVEBOOK_INVALID_DESTROY);
			
			return;
		}
		
		if (success == -1)
			success = RandomUtils.randomInt(0, 100);
		
		if (destroy == -1)
			destroy = RandomUtils.randomInt(0, 100);
		
		BookEnchant enchant = new BookEnchant(new CustomEnchant(type, level), success, destroy);
		ItemStack is = EnchantUtils.toBook(enchant);
		if (player != null)
			if (InventoryUtils.hasSpace(player.getInventory(), is))
				player.getInventory().addItem(is);
			else
				player.getWorld().dropItem(player.getLocation(), is);
		
		KingEnchants.sendMessage(sender, M.CMD_GIVEBOOK, type.getRarity().getColor(), type.getName(), level, success, destroy, player);
	}
}