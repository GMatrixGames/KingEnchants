package net.kingenchants.commands;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.cryptolead.core.utils.CommandUtils;
import org.cryptolead.core.utils.InventoryUtils;

import net.kingenchants.KingEnchants;
import net.kingenchants.chat.M;
import net.kingenchants.enchants.EnchantRarity;
import net.kingenchants.utils.enchants.EnchantUtils;

public class Command_Give {
	
	public static void run(CommandSender sender, String[] args) {
		
		if (!sender.hasPermission("king.give") && !sender.hasPermission("king.*")) {
			KingEnchants.sendMessage(sender, M.COMMAND_NO_PREMISSIONS);
			
			return;
		}
		
		if (args.length < 1) {
			KingEnchants.sendMessage(sender, M.CMD_GIVE_HELP);
			
			return;
		}
		
		Map<String, String> paramsNames = new HashMap<>();
		paramsNames.put("p", "PLAYER");
		paramsNames.put("a", "NUMBER");
		
		Map<String, Object> params = CommandUtils.getParameters(paramsNames, args);
		
		EnchantRarity rarity = EnchantRarity.getRarity(args[0]);
		if (rarity == null) {
			KingEnchants.sendMessage(sender, M.CMD_GIVE_INVALID_RARITY);
			
			return;
		}
		
		Player player = null;
		if (sender instanceof Player)
			player = (Player) sender;
		
		if (params.containsKey("p"))
			if (params.get("p") == null) {
				KingEnchants.sendMessage(sender, M.CMD_GIVE_INVALID_PLAYER);
				
				return;
			} else
				player = (Player) params.get("p");
		
		
		int level = 1;
		if (params.containsKey("a"))
			if (params.get("a") == null) {
				KingEnchants.sendMessage(sender, M.CMD_GIVE_INVALID_LEVEL);
				
				return;
			} else
				level = Integer.parseInt(String.valueOf(params.get("a")));
		
		if (level != -1 && level < 0) {
			KingEnchants.sendMessage(sender, M.CMD_GIVE_INVALID_LEVEL);
			
			return;
		}
		
		ItemStack is = EnchantUtils.toBaseBook(rarity, -1);
		if (level <= 64) {
		is.setAmount(level);
		}
		if (is != null)
			if (InventoryUtils.hasSpace(player.getInventory(), is))
				player.getInventory().addItem(is);
			else
				player.getWorld().dropItem(player.getLocation(), is);
		
	}
}