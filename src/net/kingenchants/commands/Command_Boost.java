package net.kingenchants.commands;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.cryptolead.core.commons.MathUtils;
import org.cryptolead.core.utils.CommandUtils;
import org.cryptolead.core.utils.InventoryUtils;

import net.kingenchants.KingEnchants;
import net.kingenchants.chat.M;
import net.kingenchants.utils.enchants.EnchantUtils;

public class Command_Boost {
	
	public static void run(CommandSender sender, String[] args) {
		
		if (!sender.hasPermission("king.fboost") && !sender.hasPermission("king.*")) {
			KingEnchants.sendMessage(sender, M.COMMAND_NO_PREMISSIONS);
			
			return;
		}
		
		if (args.length < 2) {
			KingEnchants.sendMessage(sender, M.CMD_BOOST_HELP);
			
			return;
		}
		
		Map<String, String> paramsNames = new HashMap<>();
		paramsNames.put("p", "PLAYER");
		
		Map<String, Object> params = CommandUtils.getParameters(paramsNames, args);
		
		if (!MathUtils.isDouble(args[0])) {
			KingEnchants.sendMessage(sender, M.CMD_BOOST_INVALID_MULTIPLIER);
			
			return;
		}
		
		double multiplier = Integer.parseInt(args[0]);
		
		if (!MathUtils.isInteger(args[1])) {
			KingEnchants.sendMessage(sender, M.CMD_BOOST_INVALID_TIME);
			
			return;
		}
		
		int time = Integer.parseInt(args[1]);
		if (time < 1) {
			KingEnchants.sendMessage(sender, M.CMD_BOOST_INVALID_TIME);
			
			return;
		}
		
		Player player = null;
		if (sender instanceof Player)
			player = (Player) sender;
		if (params.containsKey("p"))
			if (params.get("p") == null) {
				KingEnchants.sendMessage(sender, M.CMD_GIVE_INVALID_PLAYER);
				
				return;
			} else
				player = (Player) params.get("p");
		
		ItemStack is = EnchantUtils.toBooster(multiplier, time);
		if (is != null)
			if (InventoryUtils.hasSpace(player.getInventory(), is))
				player.getInventory().addItem(is);
			else
				player.getWorld().dropItem(player.getLocation(), is);
		
		KingEnchants.sendMessage(sender, M.CMD_BOOST, multiplier, time, player);
	}
}