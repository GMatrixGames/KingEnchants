package net.kingenchants.commands;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.cryptolead.core.CryptoCore;
import org.cryptolead.core.commons.RandomUtils;
import org.cryptolead.core.plugin.CException;
import org.cryptolead.core.utils.CommandUtils;
import org.cryptolead.core.utils.InventoryUtils;

import net.kingenchants.KingEnchants;
import net.kingenchants.chat.M;
import net.kingenchants.data.players.P;
import net.kingenchants.enchants.ScrollType;
import net.kingenchants.utils.enchants.EnchantUtils;

public class Command_Item {
	
	public static void run(CommandSender sender, String[] args) {
		
		if (!sender.hasPermission("king.item") && !sender.hasPermission("king.*")) {
			KingEnchants.sendMessage(sender, M.COMMAND_NO_PREMISSIONS);
			
			return;
		}
		
		if (args.length < 1) {
			KingEnchants.sendMessage(sender, M.CMD_ITEM_HELP);
			
			return;
		}
		
		Map<String, String> paramsNames = new HashMap<>();
		paramsNames.put("p", "PLAYER");
		
		Map<String, Object> params = CommandUtils.getParameters(paramsNames, args);
		
		ScrollType type = ScrollType.getScroll(args[0]);
		if (type == null) {
			KingEnchants.sendMessage(sender, M.CMD_ITEM_INVALID_TYPE);
			
			return;
		}
		
		Player player = null;
		if (sender instanceof Player)
			player = (Player) sender;
		
		if (params.containsKey("p"))
			if (params.get("p") == null) {
				KingEnchants.sendMessage(sender, M.CMD_ITEM_INVALID_PLAYER);
				
				return;
			} else
				player = (Player) params.get("p");
		
		try {
			if (!P.get((Player) sender).hasPermission("king.item.*") && !P.get((Player) sender).hasPermission("king.item." + type.toString().toLowerCase())) {
				KingEnchants.sendMessage(sender, M.COMMAND_NO_PREMISSIONS);
				
				return;
			}
		} catch (CException e) {
			CryptoCore.handleException(e);
			
			return;
		}
		
		
		ItemStack is = null;
		if (type.equals(ScrollType.BLACK))
			is = EnchantUtils.toBlackScroll(RandomUtils.randomInt(100));
		else
			is = EnchantUtils.toScroll(type);
		
		if (is != null)
			if (InventoryUtils.hasSpace(player.getInventory(), is))
				player.getInventory().addItem(is);
			else
				player.getWorld().dropItem(player.getLocation(), is);
		
		KingEnchants.sendMessage(sender, M.CMD_ITEM, type.getName(), player);
	}
}