package net.kingenchants.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.kingenchants.utils.gui.GUI;

public class Command_Tinkerer extends CCommand {
	
	public Command_Tinkerer() {
		super("Tinkerer", "king.tinkerer", "tinkerer");
	}
	
	@Override
	public void run(CommandSender sender, String[] args) {
		if  (!(sender instanceof Player))
			return;
		
		Player player = (Player) sender;
		
		GUI.openPreset(player, "tinkerer");
	}
}