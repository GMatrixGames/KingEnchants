package net.kingenchants.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.kingenchants.utils.gui.GUI;

public class Command_Enchants extends CCommand {
	
	public Command_Enchants() {
		super("Enchants", "king.enchants", "enchants");
	}
	
	@Override
	public void run(CommandSender sender, String[] args) {
		if  (!(sender instanceof Player))
			return;
		
		Player player = (Player) sender;
		
		GUI.openPreset(player, "enchants");
	}
}