package net.kingenchants.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.kingenchants.utils.gui.GUI;

public class Command_Enchanter extends CCommand {
	
	public Command_Enchanter() {
		super("Enchanter", "king.enchanter", "enchanter", "ce", "customenchants");
	}
	
	@Override
	public void run(CommandSender sender, String[] args) {
		if  (!(sender instanceof Player))
			return;
		
		Player player = (Player) sender;
		
		GUI.openPreset(player, "enchanter");
	}
}