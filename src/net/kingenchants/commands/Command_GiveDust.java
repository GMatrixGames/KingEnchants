package net.kingenchants.commands;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.cryptolead.core.commons.RandomUtils;
import org.cryptolead.core.utils.CommandUtils;
import org.cryptolead.core.utils.InventoryUtils;

import net.kingenchants.KingEnchants;
import net.kingenchants.chat.M;
import net.kingenchants.enchants.DustRarity;
import net.kingenchants.utils.enchants.EnchantUtils;

public class Command_GiveDust {
	
	public static void run(CommandSender sender, String[] args) {
		
		if (!sender.hasPermission("king.givedust") && !sender.hasPermission("king.*")) {
			KingEnchants.sendMessage(sender, M.COMMAND_NO_PREMISSIONS);
			
			return;
		}
		
		if (args.length < 1) {
			KingEnchants.sendMessage(sender, M.CMD_GIVEDUST_HELP);
			
			return;
		}
		
		Map<String, String> paramsNames = new HashMap<>();
		paramsNames.put("p", "PLAYER");
		paramsNames.put("a", "NUMBER");
		paramsNames.put("s", "NUMBER");
		Map<String, Object> params = CommandUtils.getParameters(paramsNames, args);
		
		DustRarity type = DustRarity.getRarity(args[0]);
		if (type == null) {
			KingEnchants.sendMessage(sender, M.CMD_GIVEDUST_INVALID_TYPE);
			
			return;
		}
		
		Player player = null;
		if (sender instanceof Player)
			player = (Player) sender;
		
		if (params.containsKey("p"))
			if (params.get("p") == null) {
				KingEnchants.sendMessage(sender, M.CMD_GIVEBOOK_INVALID_PLAYER);
				
				return;
			} else
				player = (Player) params.get("p");
		
		int level = 1;
		if (params.containsKey("a"))
			if (params.get("a") == null) {
				KingEnchants.sendMessage(sender, M.CMD_GIVEDUST_INVALID_AMOUNT);
				
				return;
			} else
				level = Integer.parseInt(String.valueOf(params.get("a")));
		
		if (level > 64 || level < 1) {
			KingEnchants.sendMessage(sender, M.CMD_GIVEBOOK_INVALID_LEVEL_MAXIMUM, type.getMaximumDust());
			
			return;
		}
		
		if (level < 1) {
			KingEnchants.sendMessage(sender, M.CMD_GIVEBOOK_INVALID_LEVEL);
			
			return;
		}
		
		int success = -1;
		if (params.containsKey("s"))
			if (params.get("s") == null) {
				KingEnchants.sendMessage(sender, M.CMD_GIVEBOOK_INVALID_SUCCESS);
				
				return;
			} else
				success = Integer.parseInt(String.valueOf(params.get("s")));
		
		if (success != -1 && (success > 100 || success < 0)) {
			KingEnchants.sendMessage(sender, M.CMD_GIVEBOOK_INVALID_SUCCESS);
			
			return;
		}
		
		
		if (success == -1)
			success = RandomUtils.randomInt(0, 100);
		
		
		
		ItemStack is = EnchantUtils.getRandomSuccessDustStack(level, type, success);
		
		if (player != null)
			if (InventoryUtils.hasSpace(player.getInventory(), is))
				player.getInventory().addItem(is);
			else
				player.getWorld().dropItem(player.getLocation(), is);
	}
}