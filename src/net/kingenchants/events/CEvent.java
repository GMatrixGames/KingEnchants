package net.kingenchants.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class CEvent extends Event {

	private static final HandlerList
		HANDLERS = new HandlerList(); // The handlers of the event.
	
	/**
	 * Gets the handlers of the event.
	 * @return the handlers.
	 */
	public HandlerList getHandlers() {
		return HANDLERS;
	}
	
	/**
	 * Gets the handlers of the event.
	 * @return the handlers.
	 */
	public static HandlerList getHandlerList() {
		return HANDLERS;
	}
}