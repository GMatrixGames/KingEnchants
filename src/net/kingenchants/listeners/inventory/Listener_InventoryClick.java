package net.kingenchants.listeners.inventory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.cryptolead.core.CryptoCore;
import org.cryptolead.core.commons.RandomUtils;
import org.cryptolead.core.plugin.CException;
import org.cryptolead.core.utils.InventoryUtils;
import org.cryptolead.core.utils.RomanNumber;

import net.kingenchants.KingEnchants;
import net.kingenchants.chat.M;
import net.kingenchants.data.Temp;
import net.kingenchants.data.players.P;
import net.kingenchants.enchants.AllowedItems;
import net.kingenchants.enchants.BookEnchant;
import net.kingenchants.enchants.CustomEnchant;
import net.kingenchants.enchants.EnchantRarity;
import net.kingenchants.enchants.ScrollType;
import net.kingenchants.listeners.CListener;
import net.kingenchants.utils.enchants.EnchantUtils;
import net.kingenchants.utils.gui.GUI;
import net.kingenchants.utils.gui.GUIClickEvent;

public class Listener_InventoryClick extends CListener {

	@SuppressWarnings("deprecation")
	@EventHandler
	public void on(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();
		Inventory inventory = event.getClickedInventory();

		if (inventory == null)
			return;

		int slot = event.getSlot();
		ItemStack is = inventory.getItem(slot);
		ItemStack current = event.getCursor();

		GUI gui = Temp.OPEN_GUIS.get(player.getUniqueId());

		if ((event.getAction().equals(InventoryAction.PLACE_ALL)
				|| event.getAction().equals(InventoryAction.SWAP_WITH_CURSOR))
				&& inventory.equals(player.getInventory())) {
			if (is == null || current == null)
				return;

			if (is.getType().equals(Material.AIR) || current.getType().equals(Material.AIR))
				return;

			ScrollType scroll = EnchantUtils.fromScroll(current);
			if (scroll != null) {
				if (scroll.equals(ScrollType.WHITE)) {
					boolean isAllowed = false;
					for (AllowedItems items : AllowedItems.values())
						if (items.isAllowed(is)) {
							isAllowed = true;
							break;
						}

					if (!isAllowed) {
						P.sendMessage(player, M.SCROLL_WHITE_CANT_APPLY_ITEM);

						return;
					}

					Entry<List<CustomEnchant>, Boolean> enchants = EnchantUtils.getEnchants(is);
					if (enchants != null && enchants.getValue()) {
						P.sendMessage(player, M.SCROLL_WHITE_ALREADY_ON);

						return;
					}

					event.setCancelled(true);

					ItemStack item = EnchantUtils.addWhiteScroll(is);
					if (item != null)
						inventory.setItem(slot, item);

					if (current.getAmount() == 1)
						event.setCursor(null);
					else {
						current.setAmount(current.getAmount() - 1);
						if (InventoryUtils.hasSpace(player.getInventory(), current))
							player.getInventory().addItem(current);
						else
							player.getWorld().dropItem(player.getLocation(), current);
					}

					P.sendMessage(player, M.SCROLL_WHITE_APPLY);
				}

				else if (scroll.equals(ScrollType.WORB)) {
					if (!AllowedItems.WEAPON.isAllowed(is)) {
						P.sendMessage(player, M.WORB_CANT_APPLY_ITEM);

						return;
					}

					if (EnchantUtils.hasOrb(is)) {
						P.sendMessage(player, M.WORB_ALREADY_ON);

						return;
					}

					event.setCancelled(true);

					ItemStack item = EnchantUtils.addOrb(is);
					if (item != null)
						inventory.setItem(slot, item);

					if (current.getAmount() == 1)
						event.setCursor(null);
					else {
						current.setAmount(current.getAmount() - 1);
						if (InventoryUtils.hasSpace(player.getInventory(), current))
							player.getInventory().addItem(current);
						else
							player.getWorld().dropItem(player.getLocation(), current);
					}

					P.sendMessage(player, M.WORB_APPLY);
				}

				else if (scroll.equals(ScrollType.AORB)) {
					if (!AllowedItems.ARMOR.isAllowed(is)) {
						P.sendMessage(player, M.AORB_CANT_APPLY_ITEM);

						return;
					}

					if (EnchantUtils.hasOrb(is)) {
						P.sendMessage(player, M.AORB_ALREADY_ON);

						return;
					}

					event.setCancelled(true);

					ItemStack item = EnchantUtils.addOrb(is);
					if (item != null)
						inventory.setItem(slot, item);

					if (current.getAmount() == 1)
						event.setCursor(null);
					else {
						current.setAmount(current.getAmount() - 1);
						if (InventoryUtils.hasSpace(player.getInventory(), current))
							player.getInventory().addItem(current);
						else
							player.getWorld().dropItem(player.getLocation(), current);
					}

					P.sendMessage(player, M.AORB_APPLY);
				}

				else if (scroll.equals(ScrollType.RANDOM)) {
					BookEnchant book = EnchantUtils.fromBook(is);
					if (book == null) {
						P.sendMessage(player, M.SCROLL_RANDOM_CANT_APPLY_ITEM);

						return;
					}

					event.setCancelled(true);

					book.setSuccessRate(RandomUtils.randomInt(100));
					book.setDestroyRate(RandomUtils.randomInt(100));

					ItemStack item = EnchantUtils.toBook(book);
					if (item != null)
						inventory.setItem(slot, item);

					if (current.getAmount() == 1)
						event.setCursor(null);
					else {
						current.setAmount(current.getAmount() - 1);
						if (InventoryUtils.hasSpace(player.getInventory(), current))
							player.getInventory().addItem(current);
						else
							player.getWorld().dropItem(player.getLocation(), current);
					}

					P.sendMessage(player, M.SCROLL_RANDOM_APPLY);
				}

				else if (scroll.equals(ScrollType.BLACK)) {
					int percent = EnchantUtils.fromBlackScroll(current);
					if (percent == -1)
						return;

					boolean isAllowed = false;
					for (AllowedItems items : AllowedItems.values())
						if (items.isAllowed(is)) {
							isAllowed = true;
							break;
						}

					if (!isAllowed) {
						P.sendMessage(player, M.SCROLL_BLACK_CANT_APPLY_ITEM);

						return;
					}

					Entry<List<CustomEnchant>, Boolean> enchants = EnchantUtils.getEnchants(is);
					if (enchants != null && enchants.getKey().size() == 0) {
						P.sendMessage(player, M.SCROLL_BLACK_NO_ENCHANTS);

						return;
					}

					event.setCancelled(true);

					CustomEnchant enchant = enchants.getKey().get(RandomUtils.randomInt(enchants.getKey().size() - 1));

					if (current.getAmount() == 1)
						event.setCursor(null);
					else {
						current.setAmount(current.getAmount() - 1);
						if (InventoryUtils.hasSpace(player.getInventory(), current))
							player.getInventory().addItem(current);
						else
							player.getWorld().dropItem(player.getLocation(), current);
					}

					if (RandomUtils.randomInt(100) <= percent) {
						ItemStack item = EnchantUtils.removeEnchant(is, enchant.getType());
						if (item != null)
							inventory.setItem(slot, item);

						ItemStack book = EnchantUtils.toBook(new BookEnchant(enchant, percent, 100));
						if (InventoryUtils.hasSpace(player.getInventory(), book))
							player.getInventory().addItem(book);
						else
							player.getWorld().dropItem(player.getLocation(), book);

						P.sendMessage(player, M.SCROLL_BLACK_APPLY, enchant.getType().getRarity().getColor(),
								enchant.getType().getName() + " " + RomanNumber.toRoman(enchant.getLevel()));
					} else {
						if (!enchants.getValue()) {
							ItemStack item = EnchantUtils.removeEnchant(is, enchant.getType());
							if (item != null)
								inventory.setItem(slot, item);

							P.sendMessage(player, M.SCROLL_BLACK_FAILED);
						} else {
							ItemStack item = EnchantUtils.removeWhiteScroll(is);
							if (item != null)
								inventory.setItem(slot, item);

							P.sendMessage(player, M.SCROLL_BLACK_FAILED_SAVED);
						}
					}

					return;
				}

				else if (scroll.equals(ScrollType.TRANSMOG)) {
					boolean isAllowed = false;
					for (AllowedItems items : AllowedItems.values())
						if (items.isAllowed(is)) {
							isAllowed = true;
							break;
						}

					if (!isAllowed) {
						P.sendMessage(player, M.SCROLL_TRANSMOG_CANT_APPLY_ITEM);

						return;
					}

					Entry<List<CustomEnchant>, Boolean> enchants = EnchantUtils.getEnchants(is);
					if (enchants != null && enchants.getKey().size() == 0) {
						P.sendMessage(player, M.SCROLL_TRANSMOG_NO_ENCHANTS);

						return;
					}

					event.setCancelled(true);

					ItemStack item = EnchantUtils.sortEnchants(is, enchants.getKey(), enchants.getValue());
					if (item != null)
						inventory.setItem(slot, item);

					if (current.getAmount() == 1)
						event.setCursor(null);
					else {
						current.setAmount(current.getAmount() - 1);
						if (InventoryUtils.hasSpace(player.getInventory(), current))
							player.getInventory().addItem(current);
						else
							player.getWorld().dropItem(player.getLocation(), current);
					}

					P.sendMessage(player, M.SCROLL_TRANSMOG_APPLY);
				}
				return;
			}

			int successRate = EnchantUtils.fromSuccessDust(current);
			if (successRate != -1) {
				BookEnchant enchant = EnchantUtils.fromBook(is);
				if (enchant == null)
					return;

				event.setCancelled(true);

				if (enchant.getSuccessRate() >= 100) {
					P.sendMessage(player, M.SUCCESS_DUST_ALREADY_MAX);
					return;
				}

				enchant.setSuccessRate(
						enchant.getSuccessRate() + successRate > 100 ? 100 : enchant.getSuccessRate() + successRate);
				ItemStack item = EnchantUtils.toBook(enchant);

				if (item != null)
					inventory.setItem(slot, item);

				if (current.getAmount() == 1)
					event.setCursor(null);
				else {
					current.setAmount(current.getAmount() - 1);
					// if (InventoryUtils.hasSpace(player.getInventory(),
					// current))
					// player.getInventory().addItem(current);
					// else
					// player.getWorld().dropItem(player.getLocation(),
					// current);
				}

				P.sendMessage(player, M.SUCCESS_DUST_APPLIED);

				return;
			}

			BookEnchant enchant = EnchantUtils.fromBook(current);
			if (enchant == null)
				return;
			if (EnchantUtils.fromBook(is) != null)
				return;

			Entry<List<CustomEnchant>, Boolean> enchantsBase = EnchantUtils.getEnchants(is);
			if (enchantsBase == null)
				return;

			List<CustomEnchant> enchants = enchantsBase.getKey();

			boolean hasPermission = false;
			for (int i = enchants.size() + (EnchantUtils.hasOrb(is) ? 0 : 1); i < KingEnchants.MAX_ENCHANTS; i++)
				try {
					if (P.get(player).hasPermission("king.enchants." + i)) {
						hasPermission = true;
						break;
					}
				} catch (CException e) {
					CryptoCore.handleException(e);
				}

			if (!enchant.getEnchant().getType().getAllowedItems().isAllowed(is)) {
				P.sendMessage(player, M.CANT_PUT_TYPE);

				return;
			}

			if (!hasPermission) {
				P.sendMessage(player, M.CANT_PUT_MORE_ENCHANTS);

				return;
			}

			boolean hasType = false;
			for (CustomEnchant e : enchants)
				if (e.getType().equals(enchant.getEnchant().getType())) {
					hasType = true;
					break;
				}

			if (hasType) {
				P.sendMessage(player, M.CANT_PUT_THIS_ENCHANT);

				return;
			}

			int chance = RandomUtils.randomInt(100);
			if (chance <= enchant.getSuccessRate()) {
				ItemStack item = EnchantUtils.addEnchant(is, enchant.getEnchant());
				if (item != null)
					inventory.setItem(slot, item);

				P.sendMessage(player, M.ENCHANT_APPLIED);

			} else {
				if (chance <= enchant.getDestroyRate()) {
					if (!enchantsBase.getValue()) {
						inventory.setItem(slot, null);
						P.sendMessage(player, M.ITEM_DESTROYED);
					} else {
						ItemStack item = EnchantUtils.removeWhiteScroll(is);
						if (item != null)
							inventory.setItem(slot, item);

						P.sendMessage(player, M.ENCHANT_NOT_APPLIED_SCROLL);
					}
				} else
					P.sendMessage(player, M.ENCHANT_NOT_APPLIED);
			}

			if (current.getAmount() == 1)
				event.setCursor(null);
			else {
				current.setAmount(current.getAmount() - 1);
				if (InventoryUtils.hasSpace(player.getInventory(), current))
					player.getInventory().addItem(current);
				else
					player.getWorld().dropItem(player.getLocation(), current);
			}

			event.setCancelled(true);
		}

		if (gui == null)
			return;

		boolean override = false;

		/*
		 * if (gui.getID().equals("tinkerer")) { List<ItemStack> books = new
		 * ArrayList<>(); for (int i = 0; i < 9; i++) for (int j = 0; j < 6;
		 * j++) if (i != 4 && !(i == 0 && j == 0) && !(i == 8 && j == 0)) {
		 * ItemStack book =
		 * player.getOpenInventory().getTopInventory().getItem(i + (j * 9)); if
		 * (book != null) books.add(book); }
		 * 
		 * books.add(is); GUI.openPreset(player, "tinkerer", books); }
		 */

		if (gui.getID().equals("tinkerer") && is != null) {
			if (inventory.equals(player.getInventory())) {
				event.setCancelled(true);

				if (EnchantUtils.fromBook(is) == null) {
					P.sendMessage(player, M.CANT_TINKER);
				} else {
					int firstSlot = 0;
					for (int i = 0; i < 6; i++) {
						for (int j = 0; j < 4; j++)
							if (player.getOpenInventory().getTopInventory().getItem((i * 9) + j) == null) {
								firstSlot = (i * 9) + j;
								break;
							}

						if (firstSlot > 0)
							break;
					}

					if (firstSlot > 0) {
						player.getInventory().setItem(slot, null);
						player.getOpenInventory().getTopInventory().setItem(firstSlot, is);

						Map<EnchantRarity, Integer> dusts = new HashMap<>();
						for (int j = 0; j < 6; j++)
							for (int i = 0; i < 4; i++)
								if (i + (j * 9) != 0) {
									ItemStack item = player.getOpenInventory().getTopInventory().getItem(i + (j * 9));
									BookEnchant enchant = EnchantUtils.fromBook(item);
									if (enchant == null)
										continue;

									int amount = 0;
									if (dusts.get(enchant.getEnchant().getType().getRarity()) != null)
										amount = dusts.get(enchant.getEnchant().getType().getRarity());
									amount++;
									dusts.put(enchant.getEnchant().getType().getRarity(), amount);
								}

						List<ItemStack> itemDusts = new ArrayList<>();
						for (Entry<EnchantRarity, Integer> e : dusts.entrySet()) {
							ItemStack dust = EnchantUtils.toBaseDust();
							dust.setAmount(e.getValue());
							itemDusts.add(dust);
						}

						firstSlot = 5;
						for (ItemStack dust : itemDusts) {
							player.getOpenInventory().getTopInventory().setItem(firstSlot, dust);
							if (firstSlot < 7)
								firstSlot = 6;
							else if (firstSlot == 7)
								firstSlot += 6;
							else if (firstSlot % 9 < 8)
								firstSlot += 1;
							else if (firstSlot % 9 == 8)
								firstSlot += 6;
						}
					} else
						P.sendMessage(player, M.TINKERER_FULL);
				}
			} else if (slot != 0 && slot != 8 && slot % 9 < 4) {
				inventory.setItem(slot, null);
				if (InventoryUtils.hasSpace(player.getInventory(), is))
					player.getInventory().addItem(is);
				else
					player.getWorld().dropItem(player.getLocation(), is);

				Map<EnchantRarity, Integer> dusts = new HashMap<>();
				for (int j = 0; j < 6; j++)
					for (int i = 0; i < 4; i++)
						if (i + (j * 9) != 0) {
							ItemStack item = player.getOpenInventory().getTopInventory().getItem(i + (j * 9));
							BookEnchant enchant = EnchantUtils.fromBook(item);
							if (enchant == null)
								continue;

							int amount = 0;
							if (dusts.get(enchant.getEnchant().getType().getRarity()) != null)
								amount = dusts.get(enchant.getEnchant().getType().getRarity());
							amount++;
							dusts.put(enchant.getEnchant().getType().getRarity(), amount);
						}

				List<ItemStack> itemDusts = new ArrayList<>();
				for (Entry<EnchantRarity, Integer> e : dusts.entrySet()) {
					ItemStack dust = EnchantUtils.toBaseDust();
					dust.setAmount(e.getValue());
					itemDusts.add(dust);
				}

				int firstSlot = 5;
				for (ItemStack dust : itemDusts) {
					player.getOpenInventory().getTopInventory().setItem(firstSlot, dust);
					if (firstSlot < 7)
						firstSlot = 6;
					else if (firstSlot == 7)
						firstSlot += 6;
					else if (firstSlot % 9 < 8)
						firstSlot += 1;
					else if (firstSlot % 9 == 8)
						firstSlot += 6;
				}
			}
		}

		if (inventory.equals(player.getInventory()))
			return;

		boolean set = !gui.isClickable();
		event.setCancelled(set);

		gui.clicked(player, event.getSlot(), event.getAction());

		if (set)
			event.setCancelled(false);

		GUIClickEvent e = new GUIClickEvent(player, gui, event.getClick(), event.getSlot(), event.getCurrentItem(),
				event);
		KingEnchants.callEvent(e);

		if ((gui.getID().equals("merge") && (slot == 11 || slot == 15)))
			override = true;

		e.setCancelled((event.isCancelled() || !gui.isClickable()) && !override);

		event.setCancelled((e.isCancelled() || !gui.isClickable()) && !override);

	}
}