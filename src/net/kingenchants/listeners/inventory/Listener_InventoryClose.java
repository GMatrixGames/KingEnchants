package net.kingenchants.listeners.inventory;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.cryptolead.core.utils.InventoryUtils;

import net.kingenchants.data.Temp;
import net.kingenchants.listeners.CListener;
import net.kingenchants.utils.gui.GUI;

public class Listener_InventoryClose extends CListener {
	
	@EventHandler
	public void on(InventoryCloseEvent event) {
		Player player = (Player) event.getPlayer();
		Inventory inventory = event.getInventory();
		
		if (inventory == null)
			return;
		
		GUI gui = Temp.OPEN_GUIS.get(player.getUniqueId());
		
		if (gui == null)
			return;
		
		Temp.OPEN_GUIS.remove(player.getUniqueId());
		
		if (gui.getID().equals("merge")) {
			ItemStack item0 = inventory.getItem(11);
			if (item0 != null)
				player.getInventory().addItem(item0);
			
			ItemStack item1 = inventory.getItem(15);
			if (item1 != null)
				player.getInventory().addItem(item1);
		}
		
		if (gui.getID().equals("tinkerer"))
			for (int j = 0; j < 6; j++)
				for (int i = 0; i < 4; i++)
					if (i + (j * 9) != 0) {
						ItemStack is = player.getOpenInventory().getTopInventory().getItem(i + (j * 9));
						if (is == null)
							continue;
						if (InventoryUtils.hasSpace(inventory, is))
							player.getInventory().addItem(is);
						else
							player.getWorld().dropItem(player.getLocation(), is);
					}
	}
}