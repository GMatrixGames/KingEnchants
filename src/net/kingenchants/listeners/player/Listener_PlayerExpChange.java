package net.kingenchants.listeners.player;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerExpChangeEvent;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;

import net.kingenchants.data.Temp;
import net.kingenchants.listeners.CListener;

public class Listener_PlayerExpChange extends CListener {
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void on(PlayerExpChangeEvent event) {
		Player player = event.getPlayer();
		FPlayer fPlayer = FPlayers.getInstance().getByPlayer(player);
		Faction faction = fPlayer.getFaction();
		
		if (event.getAmount() > 0) {
			if (Temp.XP_MULTIPLIER.containsKey(faction))
				event.setAmount((int) Math.round(event.getAmount() * Temp.XP_MULTIPLIER.get(faction)));
			
			if (Temp.GLOBAL_XP_MULTIPLIER != -1)
				event.setAmount((int) Math.round(event.getAmount() * Temp.GLOBAL_XP_MULTIPLIER));
		}
		
	}
}