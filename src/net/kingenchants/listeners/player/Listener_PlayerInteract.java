package net.kingenchants.listeners.player;

import java.util.Map.Entry;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.cryptolead.core.utils.InventoryUtils;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;

import net.kingenchants.chat.M;
import net.kingenchants.data.Temp;
import net.kingenchants.data.players.P;
import net.kingenchants.enchants.BookEnchant;
import net.kingenchants.enchants.EnchantRarity;
import net.kingenchants.listeners.CListener;
import net.kingenchants.utils.CRunnable;
import net.kingenchants.utils.enchants.EnchantUtils;

public class Listener_PlayerInteract extends CListener {
	
	@EventHandler
	public void on(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		FPlayer fPlayer = FPlayers.getInstance().getByPlayer(player);
		Faction faction = fPlayer.getFaction();
		ItemStack is = event.getItem();
		
		if (!event.getAction().equals(Action.RIGHT_CLICK_AIR) && !event.getAction().equals(Action.RIGHT_CLICK_BLOCK))
			return;
		
		if (is == null)
			return;
		
		Entry<Double, Integer> booster = EnchantUtils.fromBooster(is);
		if (booster != null) {
			event.setCancelled(true);
			
			if (faction.isWilderness()) {
				P.sendMessage(player, M.BOOSTER_NO_FACTION);
				return;
			}
			
			if (Temp.XP_MULTIPLIER.containsKey(faction)) {
				P.sendMessage(player, M.BOOSTER_ALREADY_ACTIVE);
				return;
			}
			
			if (is.getAmount() == 1)
				player.getInventory().setItemInHand(null);
			else {
				is.setAmount(is.getAmount() - 1);
				player.setItemInHand(is);
			}
			
			Temp.XP_MULTIPLIER.put(faction, booster.getKey());
			
			new CRunnable() {
				
				@Override
				public void run() {
					if (timer == 0) {
						cancel();
						Temp.XP_MULTIPLIER.remove(faction);
					}
					
					timer--;
				}
			}.activate(20 * 60, booster.getValue());
			
			for(Player p : faction.getOnlinePlayers())
				P.sendMessage(p, M.BOOSTER_ACTIVED, player, booster.getKey(), booster.getValue());
			
			return;
		}
		
		EnchantRarity rarity = EnchantUtils.fromBaseDust(is);
		if (rarity != null) {
			event.setCancelled(true);
			Entry<ItemStack, Boolean> dust = EnchantUtils.getRandomSuccessDust();
			
			if (!InventoryUtils.hasSpace(player.getInventory(), dust.getKey())) {
				P.sendMessage(player, M.BASE_DUST_USE_NO_SPACE);
				return;
			}
			
			if (is.getAmount() == 1)
				player.getInventory().setItemInHand(null);
			else {
				is.setAmount(is.getAmount() - 1);
				player.setItemInHand(is);
			}
			
			player.getInventory().addItem(dust.getKey());
			if (dust.getValue())
				P.sendMessage(player, M.BASE_DUST_USED, EnchantUtils.fromSuccessDust(dust.getKey()));
			else
				P.sendMessage(player, M.BASE_DUST_USED_FAILED);
			
			return;
		}
		
		Entry<EnchantRarity, Integer> baseBook = EnchantUtils.fromBaseBook(is);
		if (baseBook != null) {
			BookEnchant enchant = EnchantUtils.getRandom(baseBook.getKey(), baseBook.getValue());
			if (enchant == null)
				return;
			
			ItemStack book = EnchantUtils.toBook(enchant);
			if (!InventoryUtils.hasSpace(player.getInventory(), book)) {
				P.sendMessage(player, M.BASE_BOOK_USE_NO_SPACE);
				return;
			}
			
			if (is.getAmount() > 1) {
				is.setAmount(is.getAmount() - 1);
				player.setItemInHand(is);
			} else
				player.setItemInHand(null);
			
			event.setCancelled(true);
			
			player.getInventory().addItem(book);
			
			Firework f = (Firework)player.getWorld().spawnEntity(player.getLocation(), EntityType.FIREWORK);
		      
		      FireworkMeta fm = f.getFireworkMeta();
		      
		      fm.addEffect(FireworkEffect.builder()
		        .flicker(true)
		        .withColor(enchant.getEnchant().getType().getRarity().getC())
		        .withFade(Color.WHITE)
		        .withTrail()
		        .build());
		      
		      f.setFireworkMeta(fm);
			
			P.sendMessage(player, M.BASE_BOOK_USED, book.getItemMeta().getDisplayName());
			return;
		}
	}
}